package vn.hdbank.ts.common.model.createOTP.request;

import vn.hdbank.ts.common.model.createOTP.Header;

public class CreateOTPReq {

    private Header header;
    private BodyReq bodyReq;

    public Header getHeader() { return header; }

    public void setHeader(Header value) { this.header = value; }

    public BodyReq getBodyReq() { return bodyReq; }

    public void setBodyReq(BodyReq value) { this.bodyReq = value; }
}
