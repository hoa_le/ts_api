package vn.hdbank.ts.domain.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import vn.hdbank.ts.common.Constant;
import vn.hdbank.ts.common.model.createOTP.Client;
import vn.hdbank.ts.common.model.createOTP.Common;
import vn.hdbank.ts.common.model.createOTP.UserDetail;
import vn.hdbank.ts.common.utils.Utils;
import vn.hdbank.ts.application.utils.WriteLog;

import java.util.Arrays;
import java.util.Date;

public class ESBService {

    public Object sendRequest(String requestId, Object request, String ip, int port, String api) throws Exception {
        String functionName = "sendRequest";
        Gson gson = new GsonBuilder().create();

        String url = getAPIPath(ip, String.valueOf(port), api, false);

       // Common.getInstance().WriteLog(requestId, className, functionName, "REQUEST [" + url + "]>>>>>" + gson.toJson(request), null);
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<Object> result = null;
        headers.setContentType(MediaType.APPLICATION_JSON);

        try {
            //▼(#001)Add===================================
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            requestFactory.setConnectTimeout(12000); // set short connect timeout
            requestFactory.setReadTimeout(12000); // set slightly longer read timeout
            restTemplate.setRequestFactory(requestFactory);
            //▲(#001)Add===================================

            HttpEntity<Object> requestBody = new HttpEntity<Object>(request);

            WriteLog.write(requestId,  functionName, "REQUEST [" + url + "]>>>>>" + gson.toJson(requestBody), null);

            result = restTemplate.postForEntity(url, requestBody, Object.class);
        } catch (Exception e) {
//            Common.getInstance().WriteLog(requestId, className, functionName, "Exception: " + e.getMessage(), e);
            WriteLog.error(requestId, "TRUSTINGSOCIAL", e);
            throw e;
        }

        HttpStatus statusCode = result.getStatusCode();
        if (statusCode != HttpStatus.OK) {
            WriteLog.write(requestId, "TRUSTINGSOCIAL OK "   );
//            Common.getInstance().WriteLog(requestId, className, functionName, "HTTP RESPONSE EXCEPTION >>>>> statusCode: " + statusCode.toString(), null);
        }

        WriteLog.write(requestId,  functionName, "RESPONSE [" + url + "]>>>>>" + gson.toJson(result.getBody()), null);
        return result.getBody();
    }

    public static String getAPIPath(String ip, String port, String apiPath, boolean isSSL) {
        return isSSL ? "https://" + ip + ":" + port + apiPath : "http://" + ip + ":" + port + apiPath;
    }

    Common createESBHeaderCommon(String requestId) {
        Common common = new Common();
        common.setServiceVersion(Constant.ESB_SERVICE_VERSION);
        common.setMessageId(requestId);
        common.setTransactionId(requestId);
        common.setMessageTimestamp(Utils.formatDate(new Date(), "yyyy-MM-dd'T'hh:mm:ss.SS"));
        return common;
    }

    Client createESBHeaderClient() {
        Client client = new Client();
        client.setSourceAppID(Constant.ESB_SOURCE_APP);
        client.setTargetAppIDs(Arrays.asList("ESB1", "ESB2", "ESB3")); //todo: don't understand purpose this field/value
        UserDetail userDetail = new UserDetail();

        userDetail.setUserID(Constant.ESB_USER_ID);
        userDetail.setUserPassword(Constant.ESB_USER_PASSWORD);

        client.setUserDetail(userDetail);
        return client;
    }

}
