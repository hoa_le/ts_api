package vn.hdbank.ts.domain.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import vn.hdbank.ts.application.configuration.ApplicationConfig;
import vn.hdbank.ts.application.configuration.keyManage.KeyManager;
import vn.hdbank.ts.common.Constant;
import vn.hdbank.ts.common.EnumResultCode;
import vn.hdbank.ts.common.model.ErrorInfo;
import vn.hdbank.ts.common.model.createOTP.*;
import vn.hdbank.ts.common.model.createOTP.request.*;
import vn.hdbank.ts.common.model.createOTP.response.ResponseInfo;
import vn.hdbank.ts.application.utils.Utils;
import vn.hdbank.ts.application.utils.WriteLog;

public class ESBOtpService {
    private static final String className = ESBOtpService.class.getSimpleName();
    private ESBService esbService = new ESBService();
    final ObjectMapper mapper = new ObjectMapper(); // jackson's object mapper


    private final ApplicationConfig applicationConfig;

    @Autowired
    public ESBOtpService(ApplicationConfig applicationConfig) {
        this.applicationConfig = applicationConfig;
    }

    private Gson gson = new GsonBuilder().create();

    public RequestOtpObject requestOtpESB(String requestId, String userName,
                                          String otpMessage, String isReqChallengeCode, String clientImei) {
        String functionName = "requestOtpESB";
        String params = "requestId: " + requestId + ", userName: " + userName +
                ", otpMessage: " + otpMessage + ", isReqChallengeCode: " +
                isReqChallengeCode + ", clientImei: " + clientImei;
        RequestOtpObject requestOtpObject = new RequestOtpObject();

        EnumResultCode resultCode = EnumResultCode.SYSTEM_ERROR;

        try {
            WriteLog.write(requestId, className, functionName,
                            "INFO >>>>>" + params, null);

            //region "CREATE OBJECT REQUEST TO ESB"
            Header header = new Header();
            header.setCommon(esbService.createESBHeaderCommon(requestId));
            Client esbClient = esbService.createESBHeaderClient();

            String esbUser = KeyManager.GetInstance(applicationConfig).getPartnerKey("ts").getEsbUser();
            String esbPass = KeyManager.GetInstance(applicationConfig).getPartnerKey("ts").getEsbPass();

            esbClient.getUserDetail().setUserID(esbUser);
            esbClient.getUserDetail().setUserPassword(esbPass);

            header.setClient(esbClient);

           BodyReq bodyReq =
                    new BodyReq();
            bodyReq.setFunctionCode(Constant.ESB_FUNCTION_CODE_CREATE_OTP);

             Request requestBodyReq = new  Request();
            requestBodyReq.setRequestId(requestId);
            requestBodyReq.setChannel("ts");
            requestBodyReq.setRequestTime(Utils.getCurrentDateTimeWithFormat(
                    "yyyy-MM-dd'T'HH:mm:ss'Z'"));
            requestBodyReq.setServiceCode("ts");

             Data data = new  Data();
            data.setUserId(userName);
            data.setSerialNo(userName);
            data.setClientImei(clientImei);
            data.setTransDetail(otpMessage);
            data.setIsReqChalCode(isReqChallengeCode);
            data.setPartner("");
            data.setLanguage("vi");

            // SET CHILD FOR BodyReq
            bodyReq.setRequest(requestBodyReq);
            bodyReq.setData(data);

             CreateOTPReq createOTPReq =
                    new  CreateOTPReq();
            createOTPReq.setHeader(header);
            createOTPReq.setBodyReq(bodyReq);

             RequestInfo requestInfo =
                    new RequestInfo(createOTPReq);

            //endregion

            String ip = KeyManager.GetInstance(applicationConfig).getPartnerKey("ts").getOtpGwIp();
            int port = Integer.parseInt( KeyManager.GetInstance(applicationConfig).getPartnerKey("ts").getOtpGwPort());




            Object objectResponse = new ESBService().sendRequest(requestId, requestInfo, ip,  port,
                    Constant.URL_ESB_CREATE_OTP);
             ResponseInfo responseInfo =
                    mapper.convertValue(objectResponse, ResponseInfo.class);

            if (responseInfo != null) {
                if ("0".equals(responseInfo.getCreateOTPRes().getResponseStatus()
                        .getStatus())) {
                    resultCode = EnumResultCode.SUCCESS;

                    requestOtpObject.setChallengeCode(responseInfo.getCreateOTPRes()
                            .getBodyRes()
                            .getData()
                            .getChallengeCode());
                    requestOtpObject.setMediaType(responseInfo.getCreateOTPRes()
                            .getBodyRes()
                            .getData()
                            .getMediaType());
                } else {
                    ErrorInfo errorInfo = responseInfo.getCreateOTPRes()
                            .getResponseStatus()
                            .getErrorInfo().get(0);

                    int verifyOtp=99;
                    try {
                        verifyOtp = Integer.parseInt(errorInfo.getErrorCode());
                    }catch (Exception e){ }

                    switch (verifyOtp) {
                        case 0:
                            resultCode = EnumResultCode.SUCCESS;

                            break;

                        case 20:
                            resultCode = EnumResultCode.MAX_REQUEST_OTP;

                            break;

                        case 22:
                            resultCode = EnumResultCode.USER_NO_TOKEN_ACTIVE;

                            break;

                        case 23:
                            resultCode = EnumResultCode.USER_NO_TOKEN;

                            break;

                        case 24:
                            resultCode = EnumResultCode.USER_NO_TOKEN;

                            break;

                        default:
                            resultCode = EnumResultCode.OTP_ERROR;

                            break;
                    }
                }
            }

            requestOtpObject.setEnumResultCode(resultCode);
        } catch (Exception e) {
            WriteLog.error(requestId, className, functionName,
                    "EXCEPTION >>>>>" + params, e);
        } finally {
            WriteLog.write(requestId, className, functionName,
                    "INFO >>>>> resultCode: " + resultCode.getCode() + " - " +
                            resultCode.getMessage(), null);
        }

        return requestOtpObject;
    }

    public EnumResultCode verifyOtpESB(String requestId, String userName,
                                       String otp, String otpMessage, String challengeCode) {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        String functionName = "verifyOtpESB";
        EnumResultCode resultCode = EnumResultCode.SYSTEM_ERROR;
        String params = "requestId: " + requestId + ", userName: " + userName +
                ", otpMessage: " + otpMessage + ", challengeCode: " +
                challengeCode;

        try {


            //region "CREATE OBJECT REQUEST TO ESB"
             Header header = new  Header();
            header.setCommon(esbService.createESBHeaderCommon(requestId));
            header.setClient(esbService.createESBHeaderClient());

          vn.hdbank.ts.common.model.verifyOTP.request.BodyReq bodyReq =
                    new  vn.hdbank.ts.common.model.verifyOTP.request.BodyReq();
            bodyReq.setFunctionCode(Constant.ESB_FUNCTION_CODE_VERIFY_OTP);

            vn.hdbank.ts.common.model.verifyOTP.request.Request requestBodyReq =
                    new vn.hdbank.ts.common.model.verifyOTP.request.Request();
            requestBodyReq.setRequestId(requestId);
            requestBodyReq.setChannel("ts");
            requestBodyReq.setRequestTime(Utils.getCurrentDateTimeWithFormat(
                    "yyyy-MM-dd'T'HH:mm:ss'Z'"));
            requestBodyReq.setServiceCode("FT");

            vn.hdbank.ts.common.model.verifyOTP.request.Data data = new vn.hdbank.ts.common.model.verifyOTP.request.Data();
            data.setUserId(userName);
            data.setSerialNo(userName);
            data.setMediaType("58");
            data.setTransDetail(otpMessage);
            data.setOtp(otp);
            data.setChallengeCode(challengeCode);

            // SET CHILD FOR BodyReq
            bodyReq.setRequest(requestBodyReq);
            bodyReq.setData(data);

            vn.hdbank.ts.common.model.verifyOTP.request.VerifyOTPReq verifyOTPReq =
                    new vn.hdbank.ts.common.model.verifyOTP.request.VerifyOTPReq();
            verifyOTPReq.setHeader(header);
            verifyOTPReq.setBodyReq(bodyReq);

            vn.hdbank.ts.common.model.verifyOTP.request.RequestInfo requestInfo =
                    new vn.hdbank.ts.common.model.verifyOTP.request.RequestInfo(verifyOTPReq);

            //endregion
            String ip = KeyManager.GetInstance(applicationConfig).getPartnerKey("ts").getOtpGwIp();
            int port = Integer.parseInt( KeyManager.GetInstance(applicationConfig).getPartnerKey("ts").getOtpGwPort());



            Object objectResponse = new ESBService().sendRequest(requestId,
                    requestInfo, ip,
                    port,
                    Constant.URL_ESB_VERIFY_OTP);
            vn.hdbank.ts.common.model.verifyOTP.response.ResponseInfo responseInfo =
                    mapper.convertValue(objectResponse,
                            vn.hdbank.ts.common.model.verifyOTP.response.ResponseInfo.class);

            if (responseInfo != null) {
                if ("0".equals(responseInfo.getVerifyOTPRes().getResponseStatus()
                        .getStatus())) {
                    resultCode = EnumResultCode.SUCCESS;
                } else {
                    ErrorInfo errorInfo = responseInfo.getVerifyOTPRes()
                            .getResponseStatus()
                            .getErrorInfo().get(0);
                    WriteLog.write(requestId, className, functionName,
                                    "BUG >>>>> ESB > verifyOTP > resultCode: " +
                                            errorInfo.getErrorCode() + " - " +
                                            errorInfo.getErrorDesc() + " >>> ErrorInfo: " +
                                            gson.toJson(responseInfo.getVerifyOTPRes()
                                                    .getResponseStatus()), null);

                    int verifyOtp=99;
                    try {
                        verifyOtp = Integer.parseInt(errorInfo.getErrorCode());
                    }catch (Exception e){ }

                    switch (verifyOtp) {
                        case 0:
                            resultCode = EnumResultCode.SUCCESS;

                            break;

                        case 8:
                            resultCode = EnumResultCode.OTP_INVALID;

                            break;

                        case 17:
                            resultCode = EnumResultCode.TOKEN_OUT_OF_SYNC;

                            break;

                        case 19:
                            resultCode = EnumResultCode.MAX_VERIFY_OTP;

                            break;

                        case 21:
                            resultCode = EnumResultCode.OTP_EXPIRE;

                            break;

                        case 22:
                            resultCode = EnumResultCode.USER_NO_TOKEN_ACTIVE;

                            break;

                        case 23:
                            resultCode = EnumResultCode.USER_NO_TOKEN;

                            break;

                        case 24:
                            resultCode = EnumResultCode.USER_NO_TOKEN;

                            break;

                        default:
                            resultCode = EnumResultCode.OTP_ERROR;

                            break;
                    }
                }
            }
        } catch (Exception e) {
            WriteLog.error(requestId, className, functionName,
                            "EXCEPTION >>>>>" + params, e);
        } finally {
            WriteLog.write(requestId, className, functionName,
                            "INFO >>>>> resultCode: " + resultCode.getCode() + " - " +
                                    resultCode.getMessage(), null);
        }

        return resultCode;
    }


}
