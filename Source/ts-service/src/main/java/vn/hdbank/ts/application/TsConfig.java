package vn.hdbank.ts.application;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author ThuongDc
 * @since 9/24/2020
 */

@Configuration
@ConfigurationProperties(prefix = "hdbank.ts")
@Getter
@Setter
public class TsConfig {
    private String partnerId;

    private String partnerPassword;

    private String partnerName;

    private String rsaPublicKey;

    private String rsaPrivateKey;

    private String aesSecretKey;

    private String aesSalt;

    private String authPartnerUsername;

    private String authPartnerPassword;

    private String whitelistIp;
}
