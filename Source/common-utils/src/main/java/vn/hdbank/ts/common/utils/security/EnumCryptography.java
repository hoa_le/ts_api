package vn.hdbank.ts.common.utils.security;

public enum EnumCryptography {
	Rsa,
    TripleDes,
    Algorithm3,
    Algorithm4,
    Algorithm5
}
