package vn.hdbank.ts.infrastructure.database.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;

import vn.hdbank.ts.application.resource.BasicResponse;
import vn.hdbank.ts.application.resource.LeadRequest;
import vn.hdbank.ts.application.resource.StatusRequest;
import vn.hdbank.ts.application.resource.StatusResponse;
import vn.hdbank.ts.application.utils.ResultCode;
import vn.hdbank.ts.application.utils.Utils;
import vn.hdbank.ts.datasource.DataSourceFactory;
import vn.hdbank.ts.datasource.EnumDataSource;
import vn.hdbank.ts.domain.object.handle.Constant;
import vn.hdbank.ts.utils.WriteLog;
import vn.hdbank.ts.infrastructure.database.LeadDao;
import org.apache.commons.text.RandomStringGenerator;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.Date;

@Repository
public class LeadDaoImpl implements LeadDao {
	private static final String className = LeadDao.class.getSimpleName();

	@Value("${hdbank.ts.store.lead}")
	private String lead;
	@Value("${hdbank.ts.store.status}")
	private String status;
	@Value("${hdbank.ts.campaignId}")
	private String campaignId;
	@Value("${hdbank.ts.businessGroup}")
	private String businessGroup;
	@Value("${hdbank.ts.store.timeout}")
	private int timeOut;
	@Value("${hdbank.ts.limitCreatedDate}")
	private int limitCreatedDate;
	@Value("${hdbank.ts.status-not-exist-lead}")
	private String leadNotExist;
	
	private Gson gson = new Gson();
	@Override
	public BasicResponse insertOrUpdateLead(LeadRequest request) {
		String contactId = new RandomStringGenerator.Builder().withinRange('0', 'z')
				.filteredBy(t -> t >= '0' && t <= '9', t -> t >= 'A' && t <= 'Z').build().generate(10);
		BasicResponse response = new BasicResponse();
		Date date = new Date();
        //This method returns the time in millis
        long timeMilli = date.getTime();
		try (Connection CSBConnection = DataSourceFactory.getInstance().getDataSource(EnumDataSource.ecrm)
				.getConnection();
				CallableStatement callStmt = CSBConnection.prepareCall(
						"{call " + lead + "(?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ? ,?, ?, ?, ?, ? ,?, ?, ?, ?, ? ,?, ?,"
								+ " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
								+ " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
								+ " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
								+ " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
								+ " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
								+ " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
								+ " ?, ?, ?, ?, ?, ?, ?, ?, ?)} ")) {
			
			callStmt.setString(1, request.getActionCode().toUpperCase());
			callStmt.setString(2, contactId);
			callStmt.setString(3, request.getLeadId());
			callStmt.setString(4, request.getCampaignId());
			callStmt.setString(5, request.getLeadStatusId());
			callStmt.setString(6, request.getFullName());
			if (request.getFullNameUpCase() != null)
				callStmt.setString(7, Utils.removeAccent(request.getFullNameUpCase().trim()).toUpperCase());
			else
				callStmt.setString(7, request.getFullNameUpCase());
			callStmt.setString(8, request.getNationalId());
			callStmt.setString(9, request.getPhone());
			callStmt.setString(10, request.getEmail());
			callStmt.setString(11, request.getPaymentAccount());
			callStmt.setString(12, request.getInternationalDebitCard());
			callStmt.setString(13, request.getDomesticDebitCard());
			callStmt.setString(14, request.geteBanking());
			callStmt.setString(15, request.getCif());
			callStmt.setString(16, request.getAcademicLevel());
			callStmt.setString(17, request.getResidenceStatus());
			callStmt.setString(18, request.getNationalType());
			callStmt.setString(19, request.getIssuedDate());
			callStmt.setString(20, request.getIssuedBy());
			callStmt.setString(21, request.getResidenceDurationM());
			callStmt.setString(22, request.getResidenceDurationY());
			callStmt.setString(23, request.getGender());
			callStmt.setString(24, request.getMartialStatus());
			callStmt.setString(25, request.getPin());
			callStmt.setString(26, request.getNumberOfDependents());
			callStmt.setString(27, request.getCardPrintedName());
			callStmt.setString(28, request.getDob());
			callStmt.setString(29, request.getNationality());
			callStmt.setString(30, request.getMobilePhone1());
			callStmt.setString(31, request.getMobilePhone2());
			callStmt.setString(32, request.getInsuranceNumber());
			callStmt.setString(33, request.getExpDateOfResidence());
			callStmt.setString(34, request.getAddressPermanent());
			callStmt.setString(35, request.getProvincePermanent());
			callStmt.setString(36, request.getDistrictPermanent());
			callStmt.setString(37, request.getWardPermanent());
			callStmt.setString(38, request.getAddressCurrent());
			callStmt.setString(39, request.getSameAsPermanent());
			callStmt.setString(40, request.getProvinceTemporary());
			callStmt.setString(41, request.getDistrictTemporary());
			callStmt.setString(42, request.getWardTemporary());
			callStmt.setString(43, request.getHdbEmployee());
			callStmt.setString(44, request.getEmployeeId());
			callStmt.setString(45, request.getPosition());
			callStmt.setString(46, request.getTaxNumber());
			callStmt.setString(47, request.getCompany());
			callStmt.setString(48, request.getWorkingStatus());
			if (request.getCampaignId().equalsIgnoreCase(campaignId))
				callStmt.setString(49, businessGroup);
			else
				callStmt.setString(49, request.getBusinessGroup());
			callStmt.setString(50, request.getBusinessType());
			callStmt.setString(51, request.getIndustry());
			callStmt.setString(52, request.getWorkingProvince());
			callStmt.setString(53, request.getWorkingDistrict());
			callStmt.setString(54, request.getWorkingWard());
			callStmt.setString(55, request.getWorkingAddress());
			callStmt.setString(56, request.getWorkingDepartment());
			callStmt.setString(57, request.getWorkingPosition());
			callStmt.setString(58, request.getWorkingPhone());
			callStmt.setString(59, request.getWorkingPhoneExt());
			callStmt.setString(60, request.getWorkingBusinessDuration());
			callStmt.setString(61, request.getCharterCapital());
			callStmt.setString(62, request.getNumberOfEmployees());
			callStmt.setString(63, request.getBusinessSize());
			callStmt.setString(64, request.getDurationOfWork());
			callStmt.setString(65, request.getLaborContractType());
			callStmt.setString(66, request.getReceivingSalaryType());
			callStmt.setString(67, request.getIncome());
			callStmt.setString(68, request.getOtherIncome());
			callStmt.setString(69, request.getOtherIncomeDescription());
			callStmt.setString(70, request.getMonthlyExpenses());
			callStmt.setString(71, request.getHdbCreditLimit());
			callStmt.setString(72, request.getTransactionWithHDBank());
			callStmt.setString(73, request.getDeposit());
			callStmt.setString(74, request.getLoan());
			callStmt.setString(75, request.getMaxCic());
			callStmt.setString(76, request.getMonths12());
			callStmt.setString(77, request.getMonths24());
			callStmt.setString(78, request.getMonths36());
			callStmt.setString(79, request.getCurrent());
			callStmt.setString(80, request.getOutstandingLoan());
			callStmt.setString(81, request.getTotalCreditcardOutstandingLoan());
			callStmt.setString(82, request.getRef1RelWithCardholder());
			callStmt.setString(83, request.getRef1Fullname());
			callStmt.setString(84, request.getRef1NationalId());
			callStmt.setString(85, request.getRef1MobilePhone());
			callStmt.setString(86, request.getRef1Company());
			callStmt.setString(87, request.getRef1Address());
			callStmt.setString(88, request.getRef2RelWithCardholder());
			callStmt.setString(89, request.getRef2Fullname());
			callStmt.setString(90, request.getRef2NationalId());
			callStmt.setString(91, request.getRef2MobilePhone());
			callStmt.setString(92, request.getRef2Company());
			callStmt.setString(93, request.getRef2Address());
			callStmt.setString(94, request.getPrimaryOrSecondaryCard());
			callStmt.setString(95, request.getTargetCustomer());
			callStmt.setString(96, request.getCardPolicy());
			callStmt.setString(97, request.getApplicationPeriod());
			callStmt.setString(98, request.getFromDate());
			callStmt.setString(99, request.getToDate());
			callStmt.setString(100, request.getMainContractNumber());
			callStmt.setString(101, request.getContractProductCode());
			callStmt.setString(102, request.getCardStampingAddress());
			callStmt.setString(103, request.getCardStampingDateTime());
			callStmt.setString(104, request.getCardReceivingAddress());
			callStmt.setString(105, request.getPinMailer());
			callStmt.setString(106, request.getCardStatementSendingType());
			callStmt.setString(107, request.getCardStatementReceivingAddress());
			callStmt.setString(108, request.getMonthlyStatementDate());
			callStmt.setString(109, request.getAutoDebtDeduction());
			callStmt.setString(110, request.getOtherCardServices());
			callStmt.setString(111, request.getSms());
			callStmt.setString(112, request.getBranchCode());
			callStmt.setString(113, request.getBranchName());
			callStmt.setString(114, request.getStaffCode());
			callStmt.setString(115, request.getStaffName());
			callStmt.setString(116, request.getAmlFatcaResident());
			callStmt.setString(117, request.getAmlFatcaOrganization());
			callStmt.setString(118, request.getAmlFatcaInfluence());
			callStmt.setString(119, request.getAmlFatcaUs());
			callStmt.setString(120, request.getRecommendedCreditLimit());
			callStmt.setString(121, request.getNationalIdFormer());
			callStmt.setString(122, request.getNationalTypeFormer());
			callStmt.setString(123, gson.toJson(request.getDocumentFile()));
			callStmt.setString(124, request.getBrowser());
			callStmt.setString(125, request.getUrlFull());
			callStmt.setString(126, request.getFlowType());
			callStmt.setString(127, request.getClientSupportDocType());
			callStmt.setString(128, request.getNeedUpdateDoc());
			callStmt.setString(129, request.getCicS37Time());
			callStmt.setString(130, request.getCicS37());
			callStmt.setString(131, request.getCicR11aTime());
			callStmt.setString(132, request.getCicR11a());
			callStmt.setString(133, request.getCicR14Time());
			callStmt.setString(134, request.getCicR14());
			callStmt.setString(135, request.getCreditScore());
			callStmt.setString(136, request.getFraudScore());
			callStmt.setString(137, request.getEkycLivenessCheckStatus());
			callStmt.setString(138, request.getEkycLivenessCheckScore());
			callStmt.setString(139, request.getEkycLivenessRequestTime());
			callStmt.setString(140, request.getEkycFaceMatchingStatus());
			callStmt.setString(141, request.getEkycFaceMatchingScore());
			callStmt.setString(142, request.getEkycFaceMatchingRequestTime());
			callStmt.setString(143, request.getEkycRetrievalData());
			callStmt.setString(144, request.getEkycRetrievalStatus());
			callStmt.setString(145, request.getEkycRetrievalRequestTime());
			callStmt.setString(146, request.getEkycIdCardTamperingVerdict());
			callStmt.setString(147, request.getEkycIdCardTamperingVerdictDetails());
			callStmt.setString(148, request.getOcrRequestTime());
			callStmt.setString(149, request.getOcrDetails());
			callStmt.setString(150, request.getSimFullName());
			callStmt.setString(151, request.getSimDob());
			callStmt.setString(152, request.getSimId());
			callStmt.setString(153, request.getSimPhoneNumber());
			callStmt.setString(154, request.getSimUpdatedAt());
			callStmt.setString(155, request.getSiFullName());
			callStmt.setString(156, request.getSiCompanyName());
			callStmt.setString(157, request.getSiCompanyTaxId());
			callStmt.setString(158, request.getSiDateOfBirth());
			callStmt.setString(159, request.getSiIdNumber());
			callStmt.setString(160, request.getSiLastPaidDate());
			callStmt.setString(161, request.getSiSalary());
			callStmt.setString(162, request.getSiNumber());
			callStmt.setString(163, request.getPitIdValue());
			callStmt.setString(164, request.getPitNumber());
			callStmt.setString(165, request.getPitFullname());
			callStmt.setString(166, request.getPitDob());
			callStmt.setString(167, request.getPitGender());
			callStmt.setString(168, request.getPitCompanyId());
			callStmt.setString(169, request.getPitIncomeType());
			callStmt.setString(170, request.getPitIncome());
			callStmt.setString(171, request.getPitTotalIncome());
			callStmt.setString(172, request.getOtherNid3rp());
			callStmt.setString(173, request.getDetailsApprovalRules());
			callStmt.setString(174, request.getApprovalStatus());
			callStmt.setString(175, request.getRejetedCode());
			callStmt.setString(176, request.getRejectedReason());
			callStmt.registerOutParameter(177, 12);
			callStmt.setLong(178, limitCreatedDate);
			callStmt.setString(179, request.getSecurityAnswer());
			callStmt.setString(180, request.getClientTier());
			callStmt.setString(181, request.getUtmSource());
			callStmt.setString(182, request.getUtmCampaign());
			callStmt.setString(183, request.getUtmRef());
			callStmt.setString(184, leadNotExist);
			callStmt.setQueryTimeout(timeOut);
			callStmt.execute();

			if (callStmt.getString(177).equals(Constant.SUCCESS)) {
				response.setCode(ResultCode.SUCCESS.getCode());
				response.setMessage(ResultCode.SUCCESS.getMessage());
			} else if (callStmt.getString(177).equals(ResultCode.LEAD_EXIST.getCode())) {
				response.setCode(ResultCode.LEAD_EXIST.getCode());
				response.setMessage(ResultCode.LEAD_EXIST.getMessage());
			} else if (callStmt.getString(177).equals(ResultCode.ID_NOT_FOUND.getCode())) {
				response.setCode(ResultCode.ID_NOT_FOUND.getCode());
				response.setMessage(ResultCode.ID_NOT_FOUND.getMessage());
			} else if (callStmt.getString(177).equals(ResultCode.CAMPAIGN_ID_EXIST.getCode())) {
				response.setCode(ResultCode.CAMPAIGN_ID_EXIST.getCode());
				response.setMessage(ResultCode.CAMPAIGN_ID_EXIST.getMessage());
			} else {
				response.setCode(ResultCode.TS_ERROR.getCode());
				response.setMessage(callStmt.getString(177));
				WriteLog.write(request.getRequestId(), className, "insertOrUpdateLead", callStmt.getString(177));
			}

		} catch (Exception e) {
			WriteLog.error(request.getRequestId(), className, "insertOrUpdateLead", e);
			response.setCode(ResultCode.TS_ERROR.getCode());
			response.setMessage(ResultCode.TS_ERROR.getMessage());
		} finally {
        	WriteLog.write(request.getRequestId(), className, "insertOrUpdateLead: processTime DB",  (new Date().getTime() -timeMilli ));
        }

		return response;
	}

	@Override
	public StatusResponse getLeadStatus(StatusRequest request) {
		StatusResponse response = new StatusResponse();

		try (Connection CSBConnection = DataSourceFactory.getInstance().getDataSource(EnumDataSource.ecrm)
				.getConnection();
				CallableStatement callStmt = CSBConnection.prepareCall("{call " + status + "(?, ?, ?, ?, ?)} ")) {

			callStmt.setString(1, request.getLeadId());
			callStmt.registerOutParameter(2, 12);
			callStmt.registerOutParameter(3, 12);
			callStmt.registerOutParameter(4, 12);
			callStmt.registerOutParameter(5, 12);
			callStmt.setQueryTimeout(timeOut);
			callStmt.execute();

			if (callStmt.getString(4).equals(Constant.SUCCESS)) {
				response.setStatusCode(callStmt.getString(3));
				response.setStatusName(callStmt.getString(2));
				response.setSupportCode(callStmt.getString(5));
				response.setCode(ResultCode.SUCCESS.getCode());
				response.setMessage(ResultCode.SUCCESS.getMessage());
			} else {
				response.setCode(ResultCode.ID_NOT_FOUND.getCode());
				response.setMessage(ResultCode.ID_NOT_FOUND.getMessage());
			}

		} catch (Exception e) {
			WriteLog.error(request.getRequestId(), className, "checkStatus", e);
			response.setCode(ResultCode.TS_ERROR.getCode());
			response.setMessage(ResultCode.TS_ERROR.getMessage());
		}

		return response;
	}

	@Override
	public String insertRequest(String requestID) {
		String response = null;

		try (Connection CSBConnection = DataSourceFactory.getInstance().getDataSource(EnumDataSource.ecrm)
				.getConnection();
				CallableStatement callStmt = CSBConnection
						.prepareCall("{call MBLP.HDB_TS_SERVICE.pro_insert_request(?, ?)} ")) {

			callStmt.setString(1, requestID);
			callStmt.registerOutParameter(2, 12);
			callStmt.setQueryTimeout(timeOut);
			callStmt.execute();
			response = callStmt.getString(2);

		} catch (Exception e) {
			WriteLog.error(requestID, className, "insertRequest", e);
			response = ResultCode.TS_ERROR.getCode();
		}

		return response;
	}

	@Override
	public void deleteRequest(String requestID) {

		try (Connection CSBConnection = DataSourceFactory.getInstance().getDataSource(EnumDataSource.ecrm)
				.getConnection();
				CallableStatement callStmt = CSBConnection
						.prepareCall("{call MBLP.HDB_TS_SERVICE.pro_delete_request(?, ?)} ")) {

			callStmt.setString(1, requestID);
			callStmt.registerOutParameter(2, 12);
			callStmt.setQueryTimeout(timeOut);
			callStmt.execute();
		} catch (Exception e) {
			WriteLog.error(requestID, className, "deleteRequest", e);
		}
	}
	
}
