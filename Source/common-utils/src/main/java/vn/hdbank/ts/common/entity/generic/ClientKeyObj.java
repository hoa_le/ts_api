package vn.hdbank.ts.common.entity.generic;

import java.util.Date;

public class ClientKeyObj {
	private String publicKey;
	private Date createdDate;
	
	public ClientKeyObj(String publicKey){
		this.publicKey=publicKey;
		this.createdDate=new Date();
	}
	
	public String getPublicKey() {
		return publicKey;
	}
	 
	public Date getCreatedDate() {
		return createdDate;
	}
 
}
