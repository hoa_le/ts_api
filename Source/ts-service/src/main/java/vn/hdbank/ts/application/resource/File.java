package vn.hdbank.ts.application.resource;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class File {

	@NotNull
	private String id;

	@NotNull
//    @DateTimeFormat(pattern = "yyyyMMddHHmmss")
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmss")
	private String type;

	@NotNull
	private String filename;
	
	private String directory;

}
