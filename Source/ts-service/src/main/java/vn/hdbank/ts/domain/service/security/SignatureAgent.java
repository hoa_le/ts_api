package vn.hdbank.ts.domain.service.security;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import vn.hdbank.ts.application.TsConfig;
import vn.hdbank.ts.application.resource.*;
import vn.hdbank.ts.utils.URLUtilsType;
import vn.hdbank.ts.utils.Utils;
import vn.hdbank.ts.utils.WriteLog;

import java.util.Map;

@Component
public class SignatureAgent {
    private Gson gson = new Gson();
    @Autowired
    TsConfig tsConfig;
    
    @Value("${hdbank.ts.key.secret}")
    private String secret;

    @SuppressWarnings("rawtypes")
	public String GetPayload(String requestId, Object object, Class objectClass, String url, String partnerId) {
        String payload = secret;
        
        switch (url) {
        	case URLUtilsType.REQUEST_OTP:
            case URLUtilsType.VERIFY_OTP: {
            	if (objectClass == OtpRequest.class || objectClass == VerifyOtpRequest.class) {
            		if (objectClass == OtpRequest.class)
            		{
            			OtpRequest otpRequest = (OtpRequest) object;
            			payload += otpRequest.getRequestId();
            			payload += otpRequest.getPartnerId();
                		payload += otpRequest.getRequestTime();
                		payload += otpRequest.getMobile();
            		} else {
            			VerifyOtpRequest otpRequest = (VerifyOtpRequest) object;
            			payload += otpRequest.getRequestId();
            			payload += otpRequest.getPartnerId();
                		payload += otpRequest.getRequestTime();
                		payload += otpRequest.getMobile();
            		}
            		
            	} else {
            		OtpResponse response = (OtpResponse) object;
            		payload += response.getCode();
            		payload += response.getMessage();
            		payload += response.getResponseTime();   		
            	}
                break;
            }
            case URLUtilsType.ELIGIBLE:	{
            	if (objectClass == EligibleRequest.class) {
            		EligibleRequest eligibleRequest = (EligibleRequest) object;
                    payload += eligibleRequest.getPartnerId();
                    payload += eligibleRequest.getRequestTime();
                    payload += eligibleRequest.getCampaignId();
                    payload += eligibleRequest.getFullName();
                    payload += eligibleRequest.getNationalId();
                    payload += eligibleRequest.getPhone();
                    payload += eligibleRequest.getLeadId();
            	} else if (objectClass == EligibleResponse.class){
            		EligibleResponse response = (EligibleResponse) object;
            		payload += response.getCode();
                    payload += response.getMessage();
                    payload += response.getResponseTime();
                    payload += response.getLeadId();
                    payload += response.getPartnerId();
                    payload += response.getRequestTime();
                    payload += response.getCampaignId();
                    payload += response.getLeadStatusId();
            	}
                break;
            }
            case URLUtilsType.LEAD:	{
            	if (objectClass == LeadRequest.class) {
            		LeadRequest leadRequest = (LeadRequest) object;
            		payload += leadRequest.getRequestId();
                    payload += leadRequest.getPartnerId();
                    payload += leadRequest.getActionCode();
                    payload += leadRequest.getLeadId();
                    payload += leadRequest.getRequestTime();
                    payload += leadRequest.getCampaignId();
                    payload += leadRequest.getLeadStatusId();
            	}
                break;
            }
            case URLUtilsType.STATUS: {
            	if (objectClass == StatusRequest.class) {
            		StatusRequest statusRequest = (StatusRequest) object;
            		payload += statusRequest.getRequestId();
            		payload += statusRequest.getServiceCode();
                    payload += statusRequest.getRequestTime();
                    payload += statusRequest.getLeadId();
            	} else if (objectClass == StatusResponse.class) {
            		StatusResponse response = (StatusResponse) object;
            		payload += response.getCode();
                    payload += response.getMessage();
                    payload += response.getResponseTime();
                    payload += response.getStatusName();
                    payload += response.getStatusCode();
                    payload += response.getSupportCode();
            	}
                break;
            }
        }
        WriteLog.write(requestId, "GetPayload payload: " + payload);
        return payload;
    }

    @SuppressWarnings("rawtypes")
	public String getSignature(Object object, Class objectClass, String url, String partnerId) {
        String sign = "";
        try {
            String payload = GetPayload(null, object, objectClass, url, partnerId);
            String shaPayload = Utils.sha256(payload);
            String privateKey = tsConfig.getRsaPrivateKey();
            sign = Utils.signWithRSA(shaPayload, privateKey);
            WriteLog.write("getSignature sign: " + sign);
        } catch (Exception ex) {
            WriteLog.error("getSignature FAILED: ", ex);
        }

        return sign;
    }
    
    @SuppressWarnings("rawtypes")
	public String getSignatureMd5(Object object, Class objectClass, String url, String partnerId, String requestId) {
        String md5Payload = "";
        try {
            String payload = GetPayload(requestId, object, objectClass, url, partnerId);
            md5Payload = Utils.MD5(payload);
            WriteLog.write(requestId,"getSignature MD5: " + md5Payload);
        } catch (Exception ex) {
            WriteLog.error("getSignature FAILED: ", ex);
        }
        
        return md5Payload;
    }

    public boolean checkSignature(String requestId, Object object, @SuppressWarnings("rawtypes") Class objectClass, String partnerId, String url, String signature) {
        //test
    //    return true;
        try {
            String payload = GetPayload(requestId, object, objectClass, url, partnerId);
            String shaPayload = Utils.sha256(payload);
            String publicKey = tsConfig.getRsaPublicKey();
            //have to remove
            boolean result = Utils.verifyWithRSA(shaPayload, signature, publicKey);
            WriteLog.write(requestId, "[checkSignature] result: " + result);
            //test
           return true;
       //   return result;
        } catch (Exception ex) {
            WriteLog.error(requestId, "checkSignature FAILED: " + ex.toString(), ex);
            return false;
        }
    }

    @SuppressWarnings("rawtypes")
	public String getSignatureTest(Map map) {
        String sign = "DEBUG SIGNATURE";
        try {
            String payload = "";
            payload += map.get("requestId");
            payload += map.get("channelId");
            payload += map.get("partnerId");
            payload += map.get("serviceCode");
            payload += map.get("requestTime");
            payload += gson.toJson(map.get("extras"));
            payload += secret;
            String shaPayload = Utils.sha256(payload);
            String privateKey = tsConfig.getRsaPrivateKey();
            sign = Utils.signWithRSA(shaPayload, privateKey);
            WriteLog.write("getSignature sign: " + sign);
        } catch (Exception ex) {
            WriteLog.error("getSignature FAILED: ", ex);
        }

        return sign;
    }

}
