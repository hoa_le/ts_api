package vn.hdbank.ts.common.entity.generic;

public class RouteObj {
	private String serviceCode;
	private String terminal;
	private String balancing;
	private String fixedServer;
	private String termAPIPath;
	private String termAPIDataType;
	private String termAPIMethod;
	
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public String getBalancing() {
		return balancing;
	}
	public void setBalancing(String balancing) {
		this.balancing = balancing;
	}
	public String getFixedServer() {
		return fixedServer;
	}
	public void setFixedServer(String fixedServer) {
		this.fixedServer = fixedServer;
	}
	public String getTermAPIPath() {
		return termAPIPath;
	}
	public void setTermAPIPath(String termAPIPath) {
		this.termAPIPath = termAPIPath;
	}
	public String getTermAPIDataType() {
		return termAPIDataType;
	}
	public void setTermAPIDataType(String termAPIDataType) {
		this.termAPIDataType = termAPIDataType;
	}
	public String getTermAPIMethod() {
		return termAPIMethod;
	}
	public void setTermAPIMethod(String termAPIMethod) {
		this.termAPIMethod = termAPIMethod;
	}
	
}
