package vn.hdbank.ts.common;

/**
 * Created by hoalh2 on 10/31/2016.
 */
public class Constant {
    //region "ESB"

    public static final String URL_ESB_CREATE_OTP = "/authenService/v1/authen/createOTP";
    public static final String URL_ESB_VERIFY_OTP = "/authenService/v1/authen/verifyOTP";
    public static final String ESB_FUNCTION_CODE_CREATE_OTP = "Authen-CreateOTP-REST-AuthenGW";
    public static final String ESB_FUNCTION_CODE_VERIFY_OTP = "Authen-VerifyOTP-REST-AuthenGW";
    public static final String ESB_USER_ID = "MB";
    public static final String ESB_USER_PASSWORD = "TUIxMjM=";
    public static final String ESB_SERVICE_VERSION = "1";
    public static final String ESB_SOURCE_APP = "MB";

}

