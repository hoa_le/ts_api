package vn.hdbank.ts.common.security;

public enum EnumRequestType {
	SymmetricCryptography,
    AsymmetricCryptography
}
