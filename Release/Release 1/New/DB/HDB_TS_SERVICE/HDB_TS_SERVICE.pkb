﻿CREATE OR REPLACE PACKAGE BODY MBLP.HDB_TS_SERVICE AS
   PROCEDURE pro_check_eligible(p_leadId           IN       VARCHAR2,
                                p_FULLNAME         IN       VARCHAR2,
                                p_MOBILE           IN       VARCHAR2,
                                p_NATIONALID       IN       VARCHAR2,
                                p_error                 OUT VARCHAR2,
                                p_tranDetail            OUT SYS_REFCURSOR,
                                p_resultType            OUT VARCHAR2,
                                p_campaignId       IN       VARCHAR2)
    AS
        v_count     NUMBER;
        v_client_tier   VARCHAR2(10);
        v_expired_date  DATE;
        
    BEGIN
        p_error := '000000';
        p_resultType := 'fully_matched';
        SELECT count(*) into v_count 
            FROM MBLP.LM_LEAD_DETAILS ld , MBLP.LM_LEAD le
            WHERE   le.LEAD_ID   = ld.LEAD_ID 
                AND(ld.LEAD_ID = p_leadId 
                    OR (
                            ld.MOBILEPHONE1        = p_MOBILE
                        AND ld.FULLNAME_UPCASE     = p_FULLNAME
                        AND ld.NATIONALID          = p_NATIONALID
                        )
                    )
                AND le.CAMPAIGN_ID = p_campaignId;
        IF v_count > 0 THEN
            OPEN p_tranDetail FOR
                SELECT  ld.LEAD_ID,
                        le.CAMPAIGN_ID,
                        le.STATUS_ID,
                        ld.FULLNAME,
                        TO_CHAR(ld.DOB,'YYYYMMDD') AS DOB,
                        ld.NATIONALID,
                        ld.MOBILEPHONE1,
                        ld.EMAIL,
                        ld.ACADEMIC_LEVEL,
                        ld.MARTIAL_STATUS,
                        ld.SECURITY_ANSWER,
                        ld.ADDRESS_PERMANENT,
                        ld.PROVINCE_PERMANENT,
                        ld.DISTRICT_PERMANENT,
                        ld.WARD_PERMANENT,
                        ld.ADDRESS_CURRENT,
                        ld.PROVINCE_TEMPORARY,
                        ld.DISTRICT_TEMPORARY,
                        ld.WARD_TEMPORARY,
                        ld.POSITION,
                        ld.COMPANY,
                        ld.WORKING_STATUS,
                        ld.BUSINESS_GROUP,
                        ld.BUSINESS_TYPE,
                        ld.WORKING_PROVINCE,
                        ld.WORKING_DISTRICT,
                        ld.WORKING_WARD,
                        ld.WORKING_STREET,
                        ld.WORKING_PHONE_NUMBER,
                        ld.RECEIVING_SALARY_TYPE,
                        ld.INCOME,
                        ld.CARD_RECEIVINGADDRESS,
                        ld.BRANCH_CODE,
                        ld.STAFF_CODE,
                        ld.RECOMMENDED_CREDIT_LIMIT,
                        ld.CLIENT_TIER,
                        ld.NEED_EKYC,
                        ld.NEED_ESIGN,
                        ld.CONTRACTPRODUCTCODE,
                        TO_CHAR(le.CREATED_DATE,'YYYYMMDDHH24MISS') AS CREATED_DATE,
                        TO_CHAR(ld.ISSUED_DATE,'YYYYMMDD') AS ISSUED_DATE,
                        ld.ISSUED_BY,
                        ld.GENDER,
                        ld.RESIDENCE_DURATION_M,
                        ld.RESIDENCE_DURATION,
                        ld.NUMBER_OF_DEPENDENTS,
                        ld.REF1_REL_WITH_CARDHOLDER,
                        ld.REF1_FULLNAME,
                        ld.REF1_PHONE,
                        ld.REF2_REL_WITH_CARDHOLDER,
                        ld.REF2_FULLNAME,
                        ld.REF2_PHONE,
                        ld.DOCUMENT_FILE,
                        ld.WORKING_POSITION
                FROM    MBLP.LM_LEAD_DETAILS ld , MBLP.LM_LEAD le
                WHERE   le.LEAD_ID   = ld.LEAD_ID 
                    AND(ld.LEAD_ID   = p_leadId 
                        OR (
                                ld.MOBILEPHONE1         = p_MOBILE
                            AND ld.FULLNAME_UPCASE      = p_FULLNAME
                            AND ld.NATIONALID           = p_NATIONALID
                            )
                        ) 
                    AND le.CAMPAIGN_ID = p_campaignId fetch first 1 rows only;
        ELSE
            BEGIN
            SELECT count(*) into v_count 
                FROM MBLP.LM_LEAD_DETAILS ld , MBLP.LM_LEAD le
               WHERE   le.LEAD_ID     = ld.LEAD_ID 
                   AND ld.NATIONALID  = p_NATIONALID
                   AND le.CAMPAIGN_ID = p_campaignId;
            IF v_count > 0 THEN 
                OPEN p_tranDetail FOR
                    SELECT  ld.LEAD_ID,
                        le.CAMPAIGN_ID,
                        le.STATUS_ID,
                        ld.FULLNAME,
                        TO_CHAR(ld.DOB,'YYYYMMDD') AS DOB,
                        ld.NATIONALID,
                        ld.MOBILEPHONE1,
                        ld.EMAIL,
                        ld.ACADEMIC_LEVEL,
                        ld.MARTIAL_STATUS,
                        ld.SECURITY_ANSWER,
                        ld.ADDRESS_PERMANENT,
                        ld.PROVINCE_PERMANENT,
                        ld.DISTRICT_PERMANENT,
                        ld.WARD_PERMANENT,
                        ld.ADDRESS_CURRENT,
                        ld.PROVINCE_TEMPORARY,
                        ld.DISTRICT_TEMPORARY,
                        ld.WARD_TEMPORARY,
                        ld.POSITION,
                        ld.COMPANY,
                        ld.WORKING_STATUS,
                        ld.BUSINESS_GROUP,
                        ld.BUSINESS_TYPE,
                        ld.WORKING_PROVINCE,
                        ld.WORKING_DISTRICT,
                        ld.WORKING_WARD,
                        ld.WORKING_STREET,
                        ld.WORKING_PHONE_NUMBER,
                        ld.RECEIVING_SALARY_TYPE,
                        ld.INCOME,
                        ld.CARD_RECEIVINGADDRESS,
                        ld.BRANCH_CODE,
                        ld.STAFF_CODE,
                        ld.RECOMMENDED_CREDIT_LIMIT,
                        ld.CLIENT_TIER,
                        ld.NEED_EKYC,
                        ld.NEED_ESIGN,
                        ld.CONTRACTPRODUCTCODE,
                        TO_CHAR(le.CREATED_DATE,'YYYYMMDDHH24MISS') AS CREATED_DATE,
                        TO_CHAR(ld.ISSUED_DATE,'YYYYMMDD') AS ISSUED_DATE,
                        ld.ISSUED_BY,
                        ld.GENDER,
                        ld.RESIDENCE_DURATION_M,
                        ld.RESIDENCE_DURATION,
                        ld.NUMBER_OF_DEPENDENTS,
                        ld.REF1_REL_WITH_CARDHOLDER,
                        ld.REF1_FULLNAME,
                        ld.REF1_PHONE,
                        ld.REF2_REL_WITH_CARDHOLDER,
                        ld.REF2_FULLNAME,
                        ld.REF2_PHONE,
                        ld.DOCUMENT_FILE,
                        ld.WORKING_POSITION
                    FROM    MBLP.LM_LEAD_DETAILS ld , MBLP.LM_LEAD le
                    WHERE   le.LEAD_ID = ld.LEAD_ID
                        AND ld.NATIONALID  = p_NATIONALID 
                        AND le.CAMPAIGN_ID = p_campaignId fetch first 1 rows only;
                p_resultType := 'semi_matched';
            ELSE 
                BEGIN
                SELECT count(*) into v_count 
                    FROM MBLP.LM_LEAD_DETAILS ld , MBLP.LM_LEAD le
                    WHERE   le.LEAD_ID = ld.LEAD_ID
                        AND ld.MOBILEPHONE1         = p_MOBILE  
                        AND ld.FULLNAME_UPCASE      = p_FULLNAME
                        AND le.CAMPAIGN_ID = p_campaignId;
                IF v_count > 0 THEN
                    OPEN p_tranDetail FOR
                        SELECT  ld.LEAD_ID,
                            le.CAMPAIGN_ID,
                            le.STATUS_ID,
                            ld.FULLNAME,
                            TO_CHAR(ld.DOB,'YYYYMMDD') AS DOB,
                            ld.NATIONALID,
                            ld.MOBILEPHONE1,
                            ld.EMAIL,
                            ld.ACADEMIC_LEVEL,
                            ld.MARTIAL_STATUS,
                            ld.SECURITY_ANSWER,
                            ld.ADDRESS_PERMANENT,
                            ld.PROVINCE_PERMANENT,
                            ld.DISTRICT_PERMANENT,
                            ld.WARD_PERMANENT,
                            ld.ADDRESS_CURRENT,
                            ld.PROVINCE_TEMPORARY,
                            ld.DISTRICT_TEMPORARY,
                            ld.WARD_TEMPORARY,
                            ld.POSITION,
                            ld.COMPANY,
                            ld.WORKING_STATUS,
                            ld.BUSINESS_GROUP,
                            ld.BUSINESS_TYPE,
                            ld.WORKING_PROVINCE,
                            ld.WORKING_DISTRICT,
                            ld.WORKING_WARD,
                            ld.WORKING_STREET,
                            ld.WORKING_PHONE_NUMBER,
                            ld.RECEIVING_SALARY_TYPE,
                            ld.INCOME,
                            ld.CARD_RECEIVINGADDRESS,
                            ld.BRANCH_CODE,
                            ld.STAFF_CODE,
                            ld.RECOMMENDED_CREDIT_LIMIT,
                            ld.CLIENT_TIER,
                            ld.NEED_EKYC,
                            ld.NEED_ESIGN,
                            ld.CONTRACTPRODUCTCODE,
                            TO_CHAR(le.CREATED_DATE,'YYYYMMDDHH24MISS') AS CREATED_DATE,
                            TO_CHAR(ld.ISSUED_DATE,'YYYYMMDD') AS ISSUED_DATE,
                            ld.ISSUED_BY,
                            ld.GENDER,
                            ld.RESIDENCE_DURATION_M,
                            ld.RESIDENCE_DURATION,
                            ld.NUMBER_OF_DEPENDENTS,
                            ld.REF1_REL_WITH_CARDHOLDER,
                            ld.REF1_FULLNAME,
                            ld.REF1_PHONE,
                            ld.REF2_REL_WITH_CARDHOLDER,
                            ld.REF2_FULLNAME,
                            ld.REF2_PHONE,
                            ld.DOCUMENT_FILE,
                            ld.WORKING_POSITION
                        FROM    MBLP.LM_LEAD_DETAILS ld , MBLP.LM_LEAD le
                        WHERE   le.LEAD_ID              = ld.LEAD_ID 
                            AND ld.MOBILEPHONE1         = p_MOBILE
                            AND ld.FULLNAME_UPCASE      = p_FULLNAME 
                            AND le.CAMPAIGN_ID = p_campaignId fetch first 1 rows only;
                    p_resultType := 'semi_matched';
                ELSE 
                    p_error := '1007';
                    p_resultType := 'Not_Found';
                END IF;
                END;
            END IF;
            END;
        END IF;
        
        IF p_resultType = 'fully_matched'
        THEN
            SELECT CLIENT_TIER,LEAD_FLOW_EXPIRED_DATE 
                INTO    v_client_tier,v_expired_date 
                FROM    MBLP.LM_LEAD_DETAILS 
                WHERE   LEAD_ID = p_leadId
                    OR (    NATIONALID          = p_NATIONALID 
                        AND MOBILEPHONE1        = p_MOBILE 
                        AND FULLNAME_UPCASE     = p_FULLNAME) fetch first 1 rows only;
                        
            IF v_client_tier <> 'F3' AND v_expired_date < TO_DATE(sysdate)
            THEN
                p_error := '1007';
                p_resultType := 'Not_Found';
            END IF;
        END IF;
        
        EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            p_error := '1007';
            p_resultType := 'Not_Found';
        WHEN OTHERS
        THEN
            p_error :=
                   SQLCODE
                || SQLERRM
                || ' '
                || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
            p_resultType := 'Not_Found'; 
    END pro_check_eligible;
    
    PROCEDURE pro_check_exist_lead     (p_campaignId                     IN         VARCHAR2,
                                        p_nationalId                     IN         VARCHAR2,
                                        p_leadStatusId                   IN         VARCHAR2,
                                        p_exist                             OUT     BOOLEAN,
                                        p_limitCreatedDate               IN         NUMBER,
                                        p_contractProductCode            IN			VARCHAR2,
                                        p_leadNotExist                   IN         VARCHAR2)
    AS
        v_count    NUMBER;
        CURSOR v_dateTimes IS 
            SELECT TO_NUMBER(SUBSTR((SYSDATE - le.CREATED_DATE),9,2),'99') AS date_value,
                   le.STATUS_ID AS status_id
            FROM MBLP.LM_LEAD le, MBLP.LM_LEAD_DETAILS ld
            WHERE   le.CAMPAIGN_ID = p_campaignId 
                AND ld.NATIONALID = p_nationalId
                AND ld.CONTRACTPRODUCTCODE = p_contractProductCode
                AND le.LEAD_ID = ld.LEAD_ID
                AND le.STATUS_ID = p_leadStatusId;
                
    BEGIN
        p_exist := FALSE;
        
        SELECT count(*) INTO v_count 
            FROM MBLP.LM_LEAD 
            WHERE CAMPAIGN_ID = p_campaignId;
        IF v_count != 0 
        THEN
            SELECT count(*) INTO v_count 
                FROM MBLP.LM_LEAD le, MBLP.LM_LEAD_DETAILS ld 
                WHERE   le.CAMPAIGN_ID = p_campaignId 
                    AND ld.NATIONALID = p_nationalId
                    AND le.LEAD_ID = ld.LEAD_ID
                    AND ld.CONTRACTPRODUCTCODE = p_contractProductCode;
            IF v_count > 0
            THEN
                FOR v_date IN v_dateTimes
                LOOP
                        IF v_date.date_value <= p_limitCreatedDate THEN
                            IF INSTR(p_leadNotExist,v_date.status_id) = 0
                            THEN
                                p_exist := TRUE;
                            END IF;
                        END IF;
                END LOOP;   
            END IF;
        END IF;
    END pro_check_exist_lead;
    
   PROCEDURE pro_insert_or_update_lead ( p_actionCode				     IN         VARCHAR2,
                                        p_contactId                      IN         VARCHAR2,
                                        p_leadId                         IN         VARCHAR2,
                                        p_campaignId                     IN         VARCHAR2,
                                        p_leadStatusId                   IN         VARCHAR2,
                                        p_fullName                       IN         VARCHAR2,
                                        p_fullNameUpCase                 IN         VARCHAR2,
                                        p_nationalId                     IN         VARCHAR2,
                                        p_phone                          IN         VARCHAR2,
                                        p_email                          IN         VARCHAR2,
                                        p_paymentAccount                 IN         VARCHAR2,
                                        p_internationalDebitCard         IN         VARCHAR2,
                                        p_domesticDebitCard              IN         VARCHAR2,
                                        p_eBanking                       IN         VARCHAR2,
                                        p_cif                            IN         VARCHAR2,
                                        p_academicLevel                  IN         VARCHAR2,
                                        p_residenceStatus                IN         VARCHAR2,
                                        p_nationalType                   IN         VARCHAR2,
                                        p_issuedDate                     IN         VARCHAR2,
                                        p_issuedBy                       IN         VARCHAR2,
                                        p_residenceDurationM             IN         VARCHAR2,
                                        p_residenceDurationY             IN         VARCHAR2,
                                        p_gender                         IN         VARCHAR2,
                                        p_martialStatus                  IN         VARCHAR2,
                                        p_pin                            IN         VARCHAR2,
                                        p_numberOfDependents             IN         VARCHAR2,
                                        p_cardPrintedName                IN         VARCHAR2,
                                        p_dob                            IN         VARCHAR2,
                                        p_Nationality                    IN         VARCHAR2,
                                        p_MobilePhone1                   IN         VARCHAR2,
                                        p_MobilePhone2                   IN         VARCHAR2,
                                        p_insuranceNumber                IN         VARCHAR2,
                                        p_expDateOfResidence             IN         VARCHAR2,
                                        p_addressPermanent               IN         VARCHAR2,
                                        p_provincePermanent              IN         VARCHAR2,
                                        p_districtPermanent              IN         VARCHAR2,
                                        p_wardPermanent                  IN         VARCHAR2,
                                        p_addressCurrent                 IN         VARCHAR2,
                                        p_sameAsPermanent                IN         VARCHAR2,
                                        p_provinceTemporary              IN         VARCHAR2,
                                        p_districtTemporary              IN         VARCHAR2,
                                        p_wardTemporary                  IN         VARCHAR2,
                                        p_HdbEmployee                    IN         VARCHAR2,
                                        p_employeeId                     IN         VARCHAR2,
                                        p_position                       IN         VARCHAR2,
                                        p_taxNumber                      IN         VARCHAR2,
                                        p_company                        IN         VARCHAR2,
                                        p_workingStatus                  IN         VARCHAR2,
                                        p_businessGroup                  IN         VARCHAR2,
                                        p_businessType                   IN         VARCHAR2,
                                        p_industry                       IN         VARCHAR2,
                                        p_workingProvince                IN         VARCHAR2,
                                        p_workingDistrict                IN         VARCHAR2,
                                        p_workingWard                    IN         VARCHAR2,
                                        p_workingAddress                 IN         VARCHAR2,
                                        p_workingDepartment              IN         VARCHAR2,
                                        p_workingPosition                IN         VARCHAR2,
                                        p_wokringPhone                   IN         VARCHAR2,
                                        p_wokringPhoneExt                IN         VARCHAR2,
                                        p_workingBusinessDuration        IN         VARCHAR2,
                                        p_charterCapital                 IN         VARCHAR2,
                                        p_numberOfEmployees              IN         VARCHAR2,
                                        p_businessSize                   IN         VARCHAR2,
                                        p_durationOfWork                 IN         VARCHAR2,
                                        p_laborContractType              IN         VARCHAR2,
                                        p_receivingSalaryType            IN         VARCHAR2,
                                        p_income                         IN         VARCHAR2,
                                        p_otherIncome                    IN         VARCHAR2,
                                        p_otherIncomeDescription         IN         VARCHAR2,
                                        p_monthlyExpenses                IN         VARCHAR2,
                                        p_HdbCreditLimit                 IN         VARCHAR2,
                                        p_TransactionWithHDBank          IN         VARCHAR2,
                                        p_deposit                        IN         VARCHAR2,
                                        p_loan                           IN         VARCHAR2,
                                        p_maxCic                         IN         VARCHAR2,
                                        p_12Months                       IN         VARCHAR2,
                                        p_24Months                       IN         VARCHAR2,
                                        p_36Months                       IN         VARCHAR2,
                                        p_current                        IN         VARCHAR2,
                                        p_outstandingLoan                IN         VARCHAR2,
                                        p_totalCreditcard                IN			VARCHAR2,
                                        p_ref1RelWithCardholder          IN			VARCHAR2,
                                        p_ref1Fullname                   IN			VARCHAR2,
                                        p_ref1NationalID                 IN			VARCHAR2,
                                        p_ref1MobilePhone                IN			VARCHAR2,
                                        p_ref1Company                    IN			VARCHAR2,
                                        p_ref1Address                    IN			VARCHAR2,
                                        p_ref2RelWithCardholder          IN			VARCHAR2,
                                        p_ref2Fullname                   IN			VARCHAR2,
                                        p_ref2NationalID                 IN			VARCHAR2,
                                        p_ref2MobilePhone                IN			VARCHAR2,
                                        p_ref2Company                    IN			VARCHAR2,
                                        p_ref2Address                    IN			VARCHAR2,
                                        p_primaryOrSecondaryCard         IN			VARCHAR2,
                                        p_targetCustomer                 IN			VARCHAR2,
                                        p_cardPolicy                     IN			VARCHAR2,
                                        p_applicationPeriod              IN			VARCHAR2,
                                        p_fromDate                       IN			VARCHAR2,
                                        p_toDate                         IN			VARCHAR2,
                                        p_mainContractNumber             IN			VARCHAR2,
                                        p_contractProductCode            IN			VARCHAR2,
                                        p_cardStampingAddress            IN			VARCHAR2,
                                        p_cardStampingDateTime           IN			VARCHAR2,
                                        p_cardReceivingAddress           IN			VARCHAR2,
                                        p_PinMailer                      IN			VARCHAR2,
                                        p_cardStatementSendingType       IN			VARCHAR2,
                                        p_cardStatementReceiving         IN			VARCHAR2,
                                        p_monthlyStatementDate           IN			VARCHAR2,
                                        p_autoDebtDeduction              IN			VARCHAR2,
                                        p_otherCardServices              IN			VARCHAR2,
                                        p_sms                            IN			VARCHAR2,
                                        p_branchCode                     IN			VARCHAR2,
                                        p_branchName                     IN			VARCHAR2,
                                        p_staffCode                      IN			VARCHAR2,
                                        p_staffName                      IN			VARCHAR2,
                                        p_amlFatcaResident               IN			VARCHAR2,
                                        p_amlFatcaOrganization           IN			VARCHAR2,
                                        p_amlFatcaInfluence              IN			VARCHAR2,
                                        p_amlFatcaUs                     IN			VARCHAR2,
                                        p_recommendedCreditLimit         IN			VARCHAR2,
                                        p_nationalIdFormer               IN			VARCHAR2,
                                        p_nationalTypeFormer             IN			VARCHAR2,
                                        p_documentFile                   IN			VARCHAR2,
                                        p_browser                        IN         VARCHAR2,
                                        p_urlFull                        IN         VARCHAR2,
                                        p_flowType                       IN         VARCHAR2,
                                        p_clientSupportDocType           IN         VARCHAR2,
                                        p_needUpdateDoc                  IN         VARCHAR2,
                                        p_cicS37Time                     IN         VARCHAR2,
                                        p_cicS37                         IN         VARCHAR2,
                                        p_cicR11aTime                    IN         VARCHAR2,
                                        p_cicR11a                        IN         VARCHAR2,
                                        p_cicR14Time                     IN         VARCHAR2,
                                        p_cicR14                         IN         VARCHAR2,
                                        p_creditScore                    IN         VARCHAR2,
                                        p_fraudScore                     IN         VARCHAR2,
                                        p_ekycLivenessCheckStatus        IN         VARCHAR2,
                                        p_ekycLivenessCheckScore         IN         VARCHAR2,
                                        p_ekycLivenessRequestTime        IN         VARCHAR2,
                                        p_ekycFaceMatchingStatus         IN         VARCHAR2,
                                        p_ekycFaceMatchingScore          IN         VARCHAR2,
                                        p_ekycFaceMatchingRequestTime    IN         VARCHAR2,
                                        p_ekycRetrievalData              IN         VARCHAR2,
                                        p_ekycRetrievalStatus            IN         VARCHAR2,
                                        p_ekycRetrievalRequestTime       IN         VARCHAR2,
                                        p_ekycIdVerdict                  IN         VARCHAR2,
                                        p_ekycIdVerdictDetails           IN         VARCHAR2,
                                        p_ocrRequestTime                 IN         VARCHAR2,
                                        p_ocrDetails                     IN         VARCHAR2,
                                        p_simFullName                    IN         VARCHAR2,
                                        p_simDob                         IN         VARCHAR2,
                                        p_simId                          IN         VARCHAR2,
                                        p_simPhoneNumber                 IN         VARCHAR2,
                                        p_simUpdatedAt                   IN         VARCHAR2,
                                        p_siFullName                     IN         VARCHAR2,
                                        p_siCompanyName                  IN         VARCHAR2,
                                        p_siCompanyTaxId                 IN         VARCHAR2,
                                        p_siDateOfBirth                  IN         VARCHAR2,
                                        p_siIdNumber                     IN         VARCHAR2,
                                        p_siLastPaidDate                 IN         VARCHAR2,
                                        p_siSalary                       IN         VARCHAR2,
                                        p_siNumber                       IN         VARCHAR2,
                                        p_pitIdValue                     IN         VARCHAR2,
                                        p_pitNumber                      IN         VARCHAR2,
                                        p_pitFullname                    IN         VARCHAR2,
                                        p_pitDob                         IN         VARCHAR2,
                                        p_pitGender                      IN         VARCHAR2,
                                        p_pitCompanyId                   IN         VARCHAR2,
                                        p_pitIncomeType                  IN         VARCHAR2,
                                        p_pitIncome                      IN         VARCHAR2,
                                        p_pitTotalIncome                 IN         VARCHAR2,
                                        p_otherNid3rp                    IN         VARCHAR2,
                                        p_detailsApprovalRules           IN         VARCHAR2,
                                        p_approvalStatus                 IN         VARCHAR2,
                                        p_rejectedCode                   IN         VARCHAR2,
                                        p_rejectedReason                 IN         VARCHAR2,
                                        p_error                                OUT  VARCHAR2,
                                        p_limitCreatedDate               IN         NUMBER,
                                        p_securityAnswer                 IN         VARCHAR2,
                                        p_clientTier                     IN         VARCHAR2,
                                        p_utmSource                      IN         VARCHAR2,
                                        p_utmCampaign                    IN         VARCHAR2,
                                        p_utmRef                         IN         VARCHAR2,
                                        p_leadNotExist                   IN         VARCHAR2)
    AS
        p_exist             BOOLEAN;
        v_count             NUMBER;
        p_campaignExist     BOOLEAN;
        
        v_file_arr          JSON_ARRAY_T;
        v_file_obj          JSON_OBJECT_T;
        v_file_id           VARCHAR2(100);
        v_file_name         VARCHAR2(100);
        v_file_directory    VARCHAR2(3000);
        v_file_type         VARCHAR2(100);
        
        v_file_id_exist     BOOLEAN;
        
        CURSOR v_file_id_array IS SELECT FILE_ID FROM MBLP.LM_ATTACHMENT WHERE LEAD_ID = p_leadId;
    BEGIN
        p_error := '000000';
        pro_check_campaignId(p_campaignId => p_campaignId, p_campaignExist => p_campaignExist);
        IF p_actionCode = 'INSERT' AND p_campaignExist = TRUE
        THEN
            pro_check_exist_lead     (  p_campaignId => p_campaignId,
                                        p_nationalId => p_nationalId,
                                        p_leadStatusId => p_leadStatusId,
                                        p_exist => p_exist,
                                        p_limitCreatedDate => p_limitCreatedDate,
                                        p_contractProductCode => p_contractProductCode,
                                        p_leadNotExist => p_leadNotExist);
            IF p_exist = FALSE
            THEN
                SELECT count(*) INTO v_count 
                    FROM  MBLP.LM_CONTACT
                    WHERE GLOBAL_ID = p_nationalId;
                IF v_count = 0 THEN
                    INSERT INTO MBLP.LM_CONTACT (CONTACT_ID,
                                                 NAME,
                                                 MOBILE,
                                                 EMAIL,
                                                 GLOBAL_ID,
                                                 GLOBAL_TYPE)
                                         VALUES (p_contactId,
                                                 p_fullName,
                                                 p_phone,
                                                 p_email,
                                                 p_nationalId,
                                                 p_nationalType);
                ELSE
                    UPDATE      MBLP.LM_CONTACT 
                        SET     NAME        = p_fullName,
                                MOBILE      = p_phone,
                                EMAIL       = p_email,
                                GLOBAL_TYPE = p_nationalType,
                                UPDATE_DATE = SYSDATE
                        WHERE   GLOBAL_ID   = p_nationalId;
                                                 
                END IF;
                SELECT count(*) INTO v_count FROM MBLP.LM_LEAD WHERE LEAD_ID = p_leadId;
                IF v_count != 0 THEN
                    p_error := '1009'; -- leadId da ton tai
                ELSE
                    INSERT INTO MBLP.LM_LEAD(LEAD_ID,
                                     CAMPAIGN_ID,   
                                     STATUS_ID,
                                     CREATED_DATE,
                                     CONTACT_ID,
                                     BROWSER,
                                     URL_FULL,
                                     UTM_SOURCE,
                                     UTM_CAMPAIGN,
                                     UTM_REFERRAL)
                            VALUES  (p_leadId,
                                     p_campaignId,
                                     p_leadStatusId,
                                     SYSDATE,
                                     p_contactId,
                                     p_browser,
                                     p_urlFull,
                                     p_utmSource,
                                     p_utmCampaign,
                                     p_utmRef);
                                     
                    INSERT INTO MBLP.LM_LEAD_DETAILS (LEAD_ID,
                                              FULLNAME,
                                              FULLNAME_UPCASE,
                                              DOB,
                                              NATIONALID,
                                              EMAIL,
                                              PAYMENT_ACCOUNT,
                                              INTERNATIONAL_DEBIT_CARD,
                                              DOMESTIC_DEBIT_CARD,
                                              EBANKING,
                                              --CIF,
                                              ACADEMIC_LEVEL,
                                              RESIDENCE_STATUS,
                                              NATIONALID_TYPE,
                                              ISSUED_DATE,
                                              ISSUED_BY,
                                              RESIDENCE_DURATION_M,
                                              RESIDENCE_DURATION,
                                              GENDER,
                                              MARTIAL_STATUS,
                                              PIN,
                                              NUMBER_OF_DEPENDENTS,
                                              CARD_PRINTEDNAME,
                                              NATIONALITY,
                                              MOBILEPHONE1,
                                              MOBILEPHONE2,
                                              INSURANCE_NUMBER,
                                              EXPDATEOFRESIDENCE,
                                              ADDRESS_PERMANENT,
                                              PROVINCE_PERMANENT,
                                              DISTRICT_PERMANENT,
                                              WARD_PERMANENT,
                                              ADDRESS_CURRENT,
                                              SAMEASPERMANENT,
                                              PROVINCE_TEMPORARY,
                                              DISTRICT_TEMPORARY,
                                              WARD_TEMPORARY,
                                              HDB_EMPLOYEE,
                                              EMPLOYEEID,
                                              POSITION,
                                              TAX_CODE,
                                              COMPANY,
                                              WORKING_STATUS,
                                              BUSINESS_GROUP,
                                              BUSINESS_TYPE,
                                              INDUSTRY,
                                              WORKING_PROVINCE,
                                              WORKING_DISTRICT,
                                              WORKING_WARD,
                                              WORKING_STREET,
                                              WORKING_DEPARTMENT,
                                              WORKING_POSITION,
                                              WORKING_PHONE_NUMBER,
                                              WORKING_PHONEEXT,
                                              WORKING_BUSINESSDURATION,
                                              CHARTER_CAPITAL,
                                              NUMBER_OF_EMPLOYEES,
                                              BUSINESS_SIZE,
                                              DURATIONOFWORK,
                                              LABOR_CONTRACT_TYPE,
                                              RECEIVING_SALARY_TYPE,
                                              INCOME,
                                              OTHER_INCOME,
                                              OTHER_INCOME_DESCIPTION,
                                              MONTHLY_EXPENSES,
                                              HDB_CREDIT_LIMIT,
                                              TRANSACTION_WITH_HDBANK,
                                              DEPOSIT,
                                              LOAN,
                                              MAX_CIC,
                                              MONTHS12,
                                              MONTHS24,
                                              MONTHS36,
                                              LEAD_DETAILS_CURRENT,
                                              OUTSTANDING,
                                              TOTAL_CREDITCARD_OUTSTANDING,
                                              REF1_REL_WITH_CARDHOLDER,
                                              REF1_FULLNAME,
                                              REF1_NATIONALID,
                                              REF1_PHONE,
                                              REF1_COMPANY,
                                              REF1_ADDRESS,
                                              REF2_REL_WITH_CARDHOLDER,
                                              REF2_FULLNAME,
                                              REF2_NATIONALID,
                                              REF2_PHONE,
                                              REF2_COMPANY,
                                              REF2_ADDRESS,
                                              PRIMARY_SECONDARY_CARD,
                                              TARGET_CUSTOMER,
                                              --CARD_POLICY,
                                              APPLICATION_PERIOD,
                                              FROMDATE,
                                              TODATE,
                                              MAINCONTRACT_NUMBER,
                                              CONTRACTPRODUCTCODE,
                                              CARDSTAMPING_ADDRESS,
                                              CARDSTAMPING_DATETIME,
                                              CARD_RECEIVINGADDRESS,
                                              PIN_MAILER,
                                              STATEMENT_SENDING_TYPE,
                                              STATEMENT_RECEIVING_ADDR,
                                              MONTHLYSTATEMENT_DATE,
                                              AUTO_DEBT_DEDUCTION,
                                              OTHER_CARD_SERVICES,
                                              SMS,
                                              BRANCH_CODE,
                                              BRANCH_NAME,
                                              --STAFF_CODE,
                                              --STAFF_NAME,
                                              AML_FATCA_RESIDENT,
                                              AML_FATCA_ORGANIZATION,
                                              AML_FATCA_INFLUENCE,
                                              AML_FATCA_US,
                                              RECOMMENDED_CREDIT_LIMIT,
                                              FORMER_NATIONAL_ID,
                                              FORMER_NATIONAL_TYPE,
                                              DOCUMENT_FILE,
                                              FLOW_TYPE,
                                              CLIENT_SUPPORT_DOC_TYPE,
                                              NEED_UPDATE_DOC,
                                              SECURITY_ANSWER,
                                              CLIENT_TIER)
                                    VALUES  ( p_leadId,     
                                              p_fullName,      
                                              p_fullNameUpCase,
                                              TO_DATE(p_dob, 'YYYYMMDD'),         
                                              p_nationalId,           
                                              p_email,      
                                              p_paymentAccount,      
                                              p_internationalDebitCard,      
                                              p_domesticDebitCard,      
                                              p_eBanking,      
                                              --p_cif,      
                                              p_academicLevel,      
                                              p_residenceStatus,      
                                              p_nationalType,      
                                              TO_DATE(p_issuedDate, 'YYYYMMDD'),      
                                              p_issuedBy,      
                                              p_residenceDurationM,      
                                              p_residenceDurationY,      
                                              p_gender,      
                                              p_martialStatus,      
                                              p_pin,      
                                              p_numberOfDependents,      
                                              p_cardPrintedName,         
                                              p_Nationality,      
                                              p_phone,      
                                              p_MobilePhone2,      
                                              p_insuranceNumber,      
                                              TO_DATE(p_expDateOfResidence, 'YYYYMMDD'),      
                                              p_addressPermanent,      
                                              p_provincePermanent,      
                                              p_districtPermanent,      
                                              p_wardPermanent,      
                                              p_addressCurrent,      
                                              p_sameAsPermanent,      
                                              p_provinceTemporary,      
                                              p_districtTemporary,      
                                              p_wardTemporary,      
                                              p_HdbEmployee,      
                                              p_employeeId,      
                                              p_position,      
                                              p_taxNumber,      
                                              p_company,      
                                              p_workingStatus,      
                                              p_businessGroup,      
                                              p_businessType,      
                                              p_industry,      
                                              p_workingProvince,      
                                              p_workingDistrict,      
                                              p_workingWard,      
                                              p_workingAddress,      
                                              p_workingDepartment,      
                                              p_workingPosition,      
                                              p_wokringPhone,      
                                              p_wokringPhoneExt,      
                                              p_workingBusinessDuration,      
                                              p_charterCapital,      
                                              p_numberOfEmployees,      
                                              p_businessSize,      
                                              p_durationOfWork,      
                                              p_laborContractType,      
                                              p_receivingSalaryType,      
                                              p_income,      
                                              p_otherIncome,      
                                              p_otherIncomeDescription,      
                                              p_monthlyExpenses,      
                                              p_HdbCreditLimit,      
                                              p_TransactionWithHDBank,      
                                              p_deposit,      
                                              p_loan,      
                                              p_maxCic,      
                                              p_12Months,      
                                              p_24Months,      
                                              p_36Months,      
                                              p_current,      
                                              p_outstandingLoan,      
                                              p_totalCreditcard,
                                              p_ref1RelWithCardholder,
                                              p_ref1Fullname,
                                              p_ref1NationalID,
                                              p_ref1MobilePhone,
                                              p_ref1Company,
                                              p_ref1Address,
                                              p_ref2RelWithCardholder,
                                              p_ref2Fullname,
                                              p_ref2NationalID,
                                              p_ref2MobilePhone,
                                              p_ref2Company,
                                              p_ref2Address,
                                              p_primaryOrSecondaryCard,
                                              p_targetCustomer,
                                              --p_cardPolicy,
                                              p_applicationPeriod,
                                              TO_DATE(p_fromDate, 'YYYYMMDD'),
                                              TO_DATE(p_toDate, 'YYYYMMDD'),
                                              p_mainContractNumber,
                                              p_contractProductCode,
                                              p_cardStampingAddress,
                                              TO_TIMESTAMP (p_cardStampingDateTime, 'YYYYMMDDHH24MISS.FF9'),
                                              p_cardReceivingAddress,
                                              p_PinMailer,
                                              p_cardStatementSendingType,
                                              p_cardStatementReceiving,
                                              p_monthlyStatementDate,
                                              p_autoDebtDeduction,
                                              p_otherCardServices,
                                              p_sms,
                                              p_branchCode,
                                              p_branchName,
                                              --p_staffCode,
                                              --p_staffName,
                                              p_amlFatcaResident,
                                              p_amlFatcaOrganization,
                                              p_amlFatcaInfluence,
                                              p_amlFatcaUs,
                                              p_recommendedCreditLimit,
                                              p_nationalIdFormer,
                                              p_nationalTypeFormer,
                                              p_documentFile,
                                              p_flowType,
                                              p_clientSupportDocType,
                                              p_needUpdateDoc,
                                              p_securityAnswer,
                                              p_clientTier);
                    
                    BEGIN
                    v_file_arr := JSON_ARRAY_T(p_documentFile);
                    
                    FOR INDX IN 0 .. v_file_arr.GET_SIZE - 1
                    LOOP
                        v_file_obj          := TREAT (v_file_arr.GET (INDX) AS JSON_OBJECT_T);
                        v_file_id           := REPLACE ( v_file_obj.GET ('id').TO_STRING,'"');
                        v_file_name         := REPLACE ( v_file_obj.GET ('filename').TO_STRING,'"');
                        v_file_type         := REPLACE ( v_file_obj.GET ('type').TO_STRING,'"');
                        
                        v_file_directory    := REPLACE ( v_file_obj.GET ('directory').TO_STRING,'"');
                        INSERT INTO MBLP.LM_ATTACHMENT( LEAD_ID,
                                                        FILE_ID,
                                                        TYPE,
                                                        DIRECTORY,
                                                        FILENAME)
                                                VALUES( p_leadId,
                                                        v_file_id,
                                                        v_file_type,
                                                        v_file_directory,
                                                        v_file_name);
                    END LOOP;
                    EXCEPTION  --DocumentFile is null or = []
                    WHEN OTHERS
                    THEN
                        v_file_id_exist := FALSE;
                    END;
    
                    INSERT INTO MBLP.LM_LEAD_QUALIFIED(
                                              LEAD_ID,
                                              CICS37_TIME,
                                              CIC_S37,
                                              CIC_R11A_TIME,
                                              CIC_R11A,
                                              CIC_R14_TIME,
                                              CIC_R14,
                                              CREDIT_SCORE,
                                              FRAUD_SCORE,
                                              EKYC_LIVENESS_CHECK_STATUS,
                                              EKYC_LIVENESS_CHECK_SCORE,
                                              EKYC_LIVENESS_REQUEST_TIME,
                                              EKYC_FACEMATCHING_STATUS,
                                              EKYC_FACEMATCHING_SCORE,
                                              EKYC_FACEMATCHING_REQUEST_TIME,
                                              EKYC_RETRIEVAL_DATA,
                                              EKYC_RETRIEVAL_STATUS,
                                              EKYC_RETRIEVAL_REQUEST_TIME,
                                              EKYC_IDCARD_TAMPERING_VERDICT,
                                              EKYC_IDCARD_TAMPERING_DETAILS,
                                              OCR_REQUESTTIME,
                                              OCR_DETAILS,
                                              SIM_FULLNAME,
                                              SIM_DOB,
                                              SIM_ID,
                                              SIM_PHONENUMBER,
                                              SIM_UPDATEDAT,
                                              SI_FULLNAME,
                                              SI_COMPANYNAME,
                                              SI_COMPANYTAXID,
                                              SI_DATEOFBIRTH,
                                              SI_IDNUMBER,
                                              SI_LASTPAIDDATE,
                                              SI_SALARY,
                                              SI_NUMBER,
                                              PIT_IDVALUE,
                                              PIT_NUMBER,
                                              PIT_FULLNAME,
                                              PIT_DOB,
                                              PIT_GENDER,
                                              PIT_COMPANYID,
                                              PIT_INCOMETYPE,
                                              PIT_INCOME,
                                              PIT_TOTAL_INCOME,
                                              OTHER_NID_3RP,
                                              DETAILS_APPROVAL_RULES,
                                              APPROVAL_STATUS,
                                              REJECTED_CODE,
                                              REJECTED_REASON) 
                                      VALUES (p_leadId,
                                              TO_TIMESTAMP (p_cicS37Time, 'YYYYMMDDHH24MISS.FF9'),
                                              p_cicS37,
                                              TO_TIMESTAMP (p_cicR11aTime, 'YYYYMMDDHH24MISS.FF9'),
                                              p_cicR11a,
                                              TO_TIMESTAMP (p_cicR14Time, 'YYYYMMDDHH24MISS.FF9'),
                                              p_cicR14,
                                              p_creditScore,
                                              p_fraudScore,
                                              p_ekycLivenessCheckStatus,
                                              p_ekycLivenessCheckScore,
                                              TO_TIMESTAMP (p_ekycLivenessRequestTime, 'YYYYMMDDHH24MISS.FF9'),
                                              p_ekycFaceMatchingStatus,
                                              p_ekycFaceMatchingScore,
                                              TO_TIMESTAMP (p_ekycFaceMatchingRequestTime, 'YYYYMMDDHH24MISS.FF9'),
                                              p_ekycRetrievalData,
                                              p_ekycRetrievalStatus,
                                              TO_TIMESTAMP (p_ekycRetrievalRequestTime, 'YYYYMMDDHH24MISS.FF9'),
                                              p_ekycIdVerdict,
                                              p_ekycIdVerdictDetails,
                                              TO_TIMESTAMP (p_ocrRequestTime, 'YYYYMMDDHH24MISS.FF9'),
                                              p_ocrDetails,
                                              p_simFullName,
                                              TO_DATE(p_simDob, 'YYYYMMDD'),
                                              p_simId,
                                              p_simPhoneNumber,
                                              TO_TIMESTAMP (p_simUpdatedAt, 'YYYYMMDDHH24MISS.FF9'),
                                              p_siFullName,
                                              p_siCompanyName,
                                              p_siCompanyTaxId,
                                              TO_DATE(p_siDateOfBirth, 'YYYYMMDD'),
                                              p_siIdNumber,
                                              TO_DATE(p_siLastPaidDate, 'YYYYMM'),
                                              p_siSalary,
                                              p_siNumber,
                                              p_pitIdValue,
                                              p_pitNumber,
                                              p_pitFullname,
                                              TO_DATE(p_pitDob, 'YYYYMMDD'),
                                              p_pitGender,
                                              p_pitCompanyId,
                                              p_pitIncomeType,
                                              p_pitIncome,
                                              p_pitTotalIncome,
                                              p_otherNid3rp,
                                              p_detailsApprovalRules,
                                              p_approvalStatus,
                                              p_rejectedCode,
                                              p_rejectedReason);
    
                    COMMIT;
                END IF;
                
            ELSE
                p_error := '1009';
            END IF;
        ELSIF p_actionCode = 'UPDATE'
        THEN
            IF p_campaignId = 'CREDITCARD_TS_DIN'
            THEN
            
                UPDATE  MBLP.LM_LEAD
                SET MODIFIED_DATE = SYSDATE,
                    STATUS_ID     = p_leadStatusId,
                    BROWSER       = p_browser,
                    URL_FULL      = p_urlFull,
                    UTM_SOURCE    = p_utmSource,
                    UTM_CAMPAIGN  = p_utmCampaign,
                    UTM_REFERRAL  = p_utmRef
              WHERE LEAD_ID       = p_leadId;
              
                UPDATE  MBLP.LM_LEAD_DETAILS
                SET FULLNAME					= p_fullName,	
                    FULLNAME_UPCASE             = p_fullNameUpCase,
                    DOB                         = TO_DATE(p_dob, 'YYYYMMDD'),
                    NATIONALID                  = p_nationalId,
                    EMAIL                       = p_email,
                    PAYMENT_ACCOUNT             = p_paymentAccount,
                    INTERNATIONAL_DEBIT_CARD    = p_internationalDebitCard,
                    DOMESTIC_DEBIT_CARD         = p_domesticDebitCard,
                    EBANKING                    = p_eBanking,
                    --CIF                         = p_cif,
                    ACADEMIC_LEVEL              = p_academicLevel,
                    RESIDENCE_STATUS            = p_residenceStatus,
                    NATIONALID_TYPE             = p_nationalType,
                    ISSUED_DATE                 = TO_DATE(p_issuedDate, 'YYYYMMDD'),
                    ISSUED_BY                   = p_issuedBy,
                    RESIDENCE_DURATION_M        = p_residenceDurationM,
                    RESIDENCE_DURATION          = p_residenceDurationY,
                    GENDER                      = p_gender,
                    MARTIAL_STATUS              = p_martialStatus,
                    PIN                         = p_pin,
                    NUMBER_OF_DEPENDENTS        = p_numberOfDependents,
                    CARD_PRINTEDNAME            = p_cardPrintedName,
                    NATIONALITY                 = p_Nationality,
                    MOBILEPHONE1                = p_phone,
                    MOBILEPHONE2                = p_MobilePhone2,
                    INSURANCE_NUMBER            = p_insuranceNumber,
                    EXPDATEOFRESIDENCE          = TO_DATE(p_expDateOfResidence, 'YYYYMMDD'),
                    ADDRESS_PERMANENT           = p_addressPermanent,
                    PROVINCE_PERMANENT          = p_provincePermanent,
                    DISTRICT_PERMANENT          = p_districtPermanent,
                    WARD_PERMANENT              = p_wardPermanent,
                    ADDRESS_CURRENT             = p_addressCurrent,
                    SAMEASPERMANENT             = p_sameAsPermanent,
                    PROVINCE_TEMPORARY          = p_provinceTemporary,
                    DISTRICT_TEMPORARY          = p_districtTemporary,
                    WARD_TEMPORARY              = p_wardTemporary,
                    HDB_EMPLOYEE                = p_HdbEmployee,
                    EMPLOYEEID                  = p_employeeId,
                    POSITION                    = p_position,
                    TAX_CODE                    = p_taxNumber,
                    COMPANY                     = p_company,
                    WORKING_STATUS              = p_workingStatus,
                    BUSINESS_GROUP              = p_businessGroup,
                    BUSINESS_TYPE               = p_businessType,
                    INDUSTRY                    = p_industry,
                    WORKING_PROVINCE            = p_workingProvince,
                    WORKING_DISTRICT            = p_workingDistrict,
                    WORKING_WARD                = p_workingWard,
                    WORKING_STREET              = p_workingAddress,
                    WORKING_DEPARTMENT          = p_workingDepartment,
                    WORKING_POSITION            = p_workingPosition,
                    WORKING_PHONE_NUMBER        = p_wokringPhone,
                    WORKING_PHONEEXT            = p_wokringPhoneExt,
                    WORKING_BUSINESSDURATION    = p_workingBusinessDuration,
                    CHARTER_CAPITAL             = p_charterCapital,
                    NUMBER_OF_EMPLOYEES         = p_numberOfEmployees,
                    BUSINESS_SIZE               = p_businessSize,
                    DURATIONOFWORK              = p_durationOfWork,
                    LABOR_CONTRACT_TYPE         = p_laborContractType,
                    RECEIVING_SALARY_TYPE       = p_receivingSalaryType,
                    INCOME                      = p_income,
                    OTHER_INCOME                = p_otherIncome,
                    OTHER_INCOME_DESCIPTION     = p_otherIncomeDescription,
                    MONTHLY_EXPENSES            = p_monthlyExpenses,
                    HDB_CREDIT_LIMIT            = p_HdbCreditLimit,
                    TRANSACTION_WITH_HDBANK     = p_TransactionWithHDBank,
                    DEPOSIT                     = p_deposit,
                    LOAN                        = p_loan,
                    MAX_CIC                     = p_maxCic,
                    MONTHS12                    = p_12Months,
                    MONTHS24                    = p_24Months,
                    MONTHS36                    = p_36Months,
                    LEAD_DETAILS_CURRENT        = p_current,
                    OUTSTANDING                 = p_outstandingLoan,
                    TOTAL_CREDITCARD_OUTSTANDING= p_totalCreditcard,
                    REF1_REL_WITH_CARDHOLDER    = p_ref1RelWithCardholder,
                    REF1_FULLNAME               = p_ref1Fullname,
                    REF1_NATIONALID             = p_ref1NationalID,
                    REF1_PHONE                  = p_ref1MobilePhone,
                    REF1_COMPANY                = p_ref1Company,
                    REF1_ADDRESS                = p_ref1Address,
                    REF2_REL_WITH_CARDHOLDER    = p_ref2RelWithCardholder,
                    REF2_FULLNAME               = p_ref2Fullname,
                    REF2_NATIONALID             = p_ref2NationalID,
                    REF2_PHONE                  = p_ref2MobilePhone,
                    REF2_COMPANY                = p_ref2Company,
                    REF2_ADDRESS                = p_ref2Address,
                    PRIMARY_SECONDARY_CARD      = p_primaryOrSecondaryCard,
                    TARGET_CUSTOMER             = p_targetCustomer,
                    --CARD_POLICY                 = p_cardPolicy,
                    APPLICATION_PERIOD          = p_applicationPeriod,
                    FROMDATE                    = TO_DATE(p_fromDate, 'YYYYMMDD'),
                    TODATE                      = TO_DATE(p_toDate, 'YYYYMMDD'),
                    MAINCONTRACT_NUMBER         = p_mainContractNumber,
                    CONTRACTPRODUCTCODE         = p_contractProductCode,
                    CARDSTAMPING_ADDRESS        = p_cardStampingAddress,
                    CARDSTAMPING_DATETIME       = TO_TIMESTAMP (p_cardStampingDateTime, 'YYYYMMDDHH24MISS.FF9'),
                    CARD_RECEIVINGADDRESS       = p_cardReceivingAddress,
                    PIN_MAILER                  = p_PinMailer,
                    STATEMENT_SENDING_TYPE      = p_cardStatementSendingType,
                    STATEMENT_RECEIVING_ADDR    = p_cardStatementReceiving,
                    MONTHLYSTATEMENT_DATE       = p_monthlyStatementDate,
                    AUTO_DEBT_DEDUCTION         = p_autoDebtDeduction,
                    OTHER_CARD_SERVICES         = p_otherCardServices,
                    SMS                         = p_sms,
                    BRANCH_CODE                 = p_branchCode,
                    BRANCH_NAME                 = p_branchName,
                    --STAFF_CODE                  = p_staffCode,
                    --STAFF_NAME                  = p_staffName,
                    AML_FATCA_RESIDENT          = p_amlFatcaResident,
                    AML_FATCA_ORGANIZATION      = p_amlFatcaOrganization,
                    AML_FATCA_INFLUENCE         = p_amlFatcaInfluence,
                    AML_FATCA_US                = p_amlFatcaUs,
                    RECOMMENDED_CREDIT_LIMIT    = p_recommendedCreditLimit,
                    FORMER_NATIONAL_ID          = p_nationalIdFormer,
                    FORMER_NATIONAL_TYPE        = p_nationalTypeFormer,
                    DOCUMENT_FILE               = p_documentFile,
                    FLOW_TYPE                   = p_flowType,
                    CLIENT_SUPPORT_DOC_TYPE     = p_clientSupportDocType,
                    NEED_UPDATE_DOC             = p_needUpdateDoc,
                    SECURITY_ANSWER             = p_securityAnswer
                    --CLIENT_TIER                 = p_clientTier
              WHERE LEAD_ID                     = p_leadId;
                
                IF SQL%ROWCOUNT = 0 THEN
                    p_error := '1007'; -- khong tim thay leadId de update
                END IF;
                
                UPDATE  MBLP.LM_LEAD_QUALIFIED
                SET CICS37_TIME	                        = TO_TIMESTAMP (p_cicS37Time, 'YYYYMMDDHH24MISS.FF9'),
                    CIC_S37	                            = p_cicS37,
                    CIC_R11A_TIME	                    = TO_TIMESTAMP (p_cicR11aTime, 'YYYYMMDDHH24MISS.FF9'),
                    CIC_R11A	                        = p_cicR11a,
                    CIC_R14_TIME	                    = TO_TIMESTAMP (p_cicR14Time, 'YYYYMMDDHH24MISS.FF9'),
                    CIC_R14	                            = p_cicR14,
                    CREDIT_SCORE	                    = p_creditScore,
                    FRAUD_SCORE	                        = p_fraudScore,
                    EKYC_LIVENESS_CHECK_STATUS          = p_ekycLivenessCheckStatus,
                    EKYC_LIVENESS_CHECK_SCORE           = p_ekycLivenessCheckScore,
                    EKYC_LIVENESS_REQUEST_TIME          = TO_TIMESTAMP (p_ekycLivenessRequestTime, 'YYYYMMDDHH24MISS.FF9'),
                    EKYC_FACEMATCHING_STATUS            = p_ekycFaceMatchingStatus,
                    EKYC_FACEMATCHING_SCORE             = p_ekycFaceMatchingScore,
                    EKYC_FACEMATCHING_REQUEST_TIME      = TO_TIMESTAMP (p_ekycFaceMatchingRequestTime, 'YYYYMMDDHH24MISS.FF9'),
                    EKYC_RETRIEVAL_DATA                 = p_ekycRetrievalData,
                    EKYC_RETRIEVAL_STATUS               = p_ekycRetrievalStatus,
                    EKYC_RETRIEVAL_REQUEST_TIME         = TO_TIMESTAMP (p_ekycRetrievalRequestTime, 'YYYYMMDDHH24MISS.FF9'),
                    EKYC_IDCARD_TAMPERING_VERDICT       = p_ekycIdVerdict,
                    EKYC_IDCARD_TAMPERING_DETAILS       = p_ekycIdVerdictDetails,
                    OCR_REQUESTTIME	                    = TO_TIMESTAMP (p_ocrRequestTime, 'YYYYMMDDHH24MISS.FF9'),
                    OCR_DETAILS	                        = p_ocrDetails,
                    SIM_FULLNAME	                    = p_simFullName,
                    SIM_DOB	                            = TO_DATE(p_simDob, 'YYYYMMDD'),
                    SIM_ID	                            = p_simId,
                    SIM_PHONENUMBER	                    = p_simPhoneNumber,
                    SIM_UPDATEDAT	                    = TO_TIMESTAMP (p_simUpdatedAt, 'YYYYMMDDHH24MISS.FF9'),
                    SI_FULLNAME	                        = p_siFullName,
                    SI_COMPANYNAME	                    = p_siCompanyName,
                    SI_COMPANYTAXID	                    = p_siCompanyTaxId,
                    SI_DATEOFBIRTH	                    = TO_DATE(p_siDateOfBirth, 'YYYYMMDD'),
                    SI_IDNUMBER	                        = p_siIdNumber,
                    SI_LASTPAIDDATE	                    = TO_DATE(p_siLastPaidDate, 'YYYYMM'),
                    SI_SALARY	                        = p_siSalary,
                    SI_NUMBER	                        = p_siNumber,
                    PIT_IDVALUE	                        = p_pitIdValue,
                    PIT_NUMBER	                        = p_pitNumber,
                    PIT_FULLNAME	                    = p_pitFullname,
                    PIT_DOB	                            = TO_DATE(p_pitDob, 'YYYYMMDD'),
                    PIT_GENDER	                        = p_pitGender,
                    PIT_COMPANYID	                    = p_pitCompanyId,
                    PIT_INCOMETYPE	                    = p_pitIncomeType,
                    PIT_INCOME	                        = p_pitIncome,
                    PIT_TOTAL_INCOME	                = p_pitTotalIncome,
                    OTHER_NID_3RP	                    = p_otherNid3rp,
                    DETAILS_APPROVAL_RULES              = p_detailsApprovalRules,
                    APPROVAL_STATUS                     = p_approvalStatus,
                    REJECTED_CODE                       = p_rejectedCode,
                    REJECTED_REASON                     = p_rejectedReason
              WHERE LEAD_ID = p_leadId;

                COMMIT;
            ELSE 
                UPDATE MBLP.LM_LEAD_DETAILS 
                    SET FULLNAME                 = p_fullName,
                        DOB                      = TO_DATE(p_dob, 'YYYYMMDD'),
                        NATIONALID               = p_nationalId,
                        MOBILEPHONE1             = p_phone,
                        EMAIL                    = p_email,
                        ACADEMIC_LEVEL           = p_academicLevel,
                        MARTIAL_STATUS           = p_martialStatus,
                        SECURITY_ANSWER          = p_securityAnswer,
                        ADDRESS_PERMANENT        = p_addressPermanent,
                        PROVINCE_PERMANENT       = p_provincePermanent,
                        DISTRICT_PERMANENT       = p_districtPermanent,
                        WARD_PERMANENT           = p_wardPermanent,
                        ADDRESS_CURRENT          = p_addressCurrent,
                        PROVINCE_TEMPORARY       = p_provinceTemporary,
                        DISTRICT_TEMPORARY       = p_districtTemporary,
                        WARD_TEMPORARY           = p_wardTemporary,
                        POSITION                 = p_position,
                        COMPANY                  = p_company,
                        WORKING_STATUS           = p_workingStatus,
                        BUSINESS_GROUP           = p_businessGroup,
                        BUSINESS_TYPE            = p_businessType,
                        WORKING_PROVINCE         = p_workingProvince,
                        WORKING_DISTRICT         = p_workingDistrict,
                        WORKING_WARD             = p_workingWard,
                        WORKING_STREET           = p_workingAddress,
                        WORKING_PHONE_NUMBER     = p_wokringPhone,
                        RECEIVING_SALARY_TYPE    = p_receivingSalaryType,
                        INCOME                   = p_income,
                        CARD_RECEIVINGADDRESS    = p_cardReceivingAddress,
                        BRANCH_CODE              = p_branchCode,
                        --STAFF_CODE               = p_branchName,
                        RECOMMENDED_CREDIT_LIMIT = p_recommendedCreditLimit,
                        --CLIENT_TIER              = p_clientTier,
                        RESIDENCE_DURATION_M     = p_residenceDurationM,
                        RESIDENCE_DURATION       = p_residenceDurationY,
                        NUMBER_OF_DEPENDENTS     = p_numberOfDependents,
                        REF1_REL_WITH_CARDHOLDER = p_ref1RelWithCardholder,
                        REF1_FULLNAME            = p_ref1Fullname,
                        REF1_PHONE               = p_ref1MobilePhone,
                        REF2_REL_WITH_CARDHOLDER = p_ref2RelWithCardholder,
                        REF2_FULLNAME            = p_ref2Fullname,
                        REF2_PHONE               = p_ref2MobilePhone,
                        DOCUMENT_FILE            = p_documentFile,
                        GENDER                   = p_gender,
                        ISSUED_DATE              = TO_DATE(p_issuedDate, 'YYYYMMDD'),
                        ISSUED_BY                = p_issuedBy,
                        WORKING_POSITION         = p_workingPosition
                WHERE   LEAD_ID = p_leadId;
                
                IF SQL%ROWCOUNT = 0 THEN
                    p_error := '1007'; -- khong tim thay leadId de update
                END IF;
                
                UPDATE MBLP.LM_LEAD SET MODIFIED_DATE = SYSDATE,
                                        status_id     = p_leadStatusId,
                                        BROWSER       = p_browser,
                                        URL_FULL      = p_urlFull,
                                        UTM_SOURCE    = p_utmSource,
                                        UTM_CAMPAIGN  = p_utmCampaign,
                                        UTM_REFERRAL  = p_utmRef 
                                  WHERE LEAD_ID       = p_leadId;
                           
                UPDATE  MBLP.LM_LEAD_QUALIFIED
                SET EKYC_LIVENESS_CHECK_STATUS          = p_ekycLivenessCheckStatus,
                    EKYC_LIVENESS_CHECK_SCORE           = p_ekycLivenessCheckScore,
                    EKYC_LIVENESS_REQUEST_TIME          = TO_TIMESTAMP (p_ekycLivenessRequestTime, 'YYYYMMDDHH24MISS.FF9'),
                    EKYC_FACEMATCHING_STATUS            = p_ekycFaceMatchingStatus,
                    EKYC_FACEMATCHING_SCORE             = p_ekycFaceMatchingScore,
                    EKYC_FACEMATCHING_REQUEST_TIME      = TO_TIMESTAMP (p_ekycFaceMatchingRequestTime, 'YYYYMMDDHH24MISS.FF9')
              WHERE LEAD_ID = p_leadId;
              
                COMMIT;
            END IF;
            
            BEGIN   --Add documentFile into mblp.lm_attachment
            v_file_arr := JSON_ARRAY_T(p_documentFile);
                    
            FOR INDX IN 0 .. v_file_arr.GET_SIZE - 1
            LOOP
                v_file_obj          := TREAT (v_file_arr.GET (INDX) AS JSON_OBJECT_T);
                v_file_id           := REPLACE ( v_file_obj.GET ('id').TO_STRING,'"');
                v_file_name         := REPLACE ( v_file_obj.GET ('filename').TO_STRING,'"');
                v_file_type         := REPLACE ( v_file_obj.GET ('type').TO_STRING,'"');
                v_file_directory    := REPLACE ( v_file_obj.GET ('directory').TO_STRING,'"');
                    
                v_file_id_exist := FALSE;
                FOR record in v_file_id_array
                LOOP
                    IF v_file_id = record.FILE_ID
                    THEN
                        v_file_id_exist := TRUE;
                        EXIT;
                    END IF;
                END LOOP;
                IF v_file_id_exist = FALSE
                THEN
                    INSERT INTO MBLP.LM_ATTACHMENT( LEAD_ID,
                                                    FILE_ID,
                                                    TYPE,
                                                    DIRECTORY,
                                                    FILENAME)
                                            VALUES( p_leadId,
                                                    v_file_id,
                                                    v_file_type,
                                                    v_file_directory,
                                                    v_file_name);
                ELSE
                    UPDATE MBLP.LM_ATTACHMENT SET TYPE          = v_file_type,
                                                  DIRECTORY    = v_file_directory,
                                                  FILENAME      = v_file_name
                                            WHERE FILE_ID       = v_file_id;
                END IF;
            END LOOP;
            EXCEPTION  --When documentFile is null or []
            WHEN OTHERS
            THEN
                v_file_id_exist := FALSE;
            END;
            COMMIT;
        ELSE  --CampaignId khong ton tai
            p_error := '1031';
        END IF;
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX
            THEN
                ROLLBACK;
                p_error := '1009'; -- leadId da ton tai
            WHEN OTHERS 
            THEN
                ROLLBACK;
                p_error :=
                   SQLCODE
                    || SQLERRM
                    || ' '
                    || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
    END pro_insert_or_update_lead;
    
    PROCEDURE pro_get_lead_status ( p_leadId        IN      VARCHAR2,
                                    p_statusName        OUT VARCHAR2,
                                    p_statusId          OUT VARCHAR2,
                                    p_error             OUT VARCHAR2,
                                    p_supportCode       OUT VARCHAR2)
    AS
    BEGIN
        p_error := '000000';
        SELECT ls.STATUS_ID, ls.STATUS_NAME , ld.SUPPORT_CODE INTO p_statusId, p_statusName,p_supportCode
            FROM MBLP.LM_STATUS ls, MBLP.LM_LEAD le, MBLP.LM_LEAD_DETAILS ld
            WHERE   le.LEAD_ID = p_leadId
                AND ld.LEAD_ID = le.LEAD_ID
                AND le.STATUS_ID = ls.STATUS_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                p_error :=
                   SQLCODE
                    || SQLERRM
                    || ' '
                    || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
            WHEN TOO_MANY_ROWS
            THEN
                p_error :=
                   SQLCODE
                    || SQLERRM
                    || ' '
                    || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
    END pro_get_lead_status;
    
    PROCEDURE pro_insert_request (p_requestId       IN      VARCHAR2,
                                  p_error               OUT VARCHAR2)
    AS
        v_count     NUMBER;
    BEGIN
        p_error := '000000';
        SELECT count(*) INTO v_count 
            FROM    MBLP.LM_REQUEST 
            WHERE   REQUEST_ID = p_requestId;
        IF v_count = 0 THEN
            INSERT INTO MBLP.LM_REQUEST(REQUEST_ID) VALUES (p_requestId);
            COMMIT;
        ELSE
            p_error := '1006';
        END IF;
    END pro_insert_request;
    
    PROCEDURE pro_delete_request (p_requestId       IN      VARCHAR2,
                                  p_error               OUT VARCHAR2)
    AS
        v_count     NUMBER;
    BEGIN
        p_error := '000000';
        SELECT count(*) INTO v_count 
            FROM    MBLP.LM_REQUEST 
            WHERE   REQUEST_ID = p_requestId;
        IF v_count = 0 THEN
            p_error := '1006';
        ELSE
            DELETE FROM MBLP.LM_REQUEST 
                WHERE   REQUEST_ID = p_requestId;
            COMMIT;
        END IF;
    END pro_delete_request;
    
    PROCEDURE pro_check_campaignId (p_campaignId   IN       VARCHAR2,
                                    p_campaignExist     OUT BOOLEAN)
    AS
        v_count     NUMBER;
    BEGIN
        p_campaignExist :=FALSE;
        SELECT count(*) into v_count FROM MBLP.LM_CAMPAIGN WHERE CAMPAIGN_ID = p_campaignId;
        IF v_count != 0 THEN
            p_campaignExist := TRUE;
        END IF;
    END pro_check_campaignId;
                             
END HDB_TS_SERVICE;
/
