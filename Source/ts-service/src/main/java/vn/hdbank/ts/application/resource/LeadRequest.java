package vn.hdbank.ts.application.resource;

import lombok.Getter;
import lombok.Setter;
import vn.hdbank.ts.application.resource.File;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
public class LeadRequest extends BasicRequest {
	@NotNull
	private String partnerId;
	@NotNull
	private String leadId;
	@NotNull
	private String actionCode;
	@NotNull
	private String campaignId;
	@NotNull
	private String leadStatusId;
	private String fullName;
	private String fullNameUpCase;
	private String dob;
	private String nationalId;
	private String phone;
	private String email;
	private String paymentAccount;
	private String internationalDebitCard;
	private String domesticDebitCard;
	@JsonProperty("eBanking")
	private String eBanking;

	@JsonIgnore
	public String geteBanking() {
		return eBanking;
	}

	@JsonIgnore
	public void seteBanking(String eBanking) {
		this.eBanking = eBanking;
	}

	private String cif;
	private String academicLevel;
	private String residenceStatus;
	private String nationalType;
	private String issuedDate;
	private String issuedBy;
	private String residenceDurationM;
	private String residenceDurationY;
	private String gender;
	private String martialStatus;
	private String pin;
	private String numberOfDependents;
	private String cardPrintedName;
	private String nationality;
	private String mobilePhone1;
	private String mobilePhone2;
	private String insuranceNumber;
	private String expDateOfResidence;
	private String addressPermanent;
	private String provincePermanent;
	private String districtPermanent;
	private String wardPermanent;
	private String addressCurrent;
	private String sameAsPermanent;
	private String provinceTemporary;
	private String districtTemporary;
	private String wardTemporary;
	private String hdbEmployee;
	private String employeeId;
	private String position;
	private String taxNumber;
	private String company;
	private String workingStatus;
	private String businessGroup;
	private String businessType;
	private String industry;
	private String workingProvince;
	private String workingDistrict;
	private String workingWard;
	private String workingAddress;
	private String workingDepartment;
	private String workingPosition;
	private String workingPhone;
	private String workingPhoneExt;
	private String workingBusinessDuration;
	private String charterCapital;
	private String numberOfEmployees;
	private String businessSize;
	private String durationOfWork;
	private String laborContractType;
	private String receivingSalaryType;
	private String income;
	private String otherIncome;
	private String otherIncomeDescription;
	private String monthlyExpenses;
	private String hdbCreditLimit;
	private String transactionWithHDBank;
	private String deposit;
	private String loan;
	private String maxCic;
	private String months12;
	private String months24;
	private String months36;
	private String current;
	private String outstandingLoan;
	private String totalCreditcardOutstandingLoan;
	private String ref1RelWithCardholder;
	private String ref1Fullname;
	private String ref1NationalId;
	private String ref1MobilePhone;
	private String ref1Company;
	private String ref1Address;
	private String ref2RelWithCardholder;
	private String ref2Fullname;
	private String ref2NationalId;
	private String ref2MobilePhone;
	private String ref2Company;
	private String ref2Address;
	private String primaryOrSecondaryCard;
	private String targetCustomer;
	private String cardPolicy;
	private String applicationPeriod;
	private String fromDate;
	private String toDate;
	private String mainContractNumber;
	private String contractProductCode;
	private String cardStampingAddress;
	private String cardStampingDateTime;
	private String cardReceivingAddress;
	private String pinMailer;
	private String cardStatementSendingType;
	private String cardStatementReceivingAddress;
	private String monthlyStatementDate;
	private String autoDebtDeduction;
	private String otherCardServices;
	private String sms;
	private String branchCode;
	private String branchName;
	private String staffCode;
	private String staffName;
	private String amlFatcaResident;
	private String amlFatcaOrganization;
	private String amlFatcaInfluence;
	private String amlFatcaUs;
	private String recommendedCreditLimit;
	private String nationalIdFormer;
	private String nationalTypeFormer;
	private File[] documentFile;
	private String browser;
	@NotNull
	private String urlFull;
	private String flowType;
	private String clientSupportDocType;
	private String needUpdateDoc;
	private String cicS37Time;
	private String cicS37;
	private String cicR11aTime;
	private String cicR11a;
	private String cicR14Time;
	private String cicR14;
	private String creditScore;
	private String fraudScore;
	private String ekycLivenessCheckStatus;
	private String ekycLivenessCheckScore;
	private String ekycLivenessRequestTime;
	private String ekycFaceMatchingStatus;
	private String ekycFaceMatchingScore;
	private String ekycFaceMatchingRequestTime;
	private String ekycRetrievalData;
	private String ekycRetrievalStatus;
	private String ekycRetrievalRequestTime;
	private String ekycIdCardTamperingVerdict;
	private String ekycIdCardTamperingVerdictDetails;
	private String ocrRequestTime;
	private String ocrDetails;
	private String simFullName;
	private String simDob;
	private String simId;
	private String simPhoneNumber;
	private String simUpdatedAt;
	private String siFullName;
	private String siCompanyName;
	private String siCompanyTaxId;
	private String siDateOfBirth;
	private String siIdNumber;
	private String siLastPaidDate;
	private String siSalary;
	private String siNumber;
	private String pitIdValue;
	private String pitNumber;
	private String pitFullname;
	private String pitDob;
	private String pitGender;
	private String pitCompanyId;
	private String pitIncomeType;
	private String pitIncome;
	private String pitTotalIncome;
	private String otherNid3rp;
	private String detailsApprovalRules;
	private String approvalStatus;
	private String rejetedCode;
	private String rejectedReason;
	private String securityAnswer;
	private String clientTier;
	private String utmSource;
	private String utmCampaign;
	private String utmRef;
}
