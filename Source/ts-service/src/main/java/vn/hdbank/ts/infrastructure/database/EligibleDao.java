package vn.hdbank.ts.infrastructure.database;

import vn.hdbank.ts.application.resource.EligibleRequest;
import vn.hdbank.ts.application.resource.EligibleResponse;

public interface EligibleDao {

	EligibleResponse checkEligible(EligibleRequest request);
}
