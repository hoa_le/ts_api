package vn.hdbank.ts.application.resource;

import lombok.Getter;
import lombok.Setter;
import vn.hdbank.ts.application.resource.File;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class EligibleResponse extends BasicResponse {
	@NotNull
	private String leadId;
	@NotNull
	private String partnerId;
	@NotNull
	private String requestTime;
	@NotNull
	private String campaignId;
	@NotNull
	private String leadStatusId;
	private String fullname;
	private String dob;
	@NotNull
	private String nationalId;
	private String issuedDate;
	private String issuedBy;
	private String gender;
	private String academicLevel;
	private String martialStatus;
	private String phone;
	private String email;
	private String securityAnswer;
	private String addressPermanent;
	private String provincePermanent;
	private String districtPermanent;
	private String wardPermanent;
	private String addressCurrent;
	private String provinceTemporary;
	private String districtTemporary;
	private String wardTemporary;
	private String position;
	private String company;
	private String workingStatus;
	private String businessGroup;
	private String businessType;
	private String workingProvince;
	private String workingDistrict;
	private String workingWard;
	private String workingAddress;
	private String workingPhone;
	private String workingPosition;
	private String receivingSalaryType;
	private long income;
	private String cardReceivingAddress;
	private String branchCode;
	private String staffCode;
	private long recommendedCreditLimit;
	private String clientTier;
	private String needEkyc;
	private String needeSign;
	private String contractProductCode;
	private String createdDate;
	private String resultType;
    private String signature;
    private int residenceDurationM;
	private int residenceDurationY;
	private String numberOfDependents;
	private String ref1RelWithCardholder;
	private String ref1Fullname;
	private String ref1MobilePhone;
	private String ref2RelWithCardholder;
	private String ref2Fullname;
	private String ref2MobilePhone;
	private File[] documentFile;
}
