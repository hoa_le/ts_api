package vn.hdbank.ts.common.model.createOTP.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

    private String mediaType;

    private String returnOTP;

    private String challengeCode;


    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getReturnOTP() {
        return returnOTP;
    }

    public void setReturnOTP(String returnOTP) {
        this.returnOTP = returnOTP;
    }

    public String getChallengeCode() {
        return challengeCode;
    }

    public void setChallengeCode(String challengeCode) {
        this.challengeCode = challengeCode;
    }
}
