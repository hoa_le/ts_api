package vn.hdbank.ts.common.model.createOTP;

import java.util.List;

public class AdditionalInformation {
    private List<NameValuePair> nameValuePairs;

    public List<NameValuePair> getNameValuePairs() { return nameValuePairs; }
    public void setNameValuePairs(List<NameValuePair> value) { this.nameValuePairs = value; }
}
