package vn.hdbank.ts.application.configuration.keyManage;

import org.springframework.stereotype.Component;

import vn.hdbank.ts.application.configuration.ApplicationConfig;
import vn.hdbank.ts.application.utils.SimpleDecrypt;

import java.util.HashMap;
import java.util.Map;

@Component
public class KeyManager {

    //partner name
    public static String NONE = "NONE";

    private static KeyManager s_instance = null;
    private static Map<String, PartnerKey> m_resource;

    public static void Init(ApplicationConfig applicationConfig) {
        m_resource = new HashMap<String, PartnerKey>();


        ApplicationConfig.Signature signature = applicationConfig.getSignature();

        //hdinternalkey
        PartnerKey hdInternalKey = new PartnerKey();
        hdInternalKey.setPartnerName("internal");
        hdInternalKey.setPartnerID("internal");
        hdInternalKey.setRsaPrivateKey(applicationConfig.getSignature().getInternal().getHDBRSAPrivateKey());
        hdInternalKey.setRsaPublicKey(applicationConfig.getSignature().getInternal().getHDBRSAPublicKey());
        m_resource.put(hdInternalKey.getPartnerID(),hdInternalKey);


        
        ApplicationConfig.Signature.Ts trustingSocial = signature.getTs();
        if(trustingSocial != null) {
            PartnerKey partnerKey = new PartnerKey();
            partnerKey.setPartnerID(trustingSocial.getPartnerId());
            partnerKey.setPartnerPassword(trustingSocial.getPartnerPassword());
            partnerKey.setPartnerName(trustingSocial.getPartnerName());
            partnerKey.setRsaPublicKey(trustingSocial.getRsaPublicKey());
            partnerKey.setAesSecretKey(trustingSocial.getAesSecretKey());
            partnerKey.setAesSalt(trustingSocial.getAesSalt());
            partnerKey.setIpWhiteList(trustingSocial.getWhitelistIp());
            partnerKey.setAuthPartnerUsername(trustingSocial.getAuthPartnerUsername());
            partnerKey.setAuthPartnerPassword(SimpleDecrypt.simpleDecrypt(trustingSocial.getAuthPartnerPassword(),"Framework", new String[2]));
            partnerKey.setDesHexKey(trustingSocial.getDesHexKey());
            partnerKey.setDesHexIv(trustingSocial.getDesHexIv());
            partnerKey.setOtpGwIp(trustingSocial.getOtpGwIp());
            partnerKey.setOtpGwPort(trustingSocial.getOtpGwPort());
            partnerKey.setEsbUser(trustingSocial.getEsbUser());
            partnerKey.setEsbPass(trustingSocial.getEsbPass());
            m_resource.put(partnerKey.getPartnerID(), partnerKey);
            m_resource.put(partnerKey.getAuthPartnerUsername(), partnerKey);
        }

    }

    public static KeyManager GetInstance(ApplicationConfig applicationConfig)
    {
        if (s_instance == null) {
            s_instance = new KeyManager();
            KeyManager.Init(applicationConfig);
        }
        return s_instance;
    }

    public PartnerKey getPartnerKey(String partnerID)
    {
        return m_resource.get(partnerID);
    }
}
