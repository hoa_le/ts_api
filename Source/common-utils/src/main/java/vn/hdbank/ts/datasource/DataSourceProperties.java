package vn.hdbank.ts.datasource;

public interface DataSourceProperties {
	public String getDbUserName();
	public String getDbPassword();
	public String getDbUrl();
	public String getDbSchema();
	public String getConnectionName();
	public int getMinPoolSize();
	public int getMaxPoolSize();
	public int getConnectionHarvestMaxCount();
	public int getConnectionHarvestTriggerCount();
	public long getMaxConnectionReuseTime();
	public int getMaxConnectionReuseCount();
	public int getAbandonedConnectionTimeout();
	public int getConnectionWaitTimeout();
	public int getInactiveConnectionTimeout();
	public int getTimeToLiveConnectionTimeout();
	public int getTimeoutCheckInterval();
	public int getMaxStatements();
	public String getDriverClass();
}
