package vn.hdbank.ts.common.utils.security;

public interface CryptographyInf {
	String encrypt(String encryptData) throws Exception;
    String decrypt(String decryptData) throws Exception;
    String createSignature(String encryptData) throws Exception;
    boolean verifySignature(String decryptData, String sign) throws Exception;
}
