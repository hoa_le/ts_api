package vn.hdbank.ts.application.resource;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BasicRequest {

    @NotNull
    private String requestId;

    @NotNull
//    @DateTimeFormat(pattern = "yyyyMMddHHmmss")
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmss")
    private String requestTime;

    @NotNull
    private String signature;


}
