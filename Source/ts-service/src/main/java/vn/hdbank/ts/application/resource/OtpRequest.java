package vn.hdbank.ts.application.resource;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class OtpRequest extends BasicRequest{
	@NotNull
	private String partnerId;
    @NotNull
    private String mobile;
    
}
