﻿CREATE OR REPLACE PACKAGE BODY MBLP.HDB_EKYC_IMAGEAPI AS
 
 
    PROCEDURE GET_IMAGE_BY_LEADID(P_LEAD_ID VARCHAR2, P_FILTER VARCHAR2,P_TYPE VARCHAR2, P_OUT OUT SYS_REFCURSOR)
    AS
    BEGIN
    
       open p_out for 
            select tb1.* from MBLP.LM_ATTACHMENT tb1 
            where tb1.LEAD_ID=P_LEAD_ID;
    
    END GET_IMAGE_BY_LEADID;
     
 
END HDB_EKYC_IMAGEAPI;
/
