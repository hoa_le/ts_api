package vn.hdbank.ts.application.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import vn.hdbank.ts.utils.WriteLog;
import vn.hdbank.ts.application.exception.ErrorMessage;

import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Utils {

	private static final String FILE_CONFIG = "\\config\\application.properties";

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public static boolean isEmpty(Object object) {
		return object == null ? true : (object instanceof String && ((String) object).isEmpty());
	}

	public static boolean isNumeric(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isDigit(c))
				return false;
		}
		return true;
	}

	public static void close(Statement statement) {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException ex) {
			// NOOP
		}
	}

	public static void close(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException ex) {
			// NOOP
		}
	}

	public static void close(Closeable closeable) {
		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (Exception ex) {
			// NOOP
		}
	}

	public static void close(ResultSet resultSet) {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
		} catch (Exception ex) {
			// NOOP
		}
	}

	public static String nullToEmpty(Object object) {
		return object == null ? "" : object.toString();
	}

	public static Number emptyToZero(Object object) {
		return isEmpty(object) ? 0 : new BigDecimal(object.toString());
	}

	public static String exceptionToString(Throwable ex) {
		StringWriter errors = new StringWriter();
		ex.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}

	public static String formatNumber(Object number) {
		if (isEmpty(number)) {
			number = 0;
		}
		DecimalFormat formatter = new DecimalFormat("#,###");
		return formatter.format(number).replaceAll(",", ".");

	}

	public static boolean isExistFromConfig(String strProperties, String element) {
		if (Utils.isEmpty(strProperties)) {
			return false;
		}
		String[] arr = strProperties.split(",");
		for (String arr1 : arr) {
			if (element.equalsIgnoreCase(arr1)) {
				return true;
			}
		}
		return false;
	}

	public static String replaceVnChars(String src) {
		if (Utils.isEmpty(src)) {
			return src;
		}
		String dest = Normalizer.normalize(src.trim(), Normalizer.Form.NFC);

		dest = dest.replaceAll("[áàãảạâấầẩẫậăắằẳẵặ]", "a").replaceAll("[óòỏõọôốồổỗộơớờởỡợ]", "o")
				.replaceAll("[íìĩỉị]", "i").replaceAll("[ýỳỷỹỵ]", "y").replaceAll("[éèẻẽẹêếềểễệ]", "e")
				.replaceAll("[úùủũụưứừửữự]", "u").replaceAll("[đ]", "d");
		dest = dest.replaceAll("[ÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬ]", "A").replaceAll("[ÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢ]", "O")
				.replaceAll("[ÍÌỈĨỊ]", "I").replaceAll("[ÝỲỶỸỴ]", "Y").replaceAll("[ÉÈẺẼẸÊẾỀỂỄỆ]", "E")
				.replaceAll("[ÚÙỦŨỤƯỨỪỬỮỰ]", "U").replaceAll("[Đ]", "D");
		return dest;
	}

	public static boolean isValidEmail(String email) {
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		return pattern.matcher(email).matches();
	}

	public static <E> E choice(Collection<? extends E> coll) {
		if (coll.isEmpty()) {
			return null; // or throw IAE, if you prefer
		}
		return choice(coll, coll.size());
	}

	public static <E> E choice(Collection<? extends E> coll, int size) {
		// Random rand = new Random();
		try {
			Random rand = SecureRandom.getInstanceStrong(); // SecureRandom is preferred to Random
			int index = rand.nextInt(size);
			if (coll instanceof List) { // optimization
				return ((List<? extends E>) coll).get(index);
			} else {
				Iterator<? extends E> iter = coll.iterator();
				for (int i = 0; i < index; i++) {
					iter.next();
				}
				return iter.next();
			}
		} catch (Exception ex) {
			return null;
		}
	}

	public static String subString(String message, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(message);
		if (matcher.find()) {
			return matcher.group(0);
		}
		return "";
	}

	public static String convertToJsonString(Object obj) throws JsonProcessingException {
		if (obj == null) {
			return "";
		}
		ObjectMapper mapper = new ObjectMapper();

		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		return mapper.writeValueAsString(obj);
	}

	public static <T> T fromString(Class<T> type, String jSon) throws IOException {
		return new ObjectMapper().readValue(jSon, type);
	}

	public static String toJsonString(Object object) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}

	public static String toXml(Object source, Class<?>... type) throws JAXBException {
		StringWriter sw = new StringWriter();
		JAXBContext carContext = JAXBContext.newInstance(type);
		Marshaller carMarshaller = carContext.createMarshaller();
		carMarshaller.marshal(source, sw);
		return sw.toString();
	}

	@SuppressWarnings("unchecked")
	public static <T extends Object> T toObject(String xml, Class<T> type) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(type);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		return (T) unmarshaller.unmarshal(reader);

	}

	public static String formatDate(Date date, String format) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.format(date);

	}

	public static boolean isExist(String code) {
		if (code == null || code.length() == 0) {
			return false;
		}
		return true;
	}

	public static boolean checkPhone(String phone) {
		if (phone == null || phone.length() == 0) {
			return false;
		}
		if (phone.length() <= 2) {
			return false;
		}
		if (phone.equals("01") || phone.equals("-1") || phone.equals("00")) {
			return false;
		}
		return true;
	}

	public static String hiddenPhoneNumber(String phoneNumber) {
		try {
			int lengthHide = phoneNumber.length() - 4;
			StringBuffer hide = new StringBuffer();
			for (int i = 0; i < lengthHide; i++) {
				hide.append("*");
			}
			return hide + phoneNumber.substring(lengthHide);
		} catch (Exception ex) {
			return "";
		}
	}

	public static String convertDateYYMM(int date) {
		String dateInput = date + "";
		int length = dateInput.length() - 4;
		return dateInput.substring(length, dateInput.length());

	}

	public static HttpStatus mapHttpStatus(String code) {
		String[] statusCodeArr = code.split("\\.");
		HttpStatus httpStatus = null;
		if (statusCodeArr[0] != null && statusCodeArr[0].length() > 0) {
			httpStatus = HttpStatus.valueOf(Integer.parseInt(statusCodeArr[0]));
		}
		if (httpStatus == null) {
			httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
		}
		return httpStatus;
	}

	public static String decodeBase64(String tex) {
		byte[] decode = Base64.decodeBase64(tex.getBytes());
		return new String(decode);
	}

	public static String getCurrentDateTime(String format) {
		// MM/dd/yyyy hh:mm:ss
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = new Date();
		return df.format(date);
	}

	public static String getBit37ATM() {
		return Utils.getCurrentDateTime("HHmmssMMdd") + "01";
	}

	public static String formatAmount(Long amount) {
		NumberFormat formatter = new DecimalFormat("0000000000");
		return formatter.format(amount) + "00";
	}

	public static boolean validateEmail(String email) {
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
				+ "A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex);
		if (email == null)
			return false;
		return pat.matcher(email).matches();
	}

	public static String priceWithoutDecimal(Double price) {
		DecimalFormat formatter = new DecimalFormat("###,###,###");
		return formatter.format(price);
	}

	public static String getMaskCardNo(String cardNo) {
		StringBuilder stringBuilder = new StringBuilder(cardNo);
		for (int i = 6; i < 12; i++) {
			stringBuilder.setCharAt(i, '*');
		}
		return stringBuilder.toString();
	}

	// KHA ADDED for unique otp content base on param which contain request id
	// Using for create sms content
	public static String replaceSpecialCharacters(final String str) {
		if (str == null || str.trim().equals("")) {
			return "";
		}
		final StringBuffer newString = new StringBuffer();
		for (int i = 0; i < str.length(); ++i) {
			final char chtemp = str.charAt(i);
			switch (chtemp) {
			case ' ': {
				newString.append("+");
				break;
			}
			case '\'': {
				newString.append("%27");
				break;
			}
			case '\"': {
				newString.append("%22");
				break;
			}
			case '#': {
				newString.append("%23");
				break;
			}
			case '%': {
				newString.append("%25");
				break;
			}
			case '&': {
				newString.append("%26");
				break;
			}
			case '+': {
				newString.append("%2B");
				break;
			}
			case '/': {
				newString.append("%2F");
				break;
			}
			case ':': {
				newString.append("%3A");
				break;
			}
			case ';': {
				newString.append("%3B");
				break;
			}
			case '<': {
				newString.append("%3C");
				break;
			}
			case '>': {
				newString.append("%3E");
				break;
			}
			case '?': {
				newString.append("%3F");
				break;
			}
			default: {
				newString.append(chtemp);
				break;
			}
			}
		}
		final String returnString = newString.toString();
		return returnString;
	}

	public static String convertYYYYMMDDToDDMMYYYY(String sYYYYMMDD) {
		try {
			// String ds1 = "2007-06-30";
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
			String ds2 = sdf2.format(sdf1.parse(sYYYYMMDD));
			// System.out.println(ds2); //will be 30/06/2007
			return ds2;
		} catch (Exception e) {

		}
		return sYYYYMMDD;
	}

	// get value from way4 response, check productionStatus
	public static String getFirstItemsByStringArray(String array) {
		if (!Utils.isEmpty(array)) {
			String[] arrayStr = array.split(";");
			return arrayStr[0];
		}
		return null;
	}

	public static String getLastItemsByStringArray(String array) {
		if (!Utils.isEmpty(array)) {
			String[] arrayStr = array.split(";");
			return arrayStr[arrayStr.length - 1];
		}
		return null;
	}

	// using for check cardno valid
	public static boolean checkLuhn(String ccNumber) {
		int sum = 0;
		boolean alternate = false;
		for (int i = ccNumber.length() - 1; i >= 0; i--) {
			int n = Integer.parseInt(ccNumber.substring(i, i + 1));
			if (alternate) {
				n *= 2;
				if (n > 9) {
					n = (n % 10) + 1;
				}
			}
			sum += n;
			alternate = !alternate;
		}
		return (sum % 10 == 0);
	}

	@SuppressWarnings("unused")
	public static boolean DateValid(String dateToValidate, String dateFromat) {

		if (dateToValidate == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
		try {
			// if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate);
		} catch (Exception e) {
			WriteLog.error(dateToValidate + " not corrrect with format: " + dateFromat, e);
			return false;
		}

		return true;
	}
	// ENDED

	public static String getCurrentDateTimeWithFormat(String format) {
		// MM/dd/yyyy hh:mm:ss
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = new Date();

		return df.format(date);
	}

	public static String getResponseTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());

	}

	public static String getRandomUUID() {
		return UUID.randomUUID().toString();
	}

	public static String getDateTimeDb() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		return sdf.format(cal.getTime());
	}

	public static InputStreamReader getClob(String data) {
		ByteArrayInputStream inputStream = new ByteArrayInputStream(new Gson().toJson(data).getBytes());
		InputStreamReader inputReqSteam = new InputStreamReader(inputStream);
		return inputReqSteam;
	}

	public static ResponseEntity<?> getResponseEntity(Object request, Object response, String responseCode, String type,
			String partnerId) {

		Properties properties = new Properties();
		InputStream inputStream = null;
		String dopPartner = "";
		try {
			String currentDir = System.getProperty("user.dir");
			inputStream = new FileInputStream(currentDir + FILE_CONFIG);

			properties.load(inputStream);

			dopPartner = properties.getProperty("hdbank.ts.partner-id-dop");

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (ResultCode.SUCCESS.getCode().equals(responseCode)) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		} else if (ResultCode.ID_NOT_FOUND.getCode().equals(responseCode)) {
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		} else if (ResultCode.TS_ERROR.getCode().equals(responseCode)
				|| ResultCode.LEAD_EXIST.getCode().equals(responseCode)
				|| ResultCode.INVALID_FILE.getCode().equals(responseCode)
				|| ResultCode.REQUEST_ID_EXIST.getCode().equals(responseCode)
				|| ResultCode.CAMPAIGN_ID_EXIST.getCode().equals(responseCode)
				|| ResultCode.SIGNATURE_NOT_MATCH.getCode().equals(responseCode)) {
			HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setResponseTime(Utils.getResponseTime());
			errorMessage.setCode(responseCode);
			errorMessage.setMessage(ResultCode.findByCode(responseCode).getMessage());
			errorMessage.setSignature("");
			return new ResponseEntity<>(errorMessage, status);

		} else {
			if (partnerId.equalsIgnoreCase(dopPartner)) {
				return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			} else {
				HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
				ErrorMessage errorMessage = new ErrorMessage();
				errorMessage.setResponseTime(Utils.getResponseTime());
				errorMessage.setCode(ResultCode.OTP_INVALID.getCode());
				errorMessage.setMessage(ResultCode.OTP_INVALID.getMessage());
				errorMessage.setSignature("");
				return new ResponseEntity<>(errorMessage, status);
			}
		}
	}

	public static ResponseEntity<?> responseError(Object data, String className, String funcName, String requestId) {
		ErrorMessage errorMessage = new ErrorMessage();
		Gson gson = new Gson();
		errorMessage.setResponseTime(Utils.getResponseTime());
		String responseCode = ResultCode.TS_ERROR.getCode();
		errorMessage.setCode(responseCode);
		errorMessage.setMessage(ResultCode.findByCode(responseCode).getMessage());
		WriteLog.write(requestId, errorMessage.getCode(), className, funcName,
				String.format(">>> Response: %s", gson.toJson(errorMessage)));
		return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public static String maskingCard(String cardNumber) {
		try {
			cardNumber = cardNumber.substring(0, 6) + "******" + cardNumber.substring(12, 16);
		} catch (Exception e) {
		}

		return cardNumber;
	}

	public static String removeAccent(String s) {

		String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(temp).replaceAll("").replaceAll("Đ", "D").replace("đ", "d");
	}

}
