package vn.hdbank.ts.common.model.verifyOTP.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.hdbank.ts.common.model.ResponseStatus;
import vn.hdbank.ts.common.model.createOTP.Header;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VerifyOTPRes {

    private Header header;
    private ResponseStatus responseStatus;
    private BodyRes bodyRes;

    public Header getHeader() { return header; }
    public void setHeader(Header value) { this.header = value; }

    public ResponseStatus getResponseStatus() { return responseStatus; }
    public void setResponseStatus(ResponseStatus value) { this.responseStatus = value; }

    public BodyRes getBodyRes() { return bodyRes; }
    public void setBodyRes(BodyRes value) { this.bodyRes = value; }
}
