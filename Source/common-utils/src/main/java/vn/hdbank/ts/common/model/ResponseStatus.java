package vn.hdbank.ts.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseStatus {
    private String status;
    private String globalErrorCode;
    private String globalErrorDescription;
    private List<ErrorInfo> errorInfo;

    public String getStatus() { return status; }
    public void setStatus(String value) { this.status = value; }

    public String getGlobalErrorCode() { return globalErrorCode; }
    public void setGlobalErrorCode(String value) { this.globalErrorCode = value; }

    public String getGlobalErrorDescription() { return globalErrorDescription; }
    public void setGlobalErrorDescription(String value) { this.globalErrorDescription = value; }

    public List<ErrorInfo> getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(List<ErrorInfo> errorInfo) {
        this.errorInfo = errorInfo;
    }
}
