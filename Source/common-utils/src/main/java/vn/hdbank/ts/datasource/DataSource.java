package vn.hdbank.ts.datasource;

import java.sql.Connection;

public abstract class DataSource {
	public abstract Connection getConnection() throws Exception;
	public abstract String getSchema() throws Exception;
}
