package vn.hdbank.ts.common.utils.security;

public abstract class AbstractCryptography {
	private String internalPublicKey;
    private String internalPrivateKey;

    public AbstractCryptography(String publicKey, String privateKey) {
        internalPublicKey = publicKey;
        internalPrivateKey = privateKey;
    }

    public String encrypt(String encryptData) throws Exception {
        return encryptMethod(encryptData, internalPublicKey);
    }

    public String decrypt(String decryptData) throws Exception {
        return decryptMethod(decryptData, internalPrivateKey);
    }

    public String createSignature(String decryptData) throws Exception {
        return createSignatureMethod(decryptData, internalPrivateKey);
    }

    public boolean verifySignature(String decryptData, String sign) throws Exception {
        return verifySignatureMethod(decryptData, sign, internalPublicKey);
    }

    protected abstract String encryptMethod(String data, String internalPublicKey) throws Exception;

    protected abstract String decryptMethod(String data, String internalPublicKey) throws Exception;

    protected abstract String createSignatureMethod(String data, String internalPrivateKey) throws Exception;

    protected abstract boolean verifySignatureMethod(String data, String sign, String internalPublicKey) throws Exception;
}
