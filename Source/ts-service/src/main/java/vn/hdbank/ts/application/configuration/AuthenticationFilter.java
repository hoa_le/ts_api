package vn.hdbank.ts.application.configuration;

import org.springframework.web.filter.GenericFilterBean;
import vn.hdbank.ts.utils.WriteLog;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ThuongDc
 * @since 9/29/2020
 */
public class AuthenticationFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        try {

            String ip = request.getRemoteAddr();
            if (ip.equals("127.0.0.1")) {
//                SecurityContextHolder.getContext().setAuthentication(authentication);
                filterChain.doFilter(request, response);
            } else {
                WriteLog.write("BUG", "AuthenticationFilter", "doFilter", ip + ": IP is invalid");
                ((HttpServletResponse) response).sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
                response.getOutputStream().write("Invalid IP Address".getBytes());
                response.getOutputStream().close();
            }
        } catch (NullPointerException e) {
            WriteLog.write("BUG", "AuthenticationFilter", "doFilter", "getAuthentication fail");
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
            response.getOutputStream().write("Unauthorized".getBytes());
            response.getOutputStream().close();

        } catch (Exception e) {
            WriteLog.error("AuthenticationFilter", "doFilter", e);
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
            response.getOutputStream().write(e.toString().getBytes());
            response.getOutputStream().close();
        }
    }
}
