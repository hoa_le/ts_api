package vn.hdbank.ts.common.model.createOTP.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseInfo {

    private CreateOTPRes createOTPRes;

    public CreateOTPRes getCreateOTPRes() {
        return createOTPRes;
    }

    public void setCreateOTPRes(CreateOTPRes createOTPRes) {
        this.createOTPRes = createOTPRes;
    }
}
