package vn.hdbank.ts.datasource;

import vn.hdbank.ts.datasource.impl.ECRMDataSource;

public class DataSourceFactory {
    private static DataSourceFactory instance;

    private DataSourceFactory() {
    }

    public static DataSourceFactory getInstance() {
        if (instance == null) {
            instance = new DataSourceFactory();
        }
        return instance;
    }

    public DataSource getDataSource(EnumDataSource schema) {
        if (schema == EnumDataSource.ecrm) {
            return ECRMDataSource.getInstance();
        } else {
            return null;
        }
    }
}
