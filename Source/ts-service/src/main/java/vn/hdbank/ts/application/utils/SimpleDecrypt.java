package vn.hdbank.ts.application.utils;

public class SimpleDecrypt {

    public static String simpleEncrypt(String plainText, String key, String[] err) {
        err[0] = "000000";
        String out = "";
        try {
            int i = (key.hashCode() < 0) ? key.hashCode() * -1 : key.hashCode();
            byte[] b = new byte[plainText.length()];

            for (int j = 0; j < b.length; ++j) {
                b[j] = (byte) plainText.charAt(j);
                b[j] = (byte) (b[j] ^ i);
            }
            out = data2hex(b);
        } catch (Exception ex) {
            err[0] = ex.getMessage();
        }
        return out;

    }

    public static String simpleDecrypt(String cipherText, String key, String[] err) {
        err[0] = "000000";
        String out = "";
        try {
            int i = (key.hashCode() < 0) ? key.hashCode() * -1 : key.hashCode();
            byte[] b = hex2data(cipherText);

            for (int j = 0; j < b.length; ++j) {
                b[j] = (byte) (b[j] ^ i);
            }

            StringBuffer sb = new StringBuffer(b.length);
            for (int j = 0; j < b.length; ++j) {
                sb.append((char) b[j]);
            }
            out = sb.toString();
        } catch (Exception ex) {
            err[0] = ex.getMessage();
        }
        return out;


    }

    public static byte[] hex2data(String str) {
        if (str == null) {
            return new byte[0];
        }
        int len = str.length();
        char[] hex = str.toCharArray();
        byte[] buf = new byte[len / 2];

        for (int pos = 0; pos < len / 2; ++pos) {
            buf[pos] = (byte) (toDataNibble(hex[(2 * pos)]) << 4 & 0xF0 | toDataNibble(hex[(2 * pos + 1)]) & 0xF);
        }
        return buf;
    }

    public static byte toDataNibble(char c) {
        if (('0' <= c) && (c <= '9')) {
            return (byte) ((byte) c - 48);
        }
        if (('a' <= c) && (c <= 'f')) {
            return (byte) ((byte) c - 97 + 10);
        }
        if (('A' <= c) && (c <= 'F')) {
            return (byte) ((byte) c - 65 + 10);
        }
        return -1;
    }

    public static String removeWhiteSpace(String str) {
        String temp = "";
        for (int i = 0; i < str.length(); ++i) {
            if ((str.charAt(i) != ' ') && (str.charAt(i) != '\n') && (str.charAt(i) != '\t')) {
                temp = temp + str.charAt(i);
            }
        }
        return temp;
    }

    public static String data2hex(byte[] data) {
        if (data == null) {
            return null;
        }
        int len = data.length;
        StringBuffer buf = new StringBuffer(len * 2);
        for (int pos = 0; pos < len; ++pos) {
            buf.append(toHexChar(data[pos] >>> 4 & 0xF)).append(toHexChar(data[pos] & 0xF));
        }
        return buf.toString();
    }

    private static char toHexChar(int i) {
        if ((0 <= i) && (i <= 9)) {
            return (char) (48 + i);
        }
        return (char) (97 + i - 10);
    }

    public static String encryptUTF8(String plainText, String key) throws Exception {
        int i = (key.hashCode() < 0) ? key.hashCode() * -1 : key.hashCode();
        byte[] b = plainText.getBytes("UTF8");

        for (int j = 0; j < b.length; ++j) {
            b[j] = (byte) (b[j] ^ i);
        }
        return data2hex(b);
    }

    public static String decryptUTF8(String cipherText, String key) throws Exception {
        int i = (key.hashCode() < 0) ? key.hashCode() * -1 : key.hashCode();
        byte[] b = hex2data(cipherText);

        for (int j = 0; j < b.length; ++j) {
            b[j] = (byte) (b[j] ^ i);
        }

        return new String(b, "UTF8");
    }

}
