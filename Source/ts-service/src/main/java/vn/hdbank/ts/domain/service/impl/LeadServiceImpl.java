package vn.hdbank.ts.domain.service.impl;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vn.hdbank.ts.utils.URLUtilsType;
import vn.hdbank.ts.utils.WriteLog;
import vn.hdbank.ts.application.resource.*;
import vn.hdbank.ts.application.utils.ResultCode;
import vn.hdbank.ts.domain.service.LeadService;
import vn.hdbank.ts.domain.service.security.SignatureAgent;
import vn.hdbank.ts.infrastructure.database.LeadDao;

import vn.hdbank.ts.domain.object.handle.Constant;

@Service
public class LeadServiceImpl implements LeadService {
	private static final String className = LeadService.class.getSimpleName();
	private Gson gson = new Gson();

	@Autowired
	LeadDao leadDao;

	@Autowired
	SignatureAgent signatureAgent;

	@Value("${hdbank.ts.partner-id}")
	private String tsPartner;
	
	@Value("${hdbank.ts.valid-files}")
	private String validFiles;

	@Override
	public BasicResponse insertOrUpdateLead(LeadRequest request) {
		BasicResponse response = new BasicResponse();
		try {
			
			String phone = request.getPhone();
			String urlFull = request.getUrlFull();
			String fullName = request.getFullName();
			String requestId = request.getRequestId();
			String partnerId = request.getPartnerId();
			String signature = request.getSignature();
			String actionCode = request.getActionCode();
			String campaignId = request.getCampaignId();
			String leadStatus = request.getLeadStatusId();
			String nationalId = request.getNationalId();
			String requestTime = request.getRequestTime();
			File[] documentFiles = request.getDocumentFile();
			String fullNameUpperCase = request.getFullNameUpCase();
			
			boolean fileCheck = true;
			boolean isValidFile = true;
			if (documentFiles != null) {
				for (File file : documentFiles) {
					for (String validFile : validFiles.split(",")) {
						if (file.getFilename().toLowerCase().endsWith(validFile))
						{
							isValidFile = true;
							break;
						} else
							isValidFile = false;
					}
					
					if (!isValidFile)
						fileCheck = false;
				}
			}
			
			if (!fileCheck) {
				response.setCode(ResultCode.INVALID_FILE.getCode());
				response.setMessage(ResultCode.INVALID_FILE.getMessage());
				WriteLog.write(request.getRequestId(), className, "insertOrUpdateLead", "Invalid format of file");
				
			} else if (requestId.equals("") || partnerId.equals("") || actionCode.equals("") 
					|| requestTime.equals("") || campaignId.equals("") || signature.equals("")) 
			{
				
				response.setCode(ResultCode.TS_ERROR.getCode());
				response.setMessage("Lack of required infor");
				WriteLog.write(request.getRequestId(), className, "insertOrUpdateLead", "Lack of required infor");
				
			} else if (actionCode.equalsIgnoreCase("INSERT")) {
				
				if (fullName.equals("") || nationalId.equals("") || phone.equals("") 
						|| urlFull.equals("") || fullNameUpperCase.equals("") || leadStatus.equals("")) 
				{
					response.setCode(ResultCode.TS_ERROR.getCode());
					response.setMessage("Lack of required infor");
					WriteLog.write(request.getRequestId(), className, "insertLead", "Lack of required infor");
				} else {
					
        			String sigReqHashed = signatureAgent.getSignatureMd5(request, LeadRequest.class, 
            				URLUtilsType.LEAD, tsPartner, request.getRequestId() + " request");
            		if (!sigReqHashed.equalsIgnoreCase(signature)) {
            			response.setCode(ResultCode.SIGNATURE_NOT_MATCH.getCode());
            			response.setMessage(ResultCode.SIGNATURE_NOT_MATCH.getMessage());
            			
            		} else {
            			
						String insertRes = leadDao.insertRequest(request.getRequestId());
						if (insertRes.equals(Constant.SUCCESS)) {
							
							response = leadDao.insertOrUpdateLead(request);
							leadDao.deleteRequest(request.getRequestId());
							
						} else {
							response.setCode(ResultCode.REQUEST_ID_EXIST.getCode());
							response.setMessage(ResultCode.REQUEST_ID_EXIST.getMessage());
						}
            		}
				}
			} else if (actionCode.equalsIgnoreCase("UPDATE")) {
				
				if (fullName != null && !fullName.equals("") && (fullNameUpperCase == null || fullNameUpperCase.equals(""))) {
					
					response.setCode(ResultCode.TS_ERROR.getCode());
					response.setMessage("Lack of fullNameUpperCase");
					WriteLog.write(request.getRequestId(), className, "updateLead", "Lack of fullNameUpperCase");
					
				} else {
					String sigReqHashed = signatureAgent.getSignatureMd5(request, LeadRequest.class, URLUtilsType.LEAD,
							tsPartner, request.getRequestId() + " request");
					if (!sigReqHashed.equalsIgnoreCase(signature)) {
        				response.setCode(ResultCode.SIGNATURE_NOT_MATCH.getCode());
        				response.setMessage(ResultCode.SIGNATURE_NOT_MATCH.getMessage());

        			} else {
						String insertRes = leadDao.insertRequest(request.getRequestId());
						if (insertRes.equals(Constant.SUCCESS)) {
							
							response = leadDao.insertOrUpdateLead(request);
							leadDao.deleteRequest(request.getRequestId());
						} else {
							response.setCode(ResultCode.REQUEST_ID_EXIST.getCode());
							response.setMessage(ResultCode.REQUEST_ID_EXIST.getMessage());
						}
        			}
				}
			} else {
				response.setCode(ResultCode.TS_ERROR.getCode());
				response.setMessage("Action code invalid");
				WriteLog.write(request.getRequestId(), className, "insertOrUpdateLead", "Action code invalid");
			}

		} catch (Exception ex) {
			response.setCode(ResultCode.TS_ERROR.getCode());
			response.setMessage(ResultCode.TS_ERROR.getMessage());
			WriteLog.error(request.getRequestId(), className, "insertOrUpdateLead", gson.toJson(request), ex);
		}
		return response;
	}

	@Override
	public StatusResponse checkStatus(StatusRequest request) {
		StatusResponse response = new StatusResponse();
		try {
			
			String leadId = request.getLeadId();
			String requestId = request.getRequestId();
			String signature = request.getSignature();
			String serviceCode = request.getServiceCode();
			String requestTime = request.getRequestTime();
			
			if (requestId.equals("") || serviceCode.equals("") || requestTime.equals("") || leadId.equals("")
					|| signature.equals("")) 
			{
				
				response.setCode(ResultCode.TS_ERROR.getCode());
				response.setMessage("Lack of required infor");
				WriteLog.write(request.getRequestId(), className, "checkStatus", "Lack of required infor");
				
			} else {
				
				String sigReqHashed = signatureAgent.getSignatureMd5(request, StatusRequest.class, URLUtilsType.STATUS,
						tsPartner, request.getRequestId() + " request");
				if (!sigReqHashed.equalsIgnoreCase(signature)) {
        			response.setCode(ResultCode.SIGNATURE_NOT_MATCH.getCode());
        			response.setMessage(ResultCode.SIGNATURE_NOT_MATCH.getMessage());

        		} else {
					String insertRes = leadDao.insertRequest(request.getRequestId());
					if (insertRes.equals(Constant.SUCCESS)) {
						
						response = leadDao.getLeadStatus(request);
						leadDao.deleteRequest(request.getRequestId());
						String sigResHashed = signatureAgent.getSignatureMd5(response, StatusResponse.class,
								URLUtilsType.STATUS, tsPartner, request.getRequestId() + " response");
						response.setSignature(sigResHashed);
					} else {
						response.setCode(ResultCode.REQUEST_ID_EXIST.getCode());
						response.setMessage(ResultCode.REQUEST_ID_EXIST.getMessage());
					}
        		}
			}
		} catch (Exception ex) {
			response.setCode(ResultCode.TS_ERROR.getCode());
			response.setMessage(ResultCode.TS_ERROR.getMessage());
			WriteLog.error(request.getRequestId(), className, "checkStatus", gson.toJson(request), ex);
		}
		return response;
	}
}
