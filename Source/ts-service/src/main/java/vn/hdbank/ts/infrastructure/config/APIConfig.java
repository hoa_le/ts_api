package vn.hdbank.ts.infrastructure.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import vn.hdbank.ts.utils.WriteLog;
//import vn.hdbank.payoo.infrastructure.tcp.entities.GWOTPConnectionInfo;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

@Getter
@Setter
public class APIConfig {
    private static final String className = APIConfig.class.getSimpleName();

    private static APIConfig apiConfig;

    private int gwSocketMaxRetry = 5;
    private int gwMaxFailConfirm = 4;
    private String gwMaxFailLockToken;
    private String gwMaxFailLockUser;
    private String channel;
    private int instanceIndex = 0;
    private int soTimeout = 10000;

    public APIConfig(ApplicationReadyEvent event) {
        try {
            Properties prop = new Properties();
            Path currentWorkingDir = Paths.get("").toAbsolutePath();
            String filename = currentWorkingDir + "/config/OtpConfig.properties";
            InputStream input = new FileInputStream(filename);
            prop.load(input);

            this.setSoTimeout(Integer.parseInt(prop.getProperty("gw.sotimeout")));
            this.setGwSocketMaxRetry(Integer.parseInt(prop.getProperty("gw.socket.maxretry")));
            this.setGwMaxFailConfirm(Integer.parseInt(prop.getProperty("gw.maxfail.confirm")));
            this.setGwMaxFailLockToken(prop.getProperty("gw.maxfail.locktoken"));
            this.setGwMaxFailLockUser(prop.getProperty("gw.maxfail.lockuser"));
            this.setChannel(prop.getProperty("gw.channel"));

        } catch (Exception e) {
            WriteLog.error(null, className, "APIConfig()", "APIConfig init fail", e);
            if (event != null) {
                event.getApplicationContext().close();
                System.exit(-1);
            }
        }
    }

    public APIConfig() {

    }

    public static APIConfig getInstance() {
        if (apiConfig == null)
            apiConfig = new APIConfig(null);
        return apiConfig;
    }

    public void setApiOtp(ApplicationReadyEvent event) {
        apiConfig = new APIConfig(event);
    }
}
