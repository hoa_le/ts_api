package vn.hdbank.ts.common;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vn.hdbank.ts.common.entity.generic.ClientKeyObj;
import vn.hdbank.ts.common.entity.generic.NumberOfRequestObj;
import vn.hdbank.ts.common.entity.generic.RouteObj;
import vn.hdbank.ts.common.entity.generic.SessionObj;


public class CacheObject {
	private static final Logger logger = LoggerFactory.getLogger(CacheObject.class);
	
	// Instance object
    private static CacheObject instance;
    
    private List<String> listOfToken = new ArrayList<String>();;

    private Dictionary<String, Integer> currentRequest = new Hashtable<>();
    
    private Dictionary<String, String[]> securityChain = new Hashtable<String, String[]>();
    
    private Dictionary<String, RouteObj> routeMap = new Hashtable<String, RouteObj>();
    
    private Map<String, ClientKeyObj> clientKeyList = new HashMap<String, ClientKeyObj>();
    
    private Map<String, SessionObj> sessionList = new HashMap<String, SessionObj>();
	
    private Map<String, NumberOfRequestObj> numberOfRequest = new HashMap<String, NumberOfRequestObj>();
	
	private String serverPublicKey = "";
	private String serverPrivateKey="";

    private CacheObject() {
    	if (this.listOfToken == null)
    		listOfToken = new LinkedList<String>();
    	
    	if (this.currentRequest == null) {
    		currentRequest = new Hashtable<String, Integer>();
    	}
    	
    	if (this.securityChain == null) {
    		this.securityChain = new Hashtable<String, String[]>();
    	}
    	
    	if (this.routeMap == null) {
    		this.routeMap = new Hashtable<String, RouteObj>();
    	}
    	
    	if (this.sessionList == null) {
    		this.sessionList = new HashMap<String, SessionObj>();
    	}
    	
    	if (this.clientKeyList == null) {
    		this.clientKeyList = new HashMap<String, ClientKeyObj>();
    	}
    	
    	if (this.numberOfRequest == null) {
    		this.numberOfRequest = new HashMap<String, NumberOfRequestObj>();
    	}
    }

    // Thread safe singleton
    public static synchronized CacheObject getInstance() {
        if (instance == null) {
            instance = new CacheObject();
        }
        return instance;
    }
    
    public synchronized boolean putToken(String token) {
    	try {
			CacheObject.getInstance().listOfToken.add(token);
			return true;
		} catch (Exception e) {
			logger.error("", e);
			return false;
		}
    }
    
    public synchronized boolean removeToken(String token) {
    	try {
    		if (CacheObject.getInstance().listOfToken.contains(token)) {
    			listOfToken.remove(token);
    			return true;
    		} else 
    			return false;
		} catch (Exception e) {
			logger.error("", e);
			return false;
		}
    }
    
    public synchronized boolean putClientKeyObj(String key, ClientKeyObj ckObj) {
    	try {
    		CacheObject.getInstance().clientKeyList.put(key, ckObj);
			return true;
		} catch (Exception e) {
			logger.error("", e);
			return false;
		}
    }
    
    public synchronized boolean removeClientKeyObj(String key) {
    	try {
    		CacheObject cObj = CacheObject.getInstance();
    		Set<String> keySet = cObj.clientKeyList.keySet();
			Iterator<String> itr = keySet.iterator();
			while (itr.hasNext()) {
				String k = itr.next();
				if (key.equalsIgnoreCase(k)) {
					cObj.clientKeyList.remove(key);
					break;
				}
			}
			return true;
		} catch (Exception e) {
			logger.error("", e);
			return false;
		}
    }
    
    public synchronized boolean putSessionObj(String key, SessionObj sObj) {
    	try {
    		CacheObject.getInstance().sessionList.put(key, sObj);
			return true;
		} catch (Exception e) {
			logger.error("", e);
			return false;
		}
    }
    
    public synchronized boolean removeSessionObj(String key) {
    	try {
    		CacheObject cObj = CacheObject.getInstance();
    		Set<String> keySet = cObj.sessionList.keySet();
			Iterator<String> itr = keySet.iterator();
			while (itr.hasNext()) {
				String k = itr.next();
				if (key.equalsIgnoreCase(k)) {
					cObj.sessionList.remove(key);
					break;
				}
			}
			return true;
		} catch (Exception e) {
			logger.error("", e);
			return false;
		}
    }
    
    
    public synchronized boolean putNumberOfRequestObj(String key, NumberOfRequestObj nORObj) {
    	try {
    		CacheObject.getInstance().numberOfRequest.put(key, nORObj);
			return true;
		} catch (Exception e) {
			logger.error("", e);
			return false;
		}
    }
    
    public synchronized boolean removeNumberOfRequestObj(String key) {
    	try {
    		CacheObject cObj = CacheObject.getInstance();
    		Set<String> keySet = cObj.numberOfRequest.keySet();
			Iterator<String> itr = keySet.iterator();
			while (itr.hasNext()) {
				String k = itr.next();
				if (key.equalsIgnoreCase(k)) {
					cObj.numberOfRequest.remove(key);
					break;
				}
			}
			return true;
		} catch (Exception e) {
			logger.error("", e);
			return false;
		}
    }
    
	public List<String> getListOfToken() {
		return listOfToken;
	}

	public void setListOfToken(List<String> listOfToken) {
		this.listOfToken = listOfToken;
	}

	public Dictionary<String, Integer> getCurrentRequest() {
		return currentRequest;
	}

	public void setCurrentRequest(Dictionary<String, Integer> currentRequest) {
		this.currentRequest = currentRequest;
	}

	public Dictionary<String, String[]> getSecurityChain() {
		return securityChain;
	}

	public void setSecurityChain(Dictionary<String, String[]> securityChain) {
		this.securityChain = securityChain;
	}
	
	public String getServerPublicKey() {
		return serverPublicKey;
	}

	public void setServerPublicKey(String serverPublicKey) {
		this.serverPublicKey = serverPublicKey;
	}

	public String getServerPrivateKey() {
		return serverPrivateKey;
	}

	public void setServerPrivateKey(String serverPrivateKey) {
		this.serverPrivateKey = serverPrivateKey;
	}

	public Dictionary<String, RouteObj> getRouteMap() {
		return routeMap;
	}

	public void setRouteMap(Dictionary<String, RouteObj> routeMap) {
		this.routeMap = routeMap;
	}

	public Map<String, ClientKeyObj> getClientKeyList() {
		return clientKeyList;
	}

	public void setClientKeyList(Map<String, ClientKeyObj> clientKeyList) {
		this.clientKeyList = clientKeyList;
	}

	public Map<String, SessionObj> getSessionList() {
		return sessionList;
	}

	public void setSessionList(Map<String, SessionObj> sessionList) {
		this.sessionList = sessionList;
	}

	public Map<String, NumberOfRequestObj> getNumberOfRequest() {
		return numberOfRequest;
	}

	public void setNumberOfRequest(Map<String, NumberOfRequestObj> numberOfRequest) {
		this.numberOfRequest = numberOfRequest;
	}
	
	
}
