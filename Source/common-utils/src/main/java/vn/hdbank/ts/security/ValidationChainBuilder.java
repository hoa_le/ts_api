package vn.hdbank.ts.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ValidationChainBuilder {
	private static final Logger logger = LoggerFactory.getLogger(ValidationChainBuilder.class);
	private static ValidationChainBuilder vcb = null;
	private ValidationChainBuilder() {
		
	}
	
	public static synchronized ValidationChainBuilder getInstance() {
		if (vcb == null) {
			vcb = new ValidationChainBuilder();
		}
		return vcb;
	}
	

}
