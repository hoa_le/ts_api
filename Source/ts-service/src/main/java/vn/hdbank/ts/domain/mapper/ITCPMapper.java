package vn.hdbank.ts.domain.mapper;

import org.json.JSONObject;

public interface ITCPMapper extends IMapper<Object, JSONObject> { };