package vn.hdbank.ts.domain.service.impl;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vn.hdbank.ts.utils.URLUtilsType;
import vn.hdbank.ts.utils.WriteLog;
import vn.hdbank.ts.application.resource.*;
import vn.hdbank.ts.application.utils.ResultCode;
import vn.hdbank.ts.domain.object.handle.Constant;
import vn.hdbank.ts.domain.service.EligibleService;
import vn.hdbank.ts.domain.service.security.SignatureAgent;
import vn.hdbank.ts.infrastructure.database.EligibleDao;
import vn.hdbank.ts.infrastructure.database.LeadDao;

@Service
public class EligibleServiceImpl implements EligibleService {
	private static final String className = EligibleService.class.getSimpleName();
	private Gson gson = new Gson();

	@Autowired
	EligibleDao eligibleDao;

	@Autowired
	LeadDao leadDao;

	@Autowired
	SignatureAgent signatureAgent;

	@Value("${hdbank.ts.partner-id}")
	private String tsPartner;

	@Override
	public EligibleResponse checkEligible(EligibleRequest request) {
		EligibleResponse response = new EligibleResponse();
		try {
			
			String requestId = request.getRequestId();
			String partnerId = request.getPartnerId();
			String requestTime = request.getRequestTime();
			String campaignId = request.getCampaignId();
			String fullName = request.getFullName();
			String nationalId = request.getNationalId();
			String phone = request.getPhone();
			String leadId = request.getLeadId();
			String signature = request.getSignature();

			if (!((leadId == null || leadId.equals("")) 
					&& ((fullName ==null || fullName.equals("")) 
							|| (phone == null ||phone.equals(""))
							|| (nationalId == null || nationalId.equals("")))) 
					&& !(requestId.equals("") 
							|| partnerId.equals("") 
							|| requestTime.equals("")
							|| campaignId.equals("") 
							|| signature.equals(""))) 
			{
				
				String sigReqHashed = signatureAgent.getSignatureMd5(request, EligibleRequest.class,
						URLUtilsType.ELIGIBLE, tsPartner, request.getRequestId() + " request");
				if (!sigReqHashed.equalsIgnoreCase(signature)) {
        			response.setCode(ResultCode.SIGNATURE_NOT_MATCH.getCode());
        			response.setMessage(ResultCode.SIGNATURE_NOT_MATCH.getMessage());
        			
        		} else {
					String insertRes = leadDao.insertRequest(request.getRequestId());
					if (insertRes.equals(Constant.SUCCESS)) {
						
						response = eligibleDao.checkEligible(request);
						leadDao.deleteRequest(request.getRequestId());
						String sigResHashed = signatureAgent.getSignatureMd5(response, EligibleResponse.class,
								URLUtilsType.ELIGIBLE, tsPartner, request.getRequestId() + " response");
						response.setSignature(sigResHashed);
						
					} else {
						response.setCode(ResultCode.REQUEST_ID_EXIST.getCode());
						response.setMessage(ResultCode.REQUEST_ID_EXIST.getMessage());
					}
        		}
			} else {
				response.setCode(ResultCode.TS_ERROR.getCode());
				WriteLog.write(request.getRequestId(), className, "checkEligible", "Lack of required infor");
				response.setMessage("Lack of required infor");
			}

		} catch (Exception ex) {
			response.setCode(ResultCode.TS_ERROR.getCode());
			WriteLog.error(request.getRequestId(), className, "checkEligible", gson.toJson(request), ex);
			response.setMessage(ResultCode.TS_ERROR.getMessage());
		}
		
		return response;
	}
}
