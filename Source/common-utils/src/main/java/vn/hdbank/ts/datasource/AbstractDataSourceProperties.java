package vn.hdbank.ts.datasource;

public abstract class AbstractDataSourceProperties implements DataSourceProperties{
	private String dbUserName;
	private String dbPassword;
	private String dbUrl;
	private String dbSchema;
	private String connectionName;
	private int minPoolSize;
	private int maxPoolSize;
	private int connectionHarvestMaxCount;
	private int connectionHarvestTriggerCount;
	private long maxConnectionReuseTime;
	private int maxConnectionReuseCount;
	private int abandonedConnectionTimeout;
	private int connectionWaitTimeout;
	private int inactiveConnectionTimeout;
	private int timeToLiveConnectionTimeout;
	private int timeoutCheckInterval;
	private int maxStatements;
	private String driverClass;
	
	public String getDbUserName() {
		return dbUserName;
	}
	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}
	public String getDbPassword() {
		return dbPassword;
	}
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	public String getDbUrl() {
		return dbUrl;
	}
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}
	public String getDbSchema() {
		return dbSchema;
	}
	public void setDbSchema(String dbSchema) {
		this.dbSchema = dbSchema;
	}
	public String getConnectionName() {
		return connectionName;
	}
	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}
	public int getMinPoolSize() {
		return minPoolSize;
	}
	public void setMinPoolSize(int minPoolSize) {
		this.minPoolSize = minPoolSize;
	}
	public int getMaxPoolSize() {
		return maxPoolSize;
	}
	public void setMaxPoolSize(int maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}
	public int getConnectionHarvestMaxCount() {
		return connectionHarvestMaxCount;
	}
	public void setConnectionHarvestMaxCount(int connectionHarvestMaxCount) {
		this.connectionHarvestMaxCount = connectionHarvestMaxCount;
	}
	public int getConnectionHarvestTriggerCount() {
		return connectionHarvestTriggerCount;
	}
	public void setConnectionHarvestTriggerCount(int connectionHarvestTriggerCount) {
		this.connectionHarvestTriggerCount = connectionHarvestTriggerCount;
	}
	public long getMaxConnectionReuseTime() {
		return maxConnectionReuseTime;
	}
	public void setMaxConnectionReuseTime(long maxConnectionReuseTime) {
		this.maxConnectionReuseTime = maxConnectionReuseTime;
	}
	public int getMaxConnectionReuseCount() {
		return maxConnectionReuseCount;
	}
	public void setMaxConnectionReuseCount(int maxConnectionReuseCount) {
		this.maxConnectionReuseCount = maxConnectionReuseCount;
	}
	public int getAbandonedConnectionTimeout() {
		return abandonedConnectionTimeout;
	}
	public void setAbandonedConnectionTimeout(int abandonedConnectionTimeout) {
		this.abandonedConnectionTimeout = abandonedConnectionTimeout;
	}
	public int getConnectionWaitTimeout() {
		return connectionWaitTimeout;
	}
	public void setConnectionWaitTimeout(int connectionWaitTimeout) {
		this.connectionWaitTimeout = connectionWaitTimeout;
	}
	public int getInactiveConnectionTimeout() {
		return inactiveConnectionTimeout;
	}
	public void setInactiveConnectionTimeout(int inactiveConnectionTimeout) {
		this.inactiveConnectionTimeout = inactiveConnectionTimeout;
	}
	public int getTimeToLiveConnectionTimeout() {
		return timeToLiveConnectionTimeout;
	}
	public void setTimeToLiveConnectionTimeout(int timeToLiveConnectionTimeout) {
		this.timeToLiveConnectionTimeout = timeToLiveConnectionTimeout;
	}
	public int getTimeoutCheckInterval() {
		return timeoutCheckInterval;
	}
	public void setTimeoutCheckInterval(int timeoutCheckInterval) {
		this.timeoutCheckInterval = timeoutCheckInterval;
	}
	public int getMaxStatements() {
		return maxStatements;
	}
	public void setMaxStatements(int maxStatements) {
		this.maxStatements = maxStatements;
	}
	public String getDriverClass() {
		return driverClass;
	}
	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}
	
	
	
}
