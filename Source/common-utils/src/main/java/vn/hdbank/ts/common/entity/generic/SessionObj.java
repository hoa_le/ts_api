package vn.hdbank.ts.common.entity.generic;

import java.util.Date;

public class SessionObj {
	private String username;
	private String token;
	private String clientPublicKey;
	private String shareKey;
	private Date lastRequestTime;
	private Date createdTime;
	 
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getShareKey() {
		return shareKey;
	}
	public void setShareKey(String shareKey) {
		this.shareKey = shareKey;
	}
	public Date getLastRequestTime() {
		return lastRequestTime;
	}
	public void setLastRequestTime(Date lastRequestTime) {
		this.lastRequestTime = lastRequestTime;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public String getClientPublicKey() {
		return clientPublicKey;
	}
	public void setClientPublicKey(String clientPublicKey) {
		this.clientPublicKey = clientPublicKey;
	}
 
 
}
