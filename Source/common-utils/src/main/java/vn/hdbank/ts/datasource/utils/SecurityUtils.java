package vn.hdbank.ts.datasource.utils;

public class SecurityUtils {

    public static String decryptPassword(String cipherPass, String key) {
        //logger.info("[Encryption] Decrypt ciphertext " + cipherPass);
        String out = "";
        try {
            int i = (key.hashCode() < 0) ? key.hashCode() * -1 : key.hashCode();
            byte[] b = hex2data(cipherPass);

            for (int j = 0; j < b.length; ++j) {
                b[j] = (byte) (b[j] ^ i);
            }

            StringBuffer sb = new StringBuffer(b.length);
            for (int j = 0; j < b.length; ++j) {
                sb.append((char) b[j]);
            }
            out = sb.toString();
        } catch (Exception ex) {

        }
        return out;
    }

    public static byte[] hex2data(String str) {
        if (str == null) {
            return new byte[0];
        }
        int len = str.length();
        char[] hex = str.toCharArray();
        byte[] buf = new byte[len / 2];

        for (int pos = 0; pos < len / 2; ++pos) {
            buf[pos] = (byte) (toDataNibble(hex[(2 * pos)]) << 4 & 0xF0 | toDataNibble(hex[(2 * pos + 1)]) & 0xF);
        }
        return buf;
    }

    public static byte toDataNibble(char c) {
        if (('0' <= c) && (c <= '9')) {
            return (byte) ((byte) c - 48);
        }
        if (('a' <= c) && (c <= 'f')) {
            return (byte) ((byte) c - 97 + 10);
        }
        if (('A' <= c) && (c <= 'F')) {
            return (byte) ((byte) c - 65 + 10);
        }
        return -1;
    }
}
