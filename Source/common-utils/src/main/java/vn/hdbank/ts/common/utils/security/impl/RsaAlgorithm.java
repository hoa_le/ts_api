package vn.hdbank.ts.common.utils.security.impl;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

import vn.hdbank.ts.common.utils.security.AbstractCryptography;

public class RsaAlgorithm extends AbstractCryptography{
	
	public RsaAlgorithm(String publicKey, String privateKey) {
        super(publicKey, privateKey);
    }

    public String createPublicPrivateKey() throws Exception {
    	SecureRandom sr = new SecureRandom();
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		kpg.initialize(2048, sr);

		KeyPair kp = kpg.genKeyPair();
		PublicKey publicKey = kp.getPublic();
		PrivateKey privateKey = kp.getPrivate();
		String p1 = Base64.getEncoder().encodeToString(publicKey.getEncoded());
		String p2 = Base64.getEncoder().encodeToString(privateKey.getEncoded());
		return p1 + "#"
				+ p2;
    }

    @Override
	public String encryptMethod(String data, String internalPublicKey) throws Exception {
        // Create public key
        X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.getDecoder().decode(internalPublicKey));
        KeyFactory factory = KeyFactory.getInstance("RSA");
        PublicKey pubKey = factory.generatePublic(spec);

        // Encrypt data using public key
        Cipher c = Cipher.getInstance("RSA");
        c.init(Cipher.ENCRYPT_MODE, pubKey);
        byte encryptOut[] = c.doFinal(data.getBytes());
        return Base64.getEncoder().encodeToString(encryptOut);
    }

    @Override
    protected String decryptMethod(String data, String internalPrivateKey) throws Exception {
        // Create private key
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(internalPrivateKey));
        KeyFactory factory = KeyFactory.getInstance("RSA");
        PrivateKey priKey = factory.generatePrivate(spec);

        // Decrypt data using private key
        Cipher c = Cipher.getInstance("RSA");
        c.init(Cipher.DECRYPT_MODE, priKey);
        byte decryptOut[] = c.doFinal(Base64.getDecoder().decode(data));
        return new String(decryptOut);
    }

    @Override
	protected String createSignatureMethod(String data, String internalPrivateKey) throws Exception {
		byte[] privateKeyByte = org.apache.commons.codec.binary.Base64.decodeBase64(internalPrivateKey);
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyByte);

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");

		PrivateKey kp = keyFactory.generatePrivate(keySpec);

		byte[] dataBytes = data.getBytes("UTF-8");

		Signature signature = Signature.getInstance("SHA256withRSA");
		signature.initSign(kp);
		signature.update(dataBytes);
		return org.apache.commons.codec.binary.Base64.encodeBase64String(signature.sign());
	}

    @Override
    protected boolean verifySignatureMethod(String data, String sign, String internalPublicKey) throws Exception {
    	//System.out.println("public key: " + internalPublicKey);

		byte[] publicBytes = org.apache.commons.codec.binary.Base64.decodeBase64(internalPublicKey);
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");

		PublicKey kp = keyFactory.generatePublic(keySpec);

		byte[] dataBytes = data.getBytes("UTF-8");
		byte[] signByte = sign.getBytes("UTF-8");

		Signature signature = Signature.getInstance("SHA256withRSA");
		signature.initVerify(kp);
		signature.update(dataBytes);
		return signature.verify(org.apache.commons.codec.binary.Base64.decodeBase64(signByte));
    }
}
