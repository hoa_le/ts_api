package vn.hdbank.ts.common;

public enum EnumResultCode {SUCCESS("00", "Success"),
    SYSTEM_ERROR("099",
            "Xin lỗi Quý khách, dịch vụ này đang được bảo trì. Quý khách vui lòng thử lại giao dịch sau ít phút hoặc liên hệ 19006060 để biết thêm chi tiết."),
    USER_NO_TOKEN("29",
            "Tài khoản của Quý khách chưa đăng ký Phương thức xác thực. Quý khách vui lòng đăng ký Phương thức xác thực để thực hiện giao dịch."),
    OTP_INVALID("07",
            "Mật khẩu giao dịch không đúng. Quý khách vui lòng nhập lại."),
    OTP_EXPIRE("08",
            "Mật khẩu giao dịch đã hết hạn. Quý khách vui lòng thực hiện lại giao dịch."),
    USER_NO_TOKEN_ACTIVE("12",
            "Tài khoản của Quý khách chưa kích hoạt Phương thức xác thực. Quý khách vui lòng kích hoạt Phương thức xác thực để thực hiện giao dịch."),
    MAX_REQUEST_OTP("15",
            "Phương thức xác thực của Quý khách đã bị khóa do không xác nhận quá số lần quy định. Quý khách vui lòng liên hệ tổng đài 19006060 để được hỗ trợ."),
    MAX_VERIFY_OTP("16",
            "Phương thức xác thực của Quý khách đã bị khóa do nhập sai mật khẩu quá số lần quy định. Quý khách vui lòng liên hệ tổng đài 19006060 để được hỗ trợ."),
    OTP_ERROR("19",
            "Đã có lỗi xảy ra trong quá trình xác thực. Quý khách vui lòng thực hiện lại hoặc liên hệ tổng đài 19006060 để được hỗ trợ."),
    TOKEN_OUT_OF_SYNC("28",
            "HDBank OTP của Quý khách đã mất đồng bộ với máy chủ. Quý khách vui lòng liên hệ tổng đài 19006060 để được hỗ trợ.");



    private String code;
    private String message;

    EnumResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static EnumResultCode findByCode(String code) {
        EnumResultCode[] var1 = values();
        int len = var1.length;

        for (int var3 = 0; var3 < len; ++var3) {
            EnumResultCode type = var1[var3];

            if (type.getCode().equals(code)) {
                return type;
            }
        }

        return SYSTEM_ERROR;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public boolean equals(final String value) {
        return code.equalsIgnoreCase(value);
    }
}
