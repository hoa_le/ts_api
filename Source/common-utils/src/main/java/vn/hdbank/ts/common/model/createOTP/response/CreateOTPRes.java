package vn.hdbank.ts.common.model.createOTP.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.hdbank.ts.common.model.ResponseStatus;
import vn.hdbank.ts.common.model.createOTP.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateOTPRes {

    private Header header;
    private ResponseStatus responseStatus;
    private BodyRes bodyRes;

    public Header getHeader() { return header; }
    public void setHeader(Header value) { this.header = value; }

    public ResponseStatus getResponseStatus() { return responseStatus; }
    public void setResponseStatus(ResponseStatus value) { this.responseStatus = value; }

    public BodyRes getBodyRes() { return bodyRes; }
    public void setBodyRes(BodyRes value) { this.bodyRes = value; }
}
