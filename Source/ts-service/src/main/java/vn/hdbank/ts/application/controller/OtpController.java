package vn.hdbank.ts.application.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vn.hdbank.ts.utils.URLUtilsType;
import vn.hdbank.ts.utils.WriteLog;
import vn.hdbank.ts.application.resource.BasicResponse;
import vn.hdbank.ts.application.resource.OtpRequest;
import vn.hdbank.ts.application.resource.VerifyOtpRequest;
import vn.hdbank.ts.application.utils.Utils;
import vn.hdbank.ts.domain.service.OtpService;

@RestController
@RequestMapping("/api/hdb-ts-card/")
public class OtpController {
    private static final String className = OtpController.class.getSimpleName();
    private Gson gson = new Gson();

    @Autowired
    private OtpService otpService;

    @RequestMapping(value = URLUtilsType.REQUEST_OTP, method = RequestMethod.POST)
    public ResponseEntity<?> Request_OTP(@RequestBody OtpRequest request) {
        ResponseEntity<?> responseEntity = null;
        BasicResponse response = new BasicResponse();
        try {
        	WriteLog.write(request.getRequestId(), className, "checkOtp", String.format("Request: %s", gson.toJson(request)));
            response = otpService.requestOtp(request);

            responseEntity = Utils.getResponseEntity(request, response, response.getCode(), URLUtilsType.REQUEST_OTP,request.getPartnerId());
            WriteLog.write(request.getRequestId(), className, "checkOtp", String.format("Response: %s", gson.toJson(response)));
        } catch (Exception ex) {
            WriteLog.error(request.getRequestId(), className, "checkOtp", gson.toJson(request), ex);
            responseEntity = Utils.responseError(request, className, "checkOtp", request.getRequestId());
        }
        return responseEntity;
    }
    
    @RequestMapping(value = URLUtilsType.VERIFY_OTP, method = RequestMethod.POST)
    public ResponseEntity<?> Check_OTP(@RequestBody VerifyOtpRequest request) {
        ResponseEntity<?> responseEntity = null;
        BasicResponse response = new BasicResponse();
        try {
        	WriteLog.write(request.getRequestId(), className, "verifyOtp", String.format("Request: %s", gson.toJson(request)));
            response = otpService.verifyOtp(request);

            responseEntity = Utils.getResponseEntity(request, response, response.getCode(), URLUtilsType.REQUEST_OTP,request.getPartnerId());
            WriteLog.write(request.getRequestId(), className, "verifyOtp", String.format("Response: %s", gson.toJson(response)));
        } catch (Exception ex) {
            WriteLog.error(request.getRequestId(), className, "verifyOtp", gson.toJson(request), ex);
            responseEntity = Utils.responseError(request, className, "verifyOtp", request.getRequestId());
        }
        return responseEntity;
    }

}
