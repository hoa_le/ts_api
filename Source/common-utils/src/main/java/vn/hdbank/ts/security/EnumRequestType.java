package vn.hdbank.ts.security;

public enum EnumRequestType {
	SymmetricCryptography,
    AsymmetricCryptography
}
