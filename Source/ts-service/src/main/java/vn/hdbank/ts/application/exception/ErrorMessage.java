package vn.hdbank.ts.application.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @author ThuongDc
 * @since 10/5/2020
 */
@Setter
@Getter
public class ErrorMessage {

    private String responseTime;

    private String code;

    private String message;

    private String signature;

}