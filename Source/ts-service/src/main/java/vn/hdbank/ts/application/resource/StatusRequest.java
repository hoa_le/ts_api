package vn.hdbank.ts.application.resource;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class StatusRequest extends BasicRequest {
	@NotNull
	private String serviceCode;
	@NotNull
    private String leadId;
}
