package vn.hdbank.ts.application.resource;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatusResponse extends BasicResponse {
	@NotNull
	String statusName;
	@NotNull
	String statusCode;
	String supportCode;
    private String signature;
}
