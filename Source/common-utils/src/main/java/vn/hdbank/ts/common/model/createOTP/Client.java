package vn.hdbank.ts.common.model.createOTP;

import java.util.List;

public class Client {
    private String sourceAppID;
    private List<String> targetAppIDs;
    private UserDetail userDetail;

    public String getSourceAppID() { return sourceAppID; }
    public void setSourceAppID(String value) { this.sourceAppID = value; }

    public List<String> getTargetAppIDs() { return targetAppIDs; }
    public void setTargetAppIDs(List<String> value) { this.targetAppIDs = value; }

    public UserDetail getUserDetail() { return userDetail; }
    public void setUserDetail(UserDetail value) { this.userDetail = value; }
}
