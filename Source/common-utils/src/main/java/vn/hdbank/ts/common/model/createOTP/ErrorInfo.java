package vn.hdbank.ts.common.model.createOTP;

public class ErrorInfo {
    private String errorCode;

    private String errorDesc;

    private String sourceAppID;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getSourceAppID() {
        return sourceAppID;
    }

    public void setSourceAppID(String sourceAppID) {
        this.sourceAppID = sourceAppID;
    }
}
