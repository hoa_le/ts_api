package vn.hdbank.ts.infrastructure.database;

import vn.hdbank.ts.application.resource.BasicResponse;
import vn.hdbank.ts.application.resource.LeadRequest;
import vn.hdbank.ts.application.resource.StatusRequest;
import vn.hdbank.ts.application.resource.StatusResponse;

public interface LeadDao {

	BasicResponse insertOrUpdateLead(LeadRequest request);
	StatusResponse getLeadStatus(StatusRequest request);
	
	String insertRequest(String requestID);
	void deleteRequest(String requestID);
}
