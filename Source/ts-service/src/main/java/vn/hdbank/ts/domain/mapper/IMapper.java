package vn.hdbank.ts.domain.mapper;

import java.util.List;

@SuppressWarnings("hiding")
public interface IMapper<Object, Entity> {

    Object toObject(Entity entity);

    Entity toEntity(Object object);

    List<Object> toListObject(List<Entity> entities);
}