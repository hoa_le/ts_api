package vn.hdbank.ts.application.configuration.db;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import vn.hdbank.ts.datasource.AbstractDataSourceProperties;

@ConfigurationProperties(prefix = "db.ecrm")
@Configuration("ecrmDataSourceProperties")
public class ECRMDataSourceProperties extends AbstractDataSourceProperties {
	public ECRMDataSourceProperties() {
		super();
	}
}
