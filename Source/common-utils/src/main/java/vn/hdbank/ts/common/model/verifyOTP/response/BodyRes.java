package vn.hdbank.ts.common.model.verifyOTP.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.hdbank.ts.common.model.createOTP.response.Data;
import vn.hdbank.ts.common.model.createOTP.response.Response;
import vn.hdbank.ts.common.model.verifyOTP.request.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyRes {

    private String functionCode;

    //private Request request;

    private Response response;

    private Data data;

    public String getFunctionCode() { return functionCode; }
    public void setFunctionCode(String value) { this.functionCode = value; }

//    public Request getRequest() {
//        return request;
//    }
//
//    public void setRequest(Request request) {
//        this.request = request;
//    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
