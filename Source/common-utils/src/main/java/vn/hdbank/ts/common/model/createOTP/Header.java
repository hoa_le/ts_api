package vn.hdbank.ts.common.model.createOTP;

public class Header {
    private Common common;
    private Client client;

    public Common getCommon() { return common; }
    public void setCommon(Common value) { this.common = value; }

    public Client getClient() { return client; }
    public void setClient(Client value) { this.client = value; }
}
