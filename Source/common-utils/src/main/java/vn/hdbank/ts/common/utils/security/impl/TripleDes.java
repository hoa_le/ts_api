package vn.hdbank.ts.common.utils.security.impl;

import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import vn.hdbank.ts.common.utils.security.AbstractCryptography;

//Key: HG58YZ3CR9
public final class TripleDes extends AbstractCryptography {

 public TripleDes(String publicKey, String privateKey) {
     super(publicKey, privateKey);
 }

 @Override
 protected String encryptMethod(String data, String internalPublicKey) throws Exception {
     // create a binary key from the argument key (seed)
     /*SecureRandom sr = new SecureRandom(internalPublicKey.getBytes());
     KeyGenerator kg = KeyGenerator.getInstance("DESede");
     kg.init(sr);
     SecretKey sk = kg.generateKey();*/
	 
     // create an instance of cipher
	 KeySpec keySpec = new DESedeKeySpec(internalPublicKey.getBytes("UTF-8"));
	 SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("DESede");
	 SecretKey sk = secretKeyFactory.generateSecret(keySpec);
     Cipher cipher = Cipher.getInstance("DESede");
     cipher.init(Cipher.ENCRYPT_MODE, sk);
     byte[] encrypted = cipher.doFinal(data.getBytes());
     return Base64.getEncoder().encodeToString(encrypted);
 }

 @Override
 protected String decryptMethod(String data, String internalPrivateKey) throws Exception {
     // create a binary key from the argument key (seed)
     /*SecureRandom sr = new SecureRandom(internalPrivateKey.getBytes());
     KeyGenerator kg = KeyGenerator.getInstance("DESede");
     kg.init(sr);
     SecretKey sk = kg.generateKey();*/
	 KeySpec keySpec = new DESedeKeySpec(internalPrivateKey.getBytes("UTF-8"));
	 SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("DESede");
	 SecretKey sk = secretKeyFactory.generateSecret(keySpec);
     // do the decryption with that key
     Cipher cipher = Cipher.getInstance("DESede");
     cipher.init(Cipher.DECRYPT_MODE, sk);
     byte[] decrypted = cipher.doFinal(Base64.getDecoder().decode(data));

     return new String(decrypted);
 }

 @Override
 protected String createSignatureMethod(String data, String internalPrivateKey) throws Exception {
     return null;
 }

 @Override
 protected boolean verifySignatureMethod(String data, String sign, String internalPublicKey) throws Exception {
     return false;
 }
}
