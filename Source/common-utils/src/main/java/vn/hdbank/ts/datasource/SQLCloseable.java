package vn.hdbank.ts.datasource;

import java.sql.SQLException;

/**
 * @author ThuongDc
 * @since 8/14/2020
 */
public interface SQLCloseable extends AutoCloseable {
    @Override
    public void close() throws SQLException;
}
