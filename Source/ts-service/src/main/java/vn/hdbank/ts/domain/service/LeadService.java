package vn.hdbank.ts.domain.service;

import vn.hdbank.ts.application.resource.*;

public interface LeadService {

	BasicResponse insertOrUpdateLead(LeadRequest request);
	StatusResponse checkStatus(StatusRequest request);
}

