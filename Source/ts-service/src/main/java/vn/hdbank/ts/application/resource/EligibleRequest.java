package vn.hdbank.ts.application.resource;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EligibleRequest extends BasicRequest {

	private String partnerId;

	private String campaignId;

	private String fullName;

	private String nationalId;

	private String phone;

	private String leadId;
}
