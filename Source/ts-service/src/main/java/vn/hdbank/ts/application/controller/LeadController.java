package vn.hdbank.ts.application.controller;

import com.google.gson.Gson;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vn.hdbank.ts.utils.URLUtilsType;
import vn.hdbank.ts.utils.WriteLog;
import vn.hdbank.ts.application.resource.BasicResponse;
import vn.hdbank.ts.application.resource.LeadRequest;
import vn.hdbank.ts.application.resource.StatusRequest;
import vn.hdbank.ts.application.resource.StatusResponse;
import vn.hdbank.ts.application.utils.Utils;
import vn.hdbank.ts.domain.service.LeadService;

@RestController
@RequestMapping("/api/hdb-ts-card/")
public class LeadController {
    private static final String className = LeadController.class.getSimpleName();
    private Gson gson = new Gson();

    @Autowired
    private LeadService leadService;
    
    @RequestMapping(value = URLUtilsType.LEAD, method = RequestMethod.POST)
    public ResponseEntity<?> LM_UpdateLead(@RequestBody LeadRequest request) {
        ResponseEntity<?> responseEntity = null;
        BasicResponse response = new BasicResponse();
        
        Date date = new Date();
        long timeMilli = date.getTime();
        try {
        	WriteLog.write(request.getRequestId(), className, "insertOrUpdateLead", String.format("Request: %s", gson.toJson(request)));
            response = leadService.insertOrUpdateLead(request);

            responseEntity = Utils.getResponseEntity(request, response, response.getCode(), URLUtilsType.LEAD,request.getPartnerId());
            WriteLog.write(request.getRequestId(), className, "insertOrUpdateLead", String.format("Response: %s", gson.toJson(response)));
        } catch (Exception ex) {
            WriteLog.error(request.getRequestId(), className, "insertOrUpdateLead", gson.toJson(request), ex);
            responseEntity = Utils.responseError(request, className, "insertOrUpdateLead", request.getRequestId());
        } finally {
        	WriteLog.write(request.getRequestId(), className, "insertOrUpdateLead: processTime",  (new Date().getTime() -timeMilli ));
        }
        return responseEntity;
    }
    
    @RequestMapping(value = URLUtilsType.STATUS, method = RequestMethod.POST)
    public ResponseEntity<?> LM_CheckStatus(@RequestBody StatusRequest request) {
        ResponseEntity<?> responseEntity = null;
        StatusResponse response = new StatusResponse();
        try {
        	WriteLog.write(request.getRequestId(), className, "checkStatus", String.format("Request: %s", gson.toJson(request)));
            response = leadService.checkStatus(request);

            responseEntity = Utils.getResponseEntity(request, response, response.getCode(), URLUtilsType.LEAD, "");
            WriteLog.write(request.getRequestId(), className, "checkStatus", String.format("Response: %s", gson.toJson(response)));
        } catch (Exception ex) {
            WriteLog.error(request.getRequestId(), className, "checkStatus", gson.toJson(request), ex);
            responseEntity = Utils.responseError(request, className, "checkStatus", request.getRequestId());
        }
        return responseEntity;
    }

}
