package vn.hdbank.ts.application.resource;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class VerifyOtpRequest extends BasicRequest {
	@NotNull
	private String partnerId;
	@NotNull
    private String otp;
    @NotNull
    private String mobile;
}
