package vn.hdbank.ts;

public enum EnumResultCode {
	SUCCESS("00", "Success"),
    FORMAT_ERROR("001", "Format Error/Invalid"),
    INVALID_PARAM("002", "Invalid Parameter"),
    AUTHORIZING_NOT_AVAILABLE("003", "Authorizing Not Available"),
    NOT_SIGNED_ON("004", "Processor Not Signed On"),
    REQUEST_IS_OVERLOAD("005", "Over load"),
    NOT_SUPPORT_REQUEST("006", "Not Support Request"),    
    SERVICE_NOT_FOUND("007", "Service not found"),
    SERVICE_NOT_READY("008", "Service not ready"),
    REQUEST_ID_EXIST("009", "Request is exist "),
    INVALID_TOKEN("010","Invalid Token"),
    INVALID_CLIENT_REQUEST("011","Invalid Client Request"),
    INVALID_SESSION("012","Invalid session"),
    EXPIRED_SESSION("013","Expired session"),
    SYSTEM_ERROR("099", "System Error"),
    DATABASE_ERROR("100", "Database error"),

	//VALIDATION ERROR
	DECRYPT_ERROR("11","Cannot decrypt message"),
	INPUT_VALIDATE_FAIL("12","input data validation fail"),
	SPECIAL_CHARATER_VALIDATE_FAIL("13","special charater validation fail"),
	EXPIRE_TOKEN("14","expire token"),
	INVALID_SIGNATURE("15", "Invalid Signature");
    private String code;
    private String message;


    EnumResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static EnumResultCode findByCode(String code) {
        EnumResultCode[] var1 = values();
        int len = var1.length;

        for (int var3 = 0; var3 < len; ++var3) {
            EnumResultCode type = var1[var3];
            if (type.getCode().equals(code)) {
                return type;
            }
        }

        return SYSTEM_ERROR;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public boolean equals(final String value){
        return code.equalsIgnoreCase(value);
    }
}
