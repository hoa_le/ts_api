package vn.hdbank.ts.application.resource;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OtpResponse extends BasicResponse{

    private String signature;

}