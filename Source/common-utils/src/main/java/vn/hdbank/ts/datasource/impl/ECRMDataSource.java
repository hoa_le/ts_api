package vn.hdbank.ts.datasource.impl;

import java.sql.Connection;

import oracle.ucp.UniversalConnectionPoolAdapter;
import oracle.ucp.UniversalConnectionPoolException;
import oracle.ucp.admin.UniversalConnectionPoolManager;
import oracle.ucp.admin.UniversalConnectionPoolManagerImpl;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;
import oracle.ucp.jdbc.ValidConnection;
import org.springframework.boot.context.event.ApplicationReadyEvent;

import vn.hdbank.ts.datasource.DataSource;
import vn.hdbank.ts.datasource.DataSourceProperties;
import vn.hdbank.ts.datasource.utils.SecurityUtils;
import vn.hdbank.ts.utils.WriteLog;

public class ECRMDataSource extends DataSource {
    private static UniversalConnectionPoolManager poolManger;
    private static PoolDataSource pds;
    private static String schema;
    private static ECRMDataSource ds;

    public static ECRMDataSource getInstance(DataSourceProperties dbConfig, ApplicationReadyEvent event) {
        if (ds == null) {
            ds = new ECRMDataSource(dbConfig, event);
        }
        return ds;
    }

    public static ECRMDataSource getInstance() {
        return ds;
    }

    private ECRMDataSource(DataSourceProperties dbConfig, ApplicationReadyEvent event) {
        try {
            init(dbConfig);
        } catch (Exception e) {
            WriteLog.error("There're many errors on initializing the EOC Datasource Configurations!", e);
            event.getApplicationContext().close();
            System.exit(-1);
        }
    }

    private void init(DataSourceProperties config) throws Exception {
        if (pds != null) {
            getManager().destroyConnectionPool(config.getConnectionName());
        }
        pds = createPool(config);
        schema = config.getDbSchema();
    }

    @Override
    public String getSchema() {
        return schema;
    }

    private static UniversalConnectionPoolManager getManager() throws UniversalConnectionPoolException {
        if (poolManger == null) {
            poolManger = UniversalConnectionPoolManagerImpl.getUniversalConnectionPoolManager();
        }
        return poolManger;
    }

    private static PoolDataSource createPool(DataSourceProperties databaseConfig) throws Exception {
        PoolDataSource dataSource = PoolDataSourceFactory.getPoolDataSource();
        String driver = databaseConfig.getDriverClass();
        String user = databaseConfig.getDbUserName();
        String pass = SecurityUtils.decryptPassword(databaseConfig.getDbPassword(), "Framework");
        String url = databaseConfig.getDbUrl();
        String connectionName = databaseConfig.getConnectionName() + "_" + Math.round(Math.random() * 1000);
        dataSource.setConnectionPoolName(connectionName);
        dataSource.setConnectionFactoryClassName(driver);
        dataSource.setURL(url);
        dataSource.setUser(user);
        dataSource.setPassword(pass);
        //set config connection pool
        dataSource.setInitialPoolSize(databaseConfig.getMinPoolSize());
        dataSource.setMinPoolSize(databaseConfig.getMinPoolSize());
        dataSource.setMaxPoolSize(databaseConfig.getMaxPoolSize());
        dataSource.setInactiveConnectionTimeout(databaseConfig.getInactiveConnectionTimeout());
        dataSource.setValidateConnectionOnBorrow(true);
        // create  && start connection
        getManager().createConnectionPool((UniversalConnectionPoolAdapter) dataSource);
        getManager().startConnectionPool(connectionName);
        return dataSource;
    }

    private static PoolDataSource getPoolDataSource() {
        return pds;
    }

    public Connection getConnection() throws Exception {
        Connection connection;
        do {
            connection = getPoolDataSource().getConnection();
        }
        while (connection == null || !((ValidConnection) connection).isValid());
        return connection;
    }

}
