package vn.hdbank.ts.common.model.createOTP;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Common {
    private String serviceVersion;
    private String messageId;
    private String transactionId;
    private String messageTimestamp;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private AdditionalInformation additionalInformation;

    public String getServiceVersion() { return serviceVersion; }
    public void setServiceVersion(String value) { this.serviceVersion = value; }

    public String getMessageId() { return messageId; }
    public void setMessageId(String value) { this.messageId = value; }

    public String getTransactionId() { return transactionId; }
    public void setTransactionId(String value) { this.transactionId = value; }

    public String getMessageTimestamp() { return messageTimestamp; }
    public void setMessageTimestamp(String value) { this.messageTimestamp = value; }

    public AdditionalInformation getAdditionalInformation() { return additionalInformation; }
    public void setAdditionalInformation(AdditionalInformation value) { this.additionalInformation = value; }
}
