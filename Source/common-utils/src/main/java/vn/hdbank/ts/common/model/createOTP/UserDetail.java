package vn.hdbank.ts.common.model.createOTP;

public class UserDetail {
    private String userID;
    private String userPassword;

    public String getUserID() { return userID; }

    public void setUserID(String value) { this.userID = value; }

    public String getUserPassword() { return userPassword; }

    public void setUserPassword(String value) { this.userPassword = value; }
}
