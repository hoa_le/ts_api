package vn.hdbank.ts.common.utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;

import com.fasterxml.jackson.databind.ObjectMapper;

import vn.hdbank.ts.common.CacheObject;
import vn.hdbank.ts.common.entity.generic.SessionObj;
import vn.hdbank.ts.common.security.EnumRequestType;
import vn.hdbank.ts.common.utils.security.CryptographyFactory;
import vn.hdbank.ts.common.utils.security.CryptographyInf;
import vn.hdbank.ts.common.utils.security.EnumCryptography;


public class Utils {
	
	private static final Logger logger = Logger.getLogger(Utils.class);
	
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public static boolean isEmpty(Object object) {
		return object == null ? true : (object instanceof String && ((String) object).isEmpty());
	}

	public static boolean isNumeric(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isDigit(c))
				return false;
		}
		return true;
	}
	
	public static String nullToEmpty(Object object) {
		return object == null ? "" : object.toString();
	}

	public static Number emptyToZero(Object object) {
		return isEmpty(object) ? 0 : new BigDecimal(object.toString());
	}

	public static String exceptionToString(Throwable ex) {
		StringWriter errors = new StringWriter();
		ex.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}

	public static String formatNumber(Object number) {
		if (isEmpty(number)) {
			number = 0;
		}
		DecimalFormat formatter = new DecimalFormat("#,###");
		return formatter.format(number).replaceAll(",", ".");

	}

	public static boolean isExistFromConfig(String strProperties, String element) {
		if (Utils.isEmpty(strProperties)) {
			return false;
		}
		String[] arr = strProperties.split(",");
		for (String arr1 : arr) {
			if (element.equalsIgnoreCase(arr1)) {
				return true;
			}
		}
		return false;
	}

	public static String replaceVnChars(String src) {
		if (Utils.isEmpty(src)) {
			return src;
		}
		String dest = Normalizer.normalize(src.trim(), Normalizer.Form.NFC);

		dest = dest.replaceAll("[áàãảạâấầẩẫậăắằẳẵặ]", "a").replaceAll("[óòỏõọôốồổỗộơớờởỡợ]", "o")
				.replaceAll("[íìĩỉị]", "i").replaceAll("[ýỳỷỹỵ]", "y").replaceAll("[éèẻẽẹêếềểễệ]", "e")
				.replaceAll("[úùủũụưứừửữự]", "u").replaceAll("[đ]", "d");
		dest = dest.replaceAll("[ÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬ]", "A").replaceAll("[ÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢ]", "O")
				.replaceAll("[ÍÌỈĨỊ]", "I").replaceAll("[ÝỲỶỸỴ]", "Y").replaceAll("[ÉÈẺẼẸÊẾỀỂỄỆ]", "E")
				.replaceAll("[ÚÙỦŨỤƯỨỪỬỮỰ]", "U").replaceAll("[Đ]", "D");
		return dest;
	}

	public static boolean isValidEmail(String email) {
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		return pattern.matcher(email).matches();
	}

	public static <E> E choice(Collection<? extends E> coll) {
		if (coll.isEmpty()) {
			return null; // or throw IAE, if you prefer
		}
		return choice(coll, coll.size());
	}

	public static <E> E choice(Collection<? extends E> coll, int size) {
		Random rand = new Random();
		int index = rand.nextInt(size);
		try {
			if (coll instanceof List) { // optimization
				return ((List<? extends E>) coll).get(index);
			} else {
				Iterator<? extends E> iter = coll.iterator();
				for (int i = 0; i < index; i++) {
					iter.next();
				}
				return iter.next();
			}
		} catch (Exception ex) {
			return null;
		}
	}

	public static String subString(String message, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(message);
		if (matcher.find()) {
			return matcher.group(0);
		}
		return "";
	}

	public static <T> T fromString(Class<T> type, String jSon) throws IOException {
		return new ObjectMapper().readValue(jSon, type);
	}

	@SuppressWarnings("rawtypes")
	public static String toXml(Object source, Class... type) throws JAXBException {
		StringWriter sw = new StringWriter();
		JAXBContext carContext = JAXBContext.newInstance(type);
		Marshaller carMarshaller = carContext.createMarshaller();
		carMarshaller.marshal(source, sw);
		return sw.toString();
	}

	@SuppressWarnings("unchecked")
	public static <T extends Object> T toObject(String xml, Class<T> type) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(type);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		return (T) unmarshaller.unmarshal(reader);

	}

	public static String formatDate(Date date, String format) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.format(date);

	}

	public static String encrypt3DES(String plaintext, byte[] sharedkey, byte[] sharedvector) throws Exception {
		Cipher c = Cipher.getInstance("DESede/CBC/PKCS5Padding");
		c.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(sharedkey, "DESede"), new IvParameterSpec(sharedvector));
		byte[] encrypted = c.doFinal(plaintext.getBytes("UTF-8"));
		// return Base64.getEncoder().encodeToString(encrypted);
		return DatatypeConverter.printBase64Binary(encrypted);

		// return Base64Coder.encodeLines(encrypted);
	}

	public static String decrypt3DES(String ciphertext, byte[] sharedkey, byte[] sharedvector) throws Exception {
		ciphertext = ciphertext.replace('@', '+');
		Cipher c = Cipher.getInstance("DESede/CBC/PKCS5Padding");
		c.init(Cipher.DECRYPT_MODE, new SecretKeySpec(sharedkey, "DESede"), new IvParameterSpec(sharedvector));

		byte[] parseBase64Binary = DatatypeConverter.parseBase64Binary(ciphertext);

		byte[] decrypted = c.doFinal(parseBase64Binary);
		return new String(decrypted, "UTF-8");
	}

	public static String sha256(String data) {

		StringBuffer sb = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(data.getBytes());
			byte byteData[] = md.digest();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public static String encryptPrivateRSA(String data, String privateKeyPath) throws Exception {

		byte[] dataBytes = data.getBytes("UTF-8");

		File privateKeyFile = new File(privateKeyPath); // private key file in
														// PEM format
		PEMParser pemParser = new PEMParser(new FileReader(privateKeyFile));

		PEMKeyPair keyPair = (PEMKeyPair) pemParser.readObject();
		AsymmetricKeyParameter privateParam = PrivateKeyFactory.createKey(keyPair.getPrivateKeyInfo());

		AsymmetricBlockCipher e = new RSAEngine();
		e = new org.bouncycastle.crypto.encodings.PKCS1Encoding(e);
		e.init(true, privateParam);
		byte[] hexEncodedCipher = e.processBlock(dataBytes, 0, dataBytes.length);
		pemParser.close();
		return new String(Base64.encodeBase64(hexEncodedCipher));

	}

	public static boolean verifyWithRSA(String data, String sign, String publicKey) throws Exception {

		byte[] publicBytes = Base64.decodeBase64(publicKey);
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");

		PublicKey kp = keyFactory.generatePublic(keySpec);

		byte[] dataBytes = data.getBytes("UTF-8");
		byte[] signByte = sign.getBytes("UTF-8");

		Signature signature = Signature.getInstance("SHA256withRSA");
		signature.initVerify(kp);
		signature.update(dataBytes);
		return signature.verify(Base64.decodeBase64(signByte));

	}

	public static String signWithRSA(String data, String privateKey) throws Exception {

		byte[] privateKeyByte = Base64.decodeBase64(privateKey);
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyByte);

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");

		PrivateKey kp = keyFactory.generatePrivate(keySpec);

		byte[] dataBytes = data.getBytes("UTF-8");

		Signature signature = Signature.getInstance("SHA256withRSA");
		signature.initSign(kp);
		signature.update(dataBytes);
		return Base64.encodeBase64String(signature.sign());

	}
	
	public static String removeWhiteSpace(String str) {
		String temp = "";
		for (int i = 0; i < str.length(); ++i) {
			if ((str.charAt(i) != ' ') && (str.charAt(i) != '\n') && (str.charAt(i) != '\t')) {
				temp = temp + str.charAt(i);
			}
		}
		return temp;
	}
	public static byte[] hex2data(String str) {
		if (str == null) {
			return new byte[0];
		}
		int len = str.length();
		char[] hex = str.toCharArray();
		byte[] buf = new byte[len / 2];

		for (int pos = 0; pos < len / 2; ++pos) {
			buf[pos] = (byte) (toDataNibble(hex[(2 * pos)]) << 4 & 0xF0 | toDataNibble(hex[(2 * pos + 1)]) & 0xF);
		}
		return buf;
	}

	public static byte toDataNibble(char c) {
		if (('0' <= c) && (c <= '9')) {
			return (byte) ((byte) c - 48);
		}
		if (('a' <= c) && (c <= 'f')) {
			return (byte) ((byte) c - 97 + 10);
		}
		if (('A' <= c) && (c <= 'F')) {
			return (byte) ((byte) c - 65 + 10);
		}
		return -1;
	}
	
	public static String data2hex(byte[] data) {
		if (data == null) {
			return null;
		}
		int len = data.length;
		StringBuffer buf = new StringBuffer(len * 2);
		for (int pos = 0; pos < len; ++pos) {
			buf.append(toHexChar(data[pos] >>> 4 & 0xF)).append(toHexChar(data[pos] & 0xF));
		}
		return buf.toString();
	}

	private static char toHexChar(int i) {
		if ((0 <= i) && (i <= 9)) {
			return (char) (48 + i);
		}
		return (char) (97 + i - 10);
	}
	
	public static String encryptUTF8(String plainText, String key) throws Exception {
		int i = (key.hashCode() < 0) ? key.hashCode() * -1 : key.hashCode();
		byte[] b = plainText.getBytes("UTF8");

		for (int j = 0; j < b.length; ++j) {
			b[j] = (byte) (b[j] ^ i);
		}
		return data2hex(b);
	}

	public static String decryptUTF8(String cipherText, String key) throws Exception {
		int i = (key.hashCode() < 0) ? key.hashCode() * -1 : key.hashCode();
		byte[] b = hex2data(cipherText);

		for (int j = 0; j < b.length; ++j) {
			b[j] = (byte) (b[j] ^ i);
		}

		return new String(b, "UTF8");
	}

	public static String decryptData(String requestData, EnumRequestType requestType, String token) {

		try {
			if (requestType == EnumRequestType.AsymmetricCryptography) {
				CryptographyInf asymmetricCryptography = new CryptographyFactory.Builder()
						.EnumCryptography(EnumCryptography.Rsa)
						.PrivateKey(CacheObject.getInstance().getServerPrivateKey()).Build();
				String Data = asymmetricCryptography.decrypt(requestData);
				return Data;
			} else {
				SessionObj obj = CacheObject.getInstance().getSessionList().get(token.split("#")[1]);
				CryptographyInf symmetricCryptography = new CryptographyFactory.Builder()
						.EnumCryptography(EnumCryptography.TripleDes).PrivateKey(obj.getShareKey()).Build();
				String Data = symmetricCryptography.decrypt(requestData);
				return Data;
			}
		} catch (Exception ex) {
			logger.error("[Utils][decryptData] Exception : " + ex.getMessage(), ex);
			return null;
		}

	}
	
	public static String gen24BytesTrippleDesKey() {
		String easy = "0123456789ACEFGHJKLMNPQRUVWXYabcdefhijkprstuvwx";
		RandomString DESKey = new RandomString(24, new SecureRandom(), easy);
		return DESKey.nextString();
	}
 

	public static String toJsonString(Object obj) throws Exception {
		JsonConverter<Object> converter = new JsonConverter<Object>();
		return converter.ConvertFromObject(obj);
	}
	
	private static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('A' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	public static String MD5(String text) throws NoSuchAlgorithmException,
			UnsupportedEncodingException {
		MessageDigest md;
		md = MessageDigest.getInstance("MD5");
		byte[] md5hash = new byte[32];
		md.update(text.getBytes("UTF-8"), 0, text.length());
		md5hash = md.digest();
		return convertToHex(md5hash);
	}
}
