package vn.hdbank.ts.domain.service.impl;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vn.hdbank.ts.utils.URLUtilsType;
import vn.hdbank.ts.utils.WriteLog;
import vn.hdbank.ts.application.configuration.ApplicationConfig;
import vn.hdbank.ts.application.resource.*;
import vn.hdbank.ts.application.utils.ResultCode;
import vn.hdbank.ts.common.EnumResultCode;
import vn.hdbank.ts.common.model.createOTP.RequestOtpObject;
import vn.hdbank.ts.domain.object.handle.Constant;
import vn.hdbank.ts.domain.service.ESBOtpService;
import vn.hdbank.ts.domain.service.OtpService;
import vn.hdbank.ts.domain.service.security.SignatureAgent;
import vn.hdbank.ts.infrastructure.database.LeadDao;

@Service
public class OtpServiceImpl implements OtpService {
	private static final String className = OtpService.class.getSimpleName();
	private Gson gson = new Gson();

	@Autowired
	private ApplicationConfig applicationConfig;

	@Autowired
	LeadDao leadDao;

	@Autowired
	SignatureAgent signatureAgent;

	@Value("${hdbank.ts.partner-id}")
	private String tsPartner;
	
	@Value("${hdbank.ts.smsContent}")
	private String smsContent;

	@Override
	public BasicResponse requestOtp(OtpRequest request) {
		OtpResponse response = new OtpResponse();
		try {
			
			String mobile = request.getMobile();
			String signature = request.getSignature();
			String requestId = request.getRequestId();
			String partnerId = request.getPartnerId();
			String requestTime = request.getRequestTime();
			
			if (requestId.equals("") || requestTime.equals("") || signature.equals("")
					|| mobile.equals("") || partnerId.equals("")) 
			{
				response.setCode(ResultCode.TS_ERROR.getCode());
				response.setMessage("Lack of required infor");
				WriteLog.write(request.getRequestId(), className, "requestOTP", "Lack of required infor");
				
			} else {
				
				String sigReqHashed = signatureAgent.getSignatureMd5(request, OtpRequest.class,
						URLUtilsType.REQUEST_OTP, tsPartner, request.getRequestId() + " request");
				if (!sigReqHashed.equalsIgnoreCase(signature)) {
        			response.setCode(ResultCode.SIGNATURE_NOT_MATCH.getCode());
        			response.setMessage(ResultCode.SIGNATURE_NOT_MATCH.getMessage());
        			
        		} else {
        			
					String insertRes = leadDao.insertRequest(request.getRequestId());
					if (insertRes.equals(Constant.SUCCESS)) {
					
						ESBOtpService service = new ESBOtpService(applicationConfig);
						RequestOtpObject esbResponse = service.requestOtpESB(requestId, mobile, smsContent, "N", "");
						response.setCode(esbResponse.getEnumResultCode().getCode());
						response.setMessage(esbResponse.getEnumResultCode().getMessage());
					
						leadDao.deleteRequest(request.getRequestId());
						String sigResHashed = signatureAgent.getSignatureMd5(response, OtpResponse.class,
								URLUtilsType.REQUEST_OTP, tsPartner, request.getRequestId() + " response");
						response.setSignature(sigResHashed);
					
					} else {
						response.setCode(ResultCode.REQUEST_ID_EXIST.getCode());
						response.setMessage(ResultCode.REQUEST_ID_EXIST.getMessage());
					}
        		}
			}

		} catch (Exception ex) {
			response.setCode(ResultCode.TS_ERROR.getCode());
			response.setMessage(ResultCode.TS_ERROR.getMessage());
			WriteLog.error(request.getRequestId(), className, "requestOtp", gson.toJson(request), ex);
		}
		
		return response;
	}

	@Override
	public BasicResponse verifyOtp(VerifyOtpRequest request) {
		OtpResponse response = new OtpResponse();
		try {
			
			String otp = request.getOtp();
			String mobile = request.getMobile();
			String requestId = request.getRequestId();
			String partnerId = request.getPartnerId();
			String signature = request.getSignature();
			String requestTime = request.getRequestTime();	
			
			if (requestId.equals("") || partnerId.equals("") || requestTime.equals("") 
					|| otp.equals("") || mobile.equals("") || signature.equals("")) 
			{
				response.setCode(ResultCode.TS_ERROR.getCode());
				response.setMessage("Lack of required infor");
				WriteLog.write(request.getRequestId(), className, "verifyOTP", "Lack of required infor");
				
			} else {
				
        		String sigReqHashed = signatureAgent.getSignatureMd5(request, VerifyOtpRequest.class, 
        				URLUtilsType.VERIFY_OTP, tsPartner, request.getRequestId() + " request");
        		if (!sigReqHashed.equalsIgnoreCase(signature)) {
        			response.setCode(ResultCode.SIGNATURE_NOT_MATCH.getCode());
        			response.setMessage(ResultCode.SIGNATURE_NOT_MATCH.getMessage());
        			
        		} else {
					String insertRes = leadDao.insertRequest(request.getRequestId());
					if (insertRes.equals(Constant.SUCCESS)) {
					
						ESBOtpService service = new ESBOtpService(applicationConfig);
						EnumResultCode esbResponse = service.verifyOtpESB(requestId, mobile, otp, smsContent, "N");
						response.setCode(esbResponse.getCode());
						response.setMessage(esbResponse.getMessage());
					
						leadDao.deleteRequest(request.getRequestId());
						String sigResHashed = signatureAgent.getSignatureMd5(response, OtpResponse.class,
								URLUtilsType.VERIFY_OTP, tsPartner, request.getRequestId() + " response");
						response.setSignature(sigResHashed);
					
					} else {
						response.setCode(ResultCode.REQUEST_ID_EXIST.getCode());
						response.setMessage(ResultCode.REQUEST_ID_EXIST.getMessage());
					}
        		}
			}

		} catch (Exception ex) {
			response.setCode(ResultCode.TS_ERROR.getCode());
			response.setMessage(ResultCode.TS_ERROR.getMessage());
			WriteLog.error(request.getRequestId(), className, "verifyOtp", gson.toJson(request), ex);
		}
		
		return response;
	}
}
