package vn.hdbank.ts.common.model.createOTP;


import vn.hdbank.ts.common.EnumResultCode;

public class RequestOtpObject {
    private EnumResultCode enumResultCode;

    private String mediaType;

    private String challengeCode;


    public EnumResultCode getEnumResultCode() {
        return enumResultCode;
    }

    public void setEnumResultCode(EnumResultCode enumResultCode) {
        this.enumResultCode = enumResultCode;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getChallengeCode() {
        return challengeCode;
    }

    public void setChallengeCode(String challengeCode) {
        this.challengeCode = challengeCode;
    }
}
