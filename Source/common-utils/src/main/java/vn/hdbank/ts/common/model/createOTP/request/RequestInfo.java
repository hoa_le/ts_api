package vn.hdbank.ts.common.model.createOTP.request;

public class RequestInfo {

    private CreateOTPReq createOTPReq;

    public RequestInfo(CreateOTPReq createOTPReq) {
        this.createOTPReq = createOTPReq;
    }

    public CreateOTPReq getCreateOTPReq() {
        return createOTPReq;
    }

    public void setCreateOTPReq(CreateOTPReq createOTPReq) {
        this.createOTPReq = createOTPReq;
    }
}
