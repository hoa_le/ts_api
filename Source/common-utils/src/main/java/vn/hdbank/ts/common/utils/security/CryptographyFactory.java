package vn.hdbank.ts.common.utils.security;

import vn.hdbank.ts.common.utils.security.impl.DefaultAlgorithm;
import vn.hdbank.ts.common.utils.security.impl.RsaAlgorithm;
import vn.hdbank.ts.common.utils.security.impl.TripleDes;

public class CryptographyFactory implements CryptographyInf{
	
	private AbstractCryptography algorithms;
	// Builder pattern
    private CryptographyFactory(Builder builder) {
        if (builder.algorithmsType == EnumCryptography.Rsa) {
            algorithms = new RsaAlgorithm(builder.publicKey, builder.privateKey);
        } else if (builder.algorithmsType == EnumCryptography.TripleDes) {
            algorithms = new TripleDes(builder.publicKey, builder.privateKey);
        } else {
            algorithms = new DefaultAlgorithm(builder.publicKey, builder.privateKey);
        }
    }

    @Override
    public String encrypt(String encryptData) throws Exception {
        return algorithms.encrypt(encryptData);
    }

    @Override
    public String decrypt(String decryptData) throws Exception {
        return algorithms.decrypt(decryptData);
    }

    @Override
    public String createSignature(String encryptData) throws Exception {
        return algorithms.createSignature(encryptData);
    }

    @Override
    public boolean verifySignature(String decryptData, String sign) throws Exception {
        return algorithms.verifySignature(decryptData, sign);
    }

	public static class Builder {
        EnumCryptography algorithmsType;
        String publicKey = "Default public key"; 
        String privateKey = "Default private key";

        public Builder EnumCryptography(EnumCryptography algorithmsType) {
            this.algorithmsType = algorithmsType;
            return this;
        }

        public Builder PublicKey(String publicKey) {
            this.publicKey = publicKey;
            return this;
        }

        public Builder PrivateKey(String privateKey) {
            this.privateKey = privateKey;
            return this;
        }

        public CryptographyFactory Build() {
            return new CryptographyFactory(this);
        }
    }
}
