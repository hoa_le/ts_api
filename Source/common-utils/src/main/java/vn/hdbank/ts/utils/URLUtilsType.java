package vn.hdbank.ts.utils;

import java.io.Serializable;

public class URLUtilsType implements Serializable {

    private static final long serialVersionUID = 2069937152339670240L;

    public static final String ELIGIBLE = "/eligible";
    public static final String LEAD = "/lead";
    public static final String REQUEST_OTP = "/otp/request";
    public static final String VERIFY_OTP = "/otp/verify";
    public static final String STATUS = "/status";

}
