package vn.hdbank.ts.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public final class JsonConverter<T> {
	private static final Logger logger = LoggerFactory.getLogger(JsonConverter.class);
 
	
	public String ConvertFromObject(T data) {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.writeValueAsString(data);
		} catch (JsonProcessingException e) {
			logger.error("[JsonConverter][ConvertFromObject] Exception : " + e.getMessage(), e);
		}

		return null;
	}
	
	public T ConvertFromData(String data,Class<T> valueType) {
		try {
			ObjectMapper objectMapper = new ObjectMapper(); 
			return objectMapper.readValue(data,valueType);
		} catch (Exception e) {
			logger.error("[JsonConverter][ConvertFromData] Exception : " + e.getMessage(), e);
		}

		return null;
	}
}
