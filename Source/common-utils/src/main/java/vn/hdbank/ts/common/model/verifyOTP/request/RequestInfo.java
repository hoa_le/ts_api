package vn.hdbank.ts.common.model.verifyOTP.request;

public class RequestInfo {

    private VerifyOTPReq verifyOTPReq;

    public RequestInfo(VerifyOTPReq verifyOTPReq) {
        this.verifyOTPReq = verifyOTPReq;
    }

    public VerifyOTPReq getVerifyOTPReq() { return verifyOTPReq; }
    public void setVerifyOTPReq(VerifyOTPReq value) { this.verifyOTPReq = value; }
}
