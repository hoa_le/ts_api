package vn.hdbank.ts.domain.service;

import vn.hdbank.ts.application.resource.*;

public interface OtpService {

	BasicResponse requestOtp(OtpRequest otpRequest);
	BasicResponse verifyOtp(VerifyOtpRequest request);
}

