package vn.hdbank.ts.application.utils;

/**
 * @author ThuongDc
 * @since 9/23/2020
 */

public enum ResultCode {
    SUCCESS("00", "Success"),
    TS_ERROR("06", "SYSTEM ERROR"),
    OTP_INVALID("07","OTP INVALID"),
    INVALID_FILE("08","FILE INVALID"),
	REQUEST_ID_EXIST("1006", "Request is exist"),
	ID_NOT_FOUND("1007", "ID_NOT_FOUND"),
	LEAD_EXIST("1009","Lead Duplicated"),
    SIGNATURE_NOT_MATCH("1030","Signature not match"),
	CAMPAIGN_ID_EXIST("1031","Campaign ID not exist");
	
    private String code;
    private String message;

    ResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ResultCode findByCode(String code) {
        ResultCode[] var1 = values();
        int len = var1.length;

        for (int var3 = 0; var3 < len; ++var3) {
            ResultCode type = var1[var3];
            if (type.getCode().equals(code)) {
                return type;
            }
        }

        return TS_ERROR;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }


}