package vn.hdbank.ts.application.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import vn.hdbank.ts.datasource.impl.ECRMDataSource;
import vn.hdbank.ts.utils.WriteLog;
import vn.hdbank.ts.application.configuration.db.ECRMDataSourceProperties;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    ECRMDataSourceProperties ecrmDataSourceProperties;


    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        //Innit database
        ECRMDataSource.getInstance(ecrmDataSourceProperties, event);

        //Constant.init();
        WriteLog.write("INFO", "########## SERVICES READY ##########");
    }

}
