package vn.hdbank.ts.infrastructure.database.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;

import vn.hdbank.ts.application.resource.File;
import vn.hdbank.ts.application.resource.EligibleRequest;
import vn.hdbank.ts.application.resource.EligibleResponse;
import vn.hdbank.ts.application.utils.ResultCode;
import vn.hdbank.ts.application.utils.Utils;
import vn.hdbank.ts.datasource.DataSourceFactory;
import vn.hdbank.ts.datasource.EnumDataSource;
import vn.hdbank.ts.utils.WriteLog;
import vn.hdbank.ts.infrastructure.database.EligibleDao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class EligibleDaoImpl implements EligibleDao {
	private static final String className = EligibleDao.class.getSimpleName();

	@Value("${hdbank.ts.store.check.eligible}")
	private String checkEligible;
	@Value("${hdbank.ts.partner-id}")
	private String partnerId;
	@Value("${hdbank.ts.store.timeout}")
	private int timeOut;
	
	private Gson gson = new Gson();

	@Override
	public EligibleResponse checkEligible(EligibleRequest request) {
		EligibleResponse response = new EligibleResponse();
		
		ResultSet rs_detail = null;
		
		try (Connection CSBConnection = DataSourceFactory.getInstance().getDataSource(EnumDataSource.ecrm)
				.getConnection();
		CallableStatement callStmt = CSBConnection.prepareCall("{call " + checkEligible + "(?, ?, ? ,?, ? ,? , ?, ?)} ")){
			
			callStmt.setString(1, request.getLeadId());
			if (request.getFullName() != null)
				callStmt.setString(2, Utils.removeAccent(request.getFullName().trim()).toUpperCase());
			else
				callStmt.setString(2, (request.getFullName()));
			callStmt.setString(3, request.getPhone());
			callStmt.setString(4, request.getNationalId());
			callStmt.registerOutParameter(5, 12);
			callStmt.registerOutParameter(6, -10);
			callStmt.registerOutParameter(7, 12);
			callStmt.setString(8, request.getCampaignId());
			callStmt.setQueryTimeout(timeOut);
			callStmt.execute();
			
			
			response.setResultType(callStmt.getString(7));
			
			switch (callStmt.getString(5)) {
			case "000000":
				response.setCode(ResultCode.SUCCESS.getCode());
				response.setMessage(ResultCode.SUCCESS.getMessage());
				rs_detail = (ResultSet) callStmt.getObject(6);
				while (rs_detail != null && rs_detail.next()) {
					response.setPartnerId(partnerId.toUpperCase());
					response.setRequestTime(request.getRequestTime());
					response.setLeadId(rs_detail.getString("LEAD_ID"));
					response.setCampaignId(rs_detail.getString("CAMPAIGN_ID"));
					response.setLeadStatusId(rs_detail.getString("STATUS_ID"));
					response.setFullname(rs_detail.getString("FULLNAME"));
					response.setDob(rs_detail.getString("DOB"));
					response.setNationalId(rs_detail.getString("NATIONALID"));
					response.setIssuedDate(rs_detail.getString("ISSUED_DATE"));
					response.setIssuedBy(rs_detail.getString("ISSUED_BY"));
					response.setGender(rs_detail.getString("GENDER"));
					response.setPhone(rs_detail.getString("MOBILEPHONE1"));
					response.setEmail(rs_detail.getString("EMAIL"));
					response.setAcademicLevel(rs_detail.getString("ACADEMIC_LEVEL"));
					response.setMartialStatus(rs_detail.getString("MARTIAL_STATUS"));
					response.setSecurityAnswer(rs_detail.getString("SECURITY_ANSWER"));
					response.setAddressPermanent(rs_detail.getString("ADDRESS_PERMANENT"));
					response.setProvincePermanent(rs_detail.getString("PROVINCE_PERMANENT"));
					response.setDistrictPermanent(rs_detail.getString("DISTRICT_PERMANENT"));
					response.setWardPermanent(rs_detail.getString("WARD_PERMANENT"));
					response.setAddressCurrent(rs_detail.getString("ADDRESS_CURRENT"));
					response.setProvinceTemporary(rs_detail.getString("PROVINCE_TEMPORARY"));
					response.setDistrictTemporary(rs_detail.getString("DISTRICT_TEMPORARY"));
					response.setWardTemporary(rs_detail.getString("WARD_TEMPORARY"));
					response.setPosition(rs_detail.getString("POSITION"));
					response.setCompany(rs_detail.getString("COMPANY"));
					response.setWorkingStatus(rs_detail.getString("WORKING_STATUS"));
					response.setBusinessGroup(rs_detail.getString("BUSINESS_GROUP"));
					response.setBusinessType(rs_detail.getString("BUSINESS_TYPE"));
					response.setWorkingProvince(rs_detail.getString("WORKING_PROVINCE"));
					response.setWorkingDistrict(rs_detail.getString("WORKING_DISTRICT"));
					response.setWorkingWard(rs_detail.getString("WORKING_WARD"));
					response.setWorkingAddress(rs_detail.getString("WORKING_STREET"));
					response.setWorkingPhone(rs_detail.getString("WORKING_PHONE_NUMBER"));
					response.setReceivingSalaryType(rs_detail.getString("RECEIVING_SALARY_TYPE"));
					response.setIncome(rs_detail.getLong("INCOME"));
					response.setCardReceivingAddress(rs_detail.getString("CARD_RECEIVINGADDRESS"));
					response.setBranchCode(rs_detail.getString("BRANCH_CODE"));
					response.setStaffCode(rs_detail.getString("STAFF_CODE"));
					response.setRecommendedCreditLimit(rs_detail.getLong("RECOMMENDED_CREDIT_LIMIT"));
					response.setClientTier(rs_detail.getString("CLIENT_TIER"));
					response.setNeedEkyc(rs_detail.getString("NEED_EKYC"));
					response.setNeedeSign(rs_detail.getString("NEED_ESIGN"));
					response.setContractProductCode(rs_detail.getString("CONTRACTPRODUCTCODE"));
					response.setCreatedDate(rs_detail.getString("CREATED_DATE"));
					response.setResidenceDurationM(rs_detail.getInt("RESIDENCE_DURATION_M"));
					response.setResidenceDurationY(rs_detail.getInt("RESIDENCE_DURATION"));
					response.setNumberOfDependents(rs_detail.getString("NUMBER_OF_DEPENDENTS"));
					response.setRef1RelWithCardholder(rs_detail.getString("REF1_REL_WITH_CARDHOLDER"));
					response.setRef1Fullname(rs_detail.getString("REF1_FULLNAME"));
					response.setRef1MobilePhone(rs_detail.getString("REF1_PHONE"));
					response.setRef2RelWithCardholder(rs_detail.getString("REF2_REL_WITH_CARDHOLDER"));
					response.setRef2Fullname(rs_detail.getString("REF2_FULLNAME"));
					response.setRef2MobilePhone(rs_detail.getString("REF2_PHONE"));
					response.setWorkingPosition(rs_detail.getString("WORKING_POSITION"));
					String documentString = rs_detail.getString("DOCUMENT_FILE");
					File[] documentArr = gson.fromJson(documentString, File[].class);
					response.setDocumentFile(documentArr);
				}
				break;
			case "1007":
				response.setCode(ResultCode.ID_NOT_FOUND.getCode());
				response.setMessage(ResultCode.ID_NOT_FOUND.getMessage());
				break;
			default:
				response.setCode(ResultCode.TS_ERROR.getCode());
				response.setMessage(callStmt.getString(5));
				WriteLog.write(request.getRequestId(), className, "checkEligible", callStmt.getString(5));
			}
			
		} catch (Exception e) {
			WriteLog.error(request.getRequestId(), className, "checkEligible", e);
			response.setCode(ResultCode.TS_ERROR.getCode());
			response.setMessage(ResultCode.TS_ERROR.getMessage());
		} finally {
			if (rs_detail != null) {
				try {
					rs_detail.close();
				} catch (SQLException e) {
					WriteLog.error(request.getRequestId(), className, "checkEligible", gson.toJson(e.getMessage()), e);
				}
			}
		}
		
		return response;
	}
}
