package vn.hdbank.ts.domain.service;

import vn.hdbank.ts.application.resource.*;

public interface EligibleService {

	EligibleResponse checkEligible(EligibleRequest eligibleRequest);
}

