package vn.hdbank.ts.application.configuration;

import lombok.Getter;
import lombok.Setter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

@EnableConfigurationProperties
@Configuration
@ConfigurationProperties(prefix = "hdbank")
@Getter
public class ApplicationConfig {

    public static final String AUTHORIZATION_HEADER = "Authorization";

    private Signature signature = new Signature();
    private Security security = new Security();
    private final CorsConfiguration cors = new CorsConfiguration();

    @Getter
    public static class Signature {

        private final Internal internal = new Internal();

        @Getter @Setter
        public class Internal {

            private String HDBRSAPrivateKey;
            private String HDBRSAPublicKey;
        }


        
        private final Ts ts = new Ts();
        
        @Getter @Setter
        public class Ts {

            private String partnerId; 

            private String partnerPassword;

            private String partnerName;

            private String rsaPublicKey;

            private String aesSecretKey;

            private String aesSalt;

            private String authPartnerUsername;

            private String authPartnerPassword;

            private String whitelistIp;

            private String desHexKey;

            private String desHexIv;

            private String otpGwIp;

            private String otpGwPort;

            private String esbUser;

            private String esbPass;
        }

    }

    @Getter
    public static class Security {

        private final Authentication authentication = new Authentication();
        private final RememberMe rememberMe = new RememberMe();

        @Getter
        public static class Authentication {

            private final Jwt jwt = new Jwt();
            private final Ldap ldap = new Ldap();
            private final Azure azure = new Azure();
            private final GSuite gsuite = new GSuite();

            @Getter @Setter
            public static class Jwt {

                private String secret;
                private String base64Secret;
                private long tokenValidityInSeconds;
                private long tokenValidityInSecondsForRememberMe;
            }

            @Getter @Setter
            public static class Ldap {

                private String domain;
                private String host;
                private String searchBase;
            }

            @Getter @Setter
            public static class Azure {
                private String url;
            }

            @Getter @Setter
            public static class GSuite {
                private String url;
            }
        }

        @Getter
        public static class RememberMe {
            private String key;
        }
    }

}
