package vn.hdbank.ts.application.configuration.keyManage;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartnerKey {
    private String partnerID;
    private String partnerPassword;
    private String partnerName;
    private String aesSecretKey;
    private String aesSalt;
    private String rsaPublicKey;
    private String rsaPrivateKey;
    private String ipWhiteList;
    private String authPartnerUsername;
    private String authPartnerPassword;
    private boolean isEncrypt;
    private String desHexKey;
    private String desHexIv;
    private String otpGwIp;
    private String otpGwPort;
    private String esbUser;
    private String esbPass;
}
