package vn.hdbank.ts.application.resource;

import lombok.Getter;
import lombok.Setter;
import vn.hdbank.ts.application.utils.Utils;


@Getter
@Setter
public class BasicResponse {

    private String responseTime = Utils.getDateTimeDb();

    private String code;

    private String message;

}