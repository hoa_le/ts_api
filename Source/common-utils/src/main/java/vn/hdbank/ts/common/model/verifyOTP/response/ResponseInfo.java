package vn.hdbank.ts.common.model.verifyOTP.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseInfo {

    private VerifyOTPRes verifyOTPRes;

    public VerifyOTPRes getVerifyOTPRes() { return verifyOTPRes; }
    public void setVerifyOTPRes(VerifyOTPRes value) { this.verifyOTPRes = value; }
}
