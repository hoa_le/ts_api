CREATE TABLE "MBLP"."LM_REQUEST" 
   (	"REQUEST_ID" VARCHAR2(100 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATAMBLP"   NO INMEMORY ;
--------------------------------------------------------
--  DDL for Index LM_REQUEST_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MBLP"."LM_REQUEST_PK" ON "MBLP"."LM_REQUEST" ("REQUEST_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATAMBLP" ;
--------------------------------------------------------
--  Constraints for Table LM_REQUEST
--------------------------------------------------------

  ALTER TABLE "MBLP"."LM_REQUEST" MODIFY ("REQUEST_ID" NOT NULL ENABLE);
  ALTER TABLE "MBLP"."LM_REQUEST" ADD CONSTRAINT "LM_REQUEST_PK" PRIMARY KEY ("REQUEST_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATAMBLP"  ENABLE;
  
  
--------------------------------------------------------
--  DDL for Table LM_ATTACHMENT
--------------------------------------------------------

  CREATE TABLE "MBLP"."LM_ATTACHMENT" 
   (	"LEAD_ID" VARCHAR2(20 BYTE), 
	"FILE_ID" VARCHAR2(100 BYTE), 
	"TYPE" VARCHAR2(100 BYTE), 
	"DIRECTORY" VARCHAR2(3000 BYTE), 
	"FILENAME" VARCHAR2(100 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATAMBLP"   NO INMEMORY ;
--------------------------------------------------------
--  DDL for Index LM_ATTACHMENT_INDEX
--------------------------------------------------------

  CREATE INDEX "MBLP"."LM_ATTACHMENT_INDEX" ON "MBLP"."LM_ATTACHMENT" ("LEAD_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATAMBLP" ;
--------------------------------------------------------
--  Constraints for Table LM_ATTACHMENT
--------------------------------------------------------

  ALTER TABLE "MBLP"."LM_ATTACHMENT" MODIFY ("LEAD_ID" NOT NULL ENABLE);
  ALTER TABLE "MBLP"."LM_ATTACHMENT" MODIFY ("FILE_ID" NOT NULL ENABLE);

