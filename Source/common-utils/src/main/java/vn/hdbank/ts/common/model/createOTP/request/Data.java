package vn.hdbank.ts.common.model.createOTP.request;

public class Data {

    private String userId;

    private String serialNo;

    private String transDetail;

    private String language;

    private String partner;

    private String isReqChalCode;

    private String clientImei;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getTransDetail() {
        return transDetail;
    }

    public void setTransDetail(String transDetail) {
        this.transDetail = transDetail;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getIsReqChalCode() {
        return isReqChalCode;
    }

    public void setIsReqChalCode(String isReqChalCode) {
        this.isReqChalCode = isReqChalCode;
    }

    public String getClientImei() {
        return clientImei;
    }

    public void setClientImei(String clientImei) {
        this.clientImei = clientImei;
    }
}
