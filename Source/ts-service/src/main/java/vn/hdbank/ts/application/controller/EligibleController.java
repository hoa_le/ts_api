package vn.hdbank.ts.application.controller;

import com.google.gson.Gson;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vn.hdbank.ts.utils.URLUtilsType;
import vn.hdbank.ts.utils.WriteLog;
import vn.hdbank.ts.application.resource.EligibleRequest;
import vn.hdbank.ts.application.resource.EligibleResponse;
import vn.hdbank.ts.application.utils.Utils;
import vn.hdbank.ts.domain.service.EligibleService;

@RestController
@RequestMapping("/api/hdb-ts-card/")
public class EligibleController {
    private static final String className = EligibleController.class.getSimpleName();
    private Gson gson = new Gson();

    @Autowired
    private EligibleService eligibleService;

    @RequestMapping(value = URLUtilsType.ELIGIBLE, method = RequestMethod.POST)
    public ResponseEntity<?> LM_CheckEligible(@RequestBody EligibleRequest request) {
        ResponseEntity<?> responseEntity = null;
        Date date = new Date();
        long timeMilli = date.getTime();
        
        EligibleResponse response = new EligibleResponse();
        try {
        	WriteLog.write(request.getRequestId(), className, "checkEligible", String.format("Request: %s", gson.toJson(request)));
            response = eligibleService.checkEligible(request);
            
            responseEntity = Utils.getResponseEntity(request, response, response.getCode(), URLUtilsType.ELIGIBLE, request.getPartnerId());
            WriteLog.write(request.getRequestId(), className, "checkEligible", String.format("Response: %s", gson.toJson(response)));
        } catch (Exception ex) {
            WriteLog.error(request.getRequestId(), className, "checkEligible", gson.toJson(request), ex);
            responseEntity = Utils.responseError(request, className, "checkEligible", request.getRequestId());
        } finally {
        	WriteLog.write(request.getRequestId(), className, "checkEligible: processTime",  (new Date().getTime() -timeMilli ));
        }
        return responseEntity;
    }

}
