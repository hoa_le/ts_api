package vn.hdbank.ts.utils;


import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileReader;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class SecurityUtils {

    public static String encrypt3DES(String plaintext, byte[] sharedkey, byte[] sharedvector) throws Exception {
        Cipher c = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        c.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(sharedkey, "DESede"), new IvParameterSpec(sharedvector));
        byte[] encrypted = c.doFinal(plaintext.getBytes("UTF-8"));
        // return Base64.getEncoder().encodeToString(encrypted);
        return DatatypeConverter.printBase64Binary(encrypted);

        // return Base64Coder.encodeLines(encrypted);
    }

    public static String decrypt3DES(String ciphertext, byte[] sharedkey, byte[] sharedvector) throws Exception {
        ciphertext = ciphertext.replace('@', '+');
        Cipher c = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        c.init(Cipher.DECRYPT_MODE, new SecretKeySpec(sharedkey, "DESede"), new IvParameterSpec(sharedvector));

        byte[] parseBase64Binary = DatatypeConverter.parseBase64Binary(ciphertext);

        byte[] decrypted = c.doFinal(parseBase64Binary);
        return new String(decrypted, "UTF-8");
    }

    public static String sha256(String data) {



        StringBuffer sb = new StringBuffer();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(data.getBytes());
            byte byteData[] = md.digest();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
                        .substring(1));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static String encryptPrivateRSA(String data, String privateKeyPath) throws Exception {

        byte[] dataBytes = data.getBytes("UTF-8");

        File privateKeyFile = new File(privateKeyPath); // private key file in
        try (// PEM format
		PEMParser pemParser = new PEMParser(new FileReader(privateKeyFile))) {
			PEMKeyPair keyPair = (PEMKeyPair) pemParser.readObject();
			AsymmetricKeyParameter privateParam = PrivateKeyFactory.createKey(keyPair.getPrivateKeyInfo());

			AsymmetricBlockCipher e = new RSAEngine();
			e = new org.bouncycastle.crypto.encodings.PKCS1Encoding(e);
			e.init(true, privateParam);
			byte[] hexEncodedCipher = e.processBlock(dataBytes, 0, dataBytes.length);

			return new String(Base64.encodeBase64(hexEncodedCipher));
		}

    }

    public static boolean verifyWithRSA(String data, String sign, String publicKey) throws Exception {

        byte[] publicBytes = Base64.decodeBase64(publicKey);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        PublicKey kp = keyFactory.generatePublic(keySpec);

        byte[] dataBytes = data.getBytes("UTF-8");
        byte[] signByte = sign.getBytes("UTF-8");

        Signature signature = Signature.getInstance("SHA1WithRSA");
        signature.initVerify(kp);
        signature.update(dataBytes);
        return signature.verify(Base64.decodeBase64(signByte));

    }

    public static String signWithRSA(String data, String privateKey) throws Exception {


        byte[] privateKeyByte = Base64.decodeBase64(privateKey);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyByte);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        PrivateKey kp = keyFactory.generatePrivate(keySpec);

        byte[] dataBytes = data.getBytes("UTF-8");

        Signature signature = Signature.getInstance("SHA1WithRSA");
        signature.initSign(kp);
        signature.update(dataBytes);
        return Base64.encodeBase64String(signature.sign());

    }

    public static String createPublicPrivateKey() throws Exception {
        SecureRandom sr = new SecureRandom();
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048, sr);

        KeyPair kp = kpg.genKeyPair();
        PublicKey publicKey = kp.getPublic();
        PrivateKey privateKey = kp.getPrivate();

        return java.util.Base64.getEncoder().encodeToString(publicKey.getEncoded()) + "#"
                + java.util.Base64.getEncoder().encodeToString(privateKey.getEncoded());
    }

    public static String createSignature(String data, String internalPrivateKey) throws Exception {
        byte[] privateKeyByte = org.apache.commons.codec.binary.Base64.decodeBase64(internalPrivateKey);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyByte);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        PrivateKey kp = keyFactory.generatePrivate(keySpec);

        byte[] dataBytes = data.getBytes("UTF-8");

        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(kp);
        signature.update(dataBytes);
        return org.apache.commons.codec.binary.Base64.encodeBase64String(signature.sign());
    }

    public static boolean verifySignature(String data, String sign, String internalPublicKey) throws Exception {
        //System.out.println("public key: " + internalPublicKey);

        byte[] publicBytes = org.apache.commons.codec.binary.Base64.decodeBase64(internalPublicKey);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        PublicKey kp = keyFactory.generatePublic(keySpec);

        byte[] dataBytes = data.getBytes("UTF-8");
        byte[] signByte = sign.getBytes("UTF-8");

        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initVerify(kp);
        signature.update(dataBytes);
        return signature.verify(org.apache.commons.codec.binary.Base64.decodeBase64(signByte));
    }




}