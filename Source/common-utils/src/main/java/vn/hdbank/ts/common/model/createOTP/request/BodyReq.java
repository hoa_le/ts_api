package vn.hdbank.ts.common.model.createOTP.request;

public class BodyReq {

    private String functionCode;

    private Request request;

    private Data data;

    public String getFunctionCode() { return functionCode; }
    public void setFunctionCode(String value) { this.functionCode = value; }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
